<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>C:/Users/c301/Documents/JOB/planes2/planes_UI.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json-array</string>
        <key>textureFileName</key>
        <filename>game/assets/graphics/graphics_UI{n1}.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>game/assets/graphics/graphics_UI{n1}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">KeepTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">assets/graphics/game/UI/EL_BIG.png</key>
            <key type="filename">assets/graphics/game/UI/El_Choise.png</key>
            <key type="filename">assets/graphics/game/UI/El_Level.png</key>
            <key type="filename">assets/graphics/game/UI/N1.png</key>
            <key type="filename">assets/graphics/game/UI/N2.png</key>
            <key type="filename">assets/graphics/game/UI/N3.png</key>
            <key type="filename">assets/graphics/game/UI/N4.png</key>
            <key type="filename">assets/graphics/game/UI/N5.png</key>
            <key type="filename">assets/graphics/game/UI/N6.png</key>
            <key type="filename">assets/graphics/game/UI/aim.png</key>
            <key type="filename">assets/graphics/game/UI/aim_icon.png</key>
            <key type="filename">assets/graphics/game/UI/arrow_bonus.png</key>
            <key type="filename">assets/graphics/game/UI/arrow_enemy.png</key>
            <key type="filename">assets/graphics/game/UI/bg_cloud1.png</key>
            <key type="filename">assets/graphics/game/UI/bg_cloud2.png</key>
            <key type="filename">assets/graphics/game/UI/bonus_icon.png</key>
            <key type="filename">assets/graphics/game/UI/bullet.png</key>
            <key type="filename">assets/graphics/game/UI/choose_level.png</key>
            <key type="filename">assets/graphics/game/UI/choose_level_ru.png</key>
            <key type="filename">assets/graphics/game/UI/cloud_bad.png</key>
            <key type="filename">assets/graphics/game/UI/cloud_icon.png</key>
            <key type="filename">assets/graphics/game/UI/control_icon.png</key>
            <key type="filename">assets/graphics/game/UI/damage_icon.png</key>
            <key type="filename">assets/graphics/game/UI/down_control.png</key>
            <key type="filename">assets/graphics/game/UI/drunken_bullet.png</key>
            <key type="filename">assets/graphics/game/UI/fire_control.png</key>
            <key type="filename">assets/graphics/game/UI/fire_up_icon.png</key>
            <key type="filename">assets/graphics/game/UI/life_icon.png</key>
            <key type="filename">assets/graphics/game/UI/line/1.png</key>
            <key type="filename">assets/graphics/game/UI/line/10.png</key>
            <key type="filename">assets/graphics/game/UI/line/11.png</key>
            <key type="filename">assets/graphics/game/UI/line/12.png</key>
            <key type="filename">assets/graphics/game/UI/line/2.png</key>
            <key type="filename">assets/graphics/game/UI/line/3.png</key>
            <key type="filename">assets/graphics/game/UI/line/4.png</key>
            <key type="filename">assets/graphics/game/UI/line/5.png</key>
            <key type="filename">assets/graphics/game/UI/line/6.png</key>
            <key type="filename">assets/graphics/game/UI/line/7.png</key>
            <key type="filename">assets/graphics/game/UI/line/8.png</key>
            <key type="filename">assets/graphics/game/UI/line/9.png</key>
            <key type="filename">assets/graphics/game/UI/line/line.png</key>
            <key type="filename">assets/graphics/game/UI/lines_prop.png</key>
            <key type="filename">assets/graphics/game/UI/lock.png</key>
            <key type="filename">assets/graphics/game/UI/pause_button.png</key>
            <key type="filename">assets/graphics/game/UI/plane_blue_closed.png</key>
            <key type="filename">assets/graphics/game/UI/plane_green_closed.png</key>
            <key type="filename">assets/graphics/game/UI/plane_red_closed.png</key>
            <key type="filename">assets/graphics/game/UI/plane_yellow_closed.png</key>
            <key type="filename">assets/graphics/game/UI/propeller.png</key>
            <key type="filename">assets/graphics/game/UI/propeller_1.png</key>
            <key type="filename">assets/graphics/game/UI/quick_planes_battle.png</key>
            <key type="filename">assets/graphics/game/UI/quick_planes_battle_ru.png</key>
            <key type="filename">assets/graphics/game/UI/speed_icon.png</key>
            <key type="filename">assets/graphics/game/UI/speed_up.png</key>
            <key type="filename">assets/graphics/game/UI/turn_back.png</key>
            <key type="filename">assets/graphics/game/UI/turn_back_1.png</key>
            <key type="filename">assets/graphics/game/UI/turn_rate_icon.png</key>
            <key type="filename">assets/graphics/game/UI/ui_background.png</key>
            <key type="filename">assets/graphics/game/UI/up_control.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>assets/graphics/game/UI</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
