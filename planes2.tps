<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>4.1.0</string>
        <key>fileName</key>
        <string>C:/Users/c301/Documents/JOB/planes2/planes2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json-array</string>
        <key>textureFileName</key>
        <filename>game/assets/graphics/graphics.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Name</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>game/assets/graphics/graphics.json</filename>
            </struct>
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>game/assets/graphics/graphics.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>0</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">assets/graphics/game/Copy of grey_panel_big.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion00.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion01.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion02.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion03.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion04.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion05.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion06.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion07.png</key>
            <key type="filename">assets/graphics/game/Ground explosion/groundExplosion08.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion00.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion01.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion02.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion03.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion04.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion05.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion06.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion07.png</key>
            <key type="filename">assets/graphics/game/Regular explosion/regularExplosion08.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion00.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion01.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion02.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion03.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion04.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion05.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion06.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion07.png</key>
            <key type="filename">assets/graphics/game/Simple explosion/simpleExplosion08.png</key>
            <key type="filename">assets/graphics/game/background.png</key>
            <key type="filename">assets/graphics/game/bolt_gold.png</key>
            <key type="filename">assets/graphics/game/bulletBeige_outline.png</key>
            <key type="filename">assets/graphics/game/button-grey/grey_button01.png</key>
            <key type="filename">assets/graphics/game/button-grey/grey_button02.png</key>
            <key type="filename">assets/graphics/game/cloud1.png</key>
            <key type="filename">assets/graphics/game/cloud3.png</key>
            <key type="filename">assets/graphics/game/cloud5.png</key>
            <key type="filename">assets/graphics/game/cloudUpload.png</key>
            <key type="filename">assets/graphics/game/coinGold.png</key>
            <key type="filename">assets/graphics/game/crossair_blackOutline.png</key>
            <key type="filename">assets/graphics/game/crossair_blueOutline.png</key>
            <key type="filename">assets/graphics/game/crossair_redOutline.png</key>
            <key type="filename">assets/graphics/game/crosshair_red_large.png</key>
            <key type="filename">assets/graphics/game/crosshair_red_small.png</key>
            <key type="filename">assets/graphics/game/fireball.png</key>
            <key type="filename">assets/graphics/game/flatDark48.png</key>
            <key type="filename">assets/graphics/game/flat_medal4.png</key>
            <key type="filename">assets/graphics/game/gemRed.png</key>
            <key type="filename">assets/graphics/game/green_boxCheckmark.png</key>
            <key type="filename">assets/graphics/game/green_boxCross.png</key>
            <key type="filename">assets/graphics/game/green_sliderDown.png</key>
            <key type="filename">assets/graphics/game/green_sliderRight.png</key>
            <key type="filename">assets/graphics/game/grey_panel_big.png</key>
            <key type="filename">assets/graphics/game/ground.png</key>
            <key type="filename">assets/graphics/game/hudHeart_full.png</key>
            <key type="filename">assets/graphics/game/lock_blue.png</key>
            <key type="filename">assets/graphics/game/lock_green.png</key>
            <key type="filename">assets/graphics/game/lock_red.png</key>
            <key type="filename">assets/graphics/game/lock_yellow.png</key>
            <key type="filename">assets/graphics/game/metalPanel.png</key>
            <key type="filename">assets/graphics/game/musicOff.png</key>
            <key type="filename">assets/graphics/game/musicOn.png</key>
            <key type="filename">assets/graphics/game/orangeCloud1.png</key>
            <key type="filename">assets/graphics/game/pill_green.png</key>
            <key type="filename">assets/graphics/game/pixelPlaneGrey/plane1.png</key>
            <key type="filename">assets/graphics/game/pixelPlaneGrey/plane2.png</key>
            <key type="filename">assets/graphics/game/pixelPlaneGrey/plane3.png</key>
            <key type="filename">assets/graphics/game/pixelPlaneGrey/plane4.png</key>
            <key type="filename">assets/graphics/game/planeBlue/planeBlue1.png</key>
            <key type="filename">assets/graphics/game/planeBlue/planeBlue2.png</key>
            <key type="filename">assets/graphics/game/planeBlue/planeBlue3.png</key>
            <key type="filename">assets/graphics/game/planeGreen/planeGreen1.png</key>
            <key type="filename">assets/graphics/game/planeGreen/planeGreen2.png</key>
            <key type="filename">assets/graphics/game/planeGreen/planeGreen3.png</key>
            <key type="filename">assets/graphics/game/planeRed/planeRed1.png</key>
            <key type="filename">assets/graphics/game/planeRed/planeRed2.png</key>
            <key type="filename">assets/graphics/game/planeRed/planeRed3.png</key>
            <key type="filename">assets/graphics/game/planeYellow/planeYellow1.png</key>
            <key type="filename">assets/graphics/game/planeYellow/planeYellow2.png</key>
            <key type="filename">assets/graphics/game/planeYellow/planeYellow3.png</key>
            <key type="filename">assets/graphics/game/plant.png</key>
            <key type="filename">assets/graphics/game/powerupBlue.png</key>
            <key type="filename">assets/graphics/game/powerupYellow_bolt.png</key>
            <key type="filename">assets/graphics/game/puffLarge.png</key>
            <key type="filename">assets/graphics/game/puffSmall.png</key>
            <key type="filename">assets/graphics/game/redCloud4.png</key>
            <key type="filename">assets/graphics/game/red_boxCross.png</key>
            <key type="filename">assets/graphics/game/red_sliderRight.png</key>
            <key type="filename">assets/graphics/game/rock.png</key>
            <key type="filename">assets/graphics/game/shadedDark49.png</key>
            <key type="filename">assets/graphics/game/signRight.png</key>
            <key type="filename">assets/graphics/game/squareGreen.png</key>
            <key type="filename">assets/graphics/game/square_shadow.png</key>
            <key type="filename">assets/graphics/game/star_empty.png</key>
            <key type="filename">assets/graphics/game/star_full.png</key>
            <key type="filename">assets/graphics/game/text_0.png</key>
            <key type="filename">assets/graphics/game/text_0_small.png</key>
            <key type="filename">assets/graphics/game/text_1.png</key>
            <key type="filename">assets/graphics/game/text_1_small.png</key>
            <key type="filename">assets/graphics/game/text_2.png</key>
            <key type="filename">assets/graphics/game/text_2_small.png</key>
            <key type="filename">assets/graphics/game/text_3.png</key>
            <key type="filename">assets/graphics/game/text_3_small.png</key>
            <key type="filename">assets/graphics/game/text_4.png</key>
            <key type="filename">assets/graphics/game/text_4_small.png</key>
            <key type="filename">assets/graphics/game/text_5.png</key>
            <key type="filename">assets/graphics/game/text_5_small.png</key>
            <key type="filename">assets/graphics/game/text_6.png</key>
            <key type="filename">assets/graphics/game/text_6_small.png</key>
            <key type="filename">assets/graphics/game/text_7.png</key>
            <key type="filename">assets/graphics/game/text_7_small.png</key>
            <key type="filename">assets/graphics/game/text_8.png</key>
            <key type="filename">assets/graphics/game/text_8_small.png</key>
            <key type="filename">assets/graphics/game/text_9.png</key>
            <key type="filename">assets/graphics/game/text_9_small.png</key>
            <key type="filename">assets/graphics/game/things_bronze.png</key>
            <key type="filename">assets/graphics/game/warning_line.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>assets/graphics/game</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
