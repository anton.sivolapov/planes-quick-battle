return {
    name= "level4",
    _desc= "super cloudy level",
    enemies_waves = {
        {
             enemies   = {
                 {
                     plane_id= "green",
                     health = 10,
                     x= 4000,
                     y= 300,
                     item= {
                         item_id= "fire_up"
                     }
                 },
                 {
                     plane_id= "green",
                     health = 5,
                     x= 1,
                     y= 100,
                     item= 
                     {
                         item_id= "add_clouds"
                     }
                 },
                 {
                     plane_id= "green",
                     health = 15,
                     x= 1,
                     y= 100,
                     time= 15,
                     item= 
                     {
                         item_id= "add_clouds"
                     }
                 }
             },
             items   = {
                 {
                     item_id= "drunken_bullet",
                     time= 20
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 300,
                     velocity      = {
                         x    = -100,
                         y    = 10
                     }
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 530,
                     velocity      = {
                         x    = 50
                     },
                     time= 10
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 930,
                     velocity      = {
                         x    = 10
                     }
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 1430,
                     velocity      = {
                         x    = 20,
                         y    = -10
                     },
                     time          = 20
                 }
             }
        },
        {
            enemies   = {
                 {
                     plane_id= "yellow",
                     health = 20,
                     x= 4000,
                     y= 300,
                     item= {
                         item_id= "fire_up"
                     }
                 },
                 {
                     plane_id= "red",
                     health = 25,
                     x= 1,
                     y= 100,
                     time= 30,
                     item= 
                     {
                         item_id= "add_clouds"
                     }
                 }
            },
             items   = {
                 {
                     item_id= "aim",
                     time= 20
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 1000,
                     velocity      = {
                         x    = -30
                     }
                 },
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 1200,
                     velocity      = {
                         x    = -20
                     },
                     time= 10
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 130,
                     velocity      = {
                         x    = 10
                     }
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 630,
                     velocity      = {
                         x    = 20,
                         y    = -10
                     },
                     time          = 20
                 },
                 {
                     object_id     = "cloud",
                     x             = 1000,
                     y             = 700,
                     velocity      = {
                         x    = 0,
                         y    = 0
                     }
                 }
             }
        }
    },
    rewards= {
        state_update= {
            openedLevels= {5},
            openedPlanes= {"yellow"}
        }
    }
}
