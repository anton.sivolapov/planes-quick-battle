return {
    name          = "level1",
    enemies_waves = {
        {
            enemies   = {
                {
                    plane_id= "green",
                    health = 5,
                    item   = 
                    {
                        item_id= "drunken_bullet"
                    }
                    
                }
            },
            objects = {
                {
                    object_id     = "cloud",
                    x             = 4000,
                    y             = 300,
                    velocity      = {
                        x    = -100,
                        y    = 10
                    }
                },
                {
                    object_id     = "cloud",
                    x             = 1,
                    y             = 730,
                    velocity      = {
                        x    = 20,
                        y    = -10
                    },
                    time          = 20
                }
            }
        },
        {
            enemies   = {
                {
                    plane_id= "green",
                    health = 5,
                    x= 0,
                    y= 600,
                    item= {
                        item_id= "fire_up"
                    }
                },
                {
                    plane_id= "green",
                    health = 5,
                    x= 4000,
                    y= 200
                }
            },
            items   = {
                {
                    item_id= "aim",
                    time= 15
                }
            }
        },
        {
            enemies   = {
                {
                    plane_id= "red",
                    health = 10,
                    x= 4000,
                    y= 100
                }
            },
            items   = {
                {
                    item_id= "speed",
                    time= 10
                }
            }
        }
    },
    rewards= {
        state_update= {
            openedLevels= {2},
            openedPlanes= {"red"}
        }
    }
}
