return {
    name= "level5",
    _desc= "many weak planes",
    enemies_waves = {
        {
             enemies   = {
                 {
                     plane_id= "green",
                     health = 4,
                     x= 4000,
                     y= 300,
                     item= {
                         item_id= "aim"
                     }
                 },
                 {
                     plane_id= "green",
                     health = 4,
                     x= 4000,
                     y= 900
                 },
                 {
                     plane_id= "green",
                     health = 4,
                     x= 1,
                     y= 100,
                     item= 
                     {
                         item_id= "fire_up"
                     }
                 },
                 {
                     plane_id= "green",
                     health = 4,
                     x= 1,
                     y= 700,
                     time= 3
                 },
                 {
                     plane_id= "green",
                     health = 4,
                     x= 1,
                     y= 1200,
                     time= 5
                 }
             },
             items   = {
                 {
                     item_id= "drunken_bullet",
                     time= 20
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 300,
                     velocity      = {
                         x    = -100,
                         y    = 10
                     }
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 530,
                     velocity      = {
                         x    = 50
                     },
                     time= 10
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 930,
                     velocity      = {
                         x    = 10
                     }
                 }
             }
        },
        {
            enemies   = {
                 {
                     plane_id= "yellow",
                     health = 3,
                     x= 4000,
                     y= 100,
                     item= {
                         item_id= "fire_up"
                     }
                 },
                 {
                     plane_id= "yellow",
                     health = 3,
                     x= 10,
                     y= 700,
                     item= {
                         item_id= "turn_rate"
                     }
                 },
                 {
                     plane_id= "yellow",
                     health = 3,
                     x= 1,
                     y= 800,
                     item= {
                         item_id= "fire_up"
                     }
                 },
                 {
                     plane_id= "yellow",
                     health = 3,
                     x= 4000,
                     y= 500,
                     item= {
                         item_id= "speed"
                     }
                 },
                 {
                     plane_id= "yellow",
                     health = 3,
                     x= 4000,
                     y= 300,
                     item= {
                         item_id= "fire_up"
                     }
                 }
            },
             items   = {
                 {
                     item_id= "aim",
                     time= 20
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 1000,
                     velocity      = {
                         x    = -30
                     }
                 }
             }
        },
        {
            enemies   = {
                 {
                     plane_id= "red",
                     health = 3,
                     x= 4000,
                     y= 100,
                     item= {
                         item_id= "fire_up"
                     }
                 },
                 {
                     plane_id= "red",
                     health = 3,
                     x= 4000,
                     y= 1000,
                 },
                 {
                     plane_id= "red",
                     health = 3,
                     x= 10,
                     y= 700,
                 },
                 {
                     plane_id= "red",
                     health = 3,
                     x= 4000,
                     y= 300,
                 }
            },
             items   = {
                 {
                     item_id= "aim",
                     time= 20
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 1000,
                     velocity      = {
                         x    = -30
                     }
                 }
             }
        },
        {
             enemies   = {
                 {
                     plane_id= "blue",
                     health = 20,
                     x= 4000,
                     y= 300
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 1000,
                     velocity      = {
                         x    = -30
                     }
                 }
             }
        },
    },
    rewards= {
        state_update= {
            openedLevels= {6}
        }
    }
}
