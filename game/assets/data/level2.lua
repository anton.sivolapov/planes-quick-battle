return {
     name          = "level2",
     enemies_waves = {
         {
             enemies   = {
                 {
                     plane_id= "green",
                     health = 4,
                     x= 4000,
                     y= 300,
                     item= {
                         item_id= "aim"
                     }
                 },
                 {
                     plane_id= "green",
                     health = 4,
                     x= 1,
                     y= 100
                 }
             },
             items   = {
                 {
                     item_id= "random_bonus",
                     time= 25
                 },
                 {
                     item_id= "speed",
                     time= 15
                 }
             }
         },
         {
             enemies   = {
                 {
                     plane_id= "red",
                     health = 6,
                     x= 0,
                     y= 600
                 },
                 {
                     plane_id= "green",
                     health = 4,
                     time= 15,
                     x= 4000,
                     y= 200
                 }
             },
             items   = {
                 {
                     item_id= "add_clouds"
                 },
                 {
                     item_id= "drunken_bullet",
                     time= 35
                 },
                 {
                     item_id= "turn_rate",
                     time= 25
                 }
             }
         },
         -- {
         --     enemies   = {
         --         {
         --             plane_id= "yellow",
         --             health = 5,
         --             x= 4000,
         --             y= 600
         --         },
         --         {
         --             plane_id= "green",
         --             health = 5,
         --             time= 15,
         --             x= 1,
         --             y= 500
         --         },
         --         {
         --             plane_id= "green",
         --             health = 10,
         --             time= 35,
         --             x= 4000,
         --             y= 200
         --         }
         --     },
         --     objects = {
         --         {
         --             object_id     = "cloud",
         --             x             = 4000,
         --             y             = 700,
         --             velocity      = {
         --                 x     = -20
         --             }
         --         },
         --         {
         --             object_id     = "cloud",
         --             x             = 1,
         --             y             = 900,
         --             velocity      = {
         --                 x     = 50
         --             },
         --             time          = 20
         --         }
         --     },
         --     items   = {
         --         {
         --             item_id= "fire_up"
         --         },
         --         {
         --             item_id= "speed",
         --             time= 10
         --         }
         --     }
         -- }
     },
     rewards= {
         state_update= {
             openedLevels= {3}
         }
     }
 }
