return {
     name ="level3",
     night= true,
     enemies_waves = {
         {
             enemies   = {
                 {
                     plane_id= "yellow",
                     health = 10,
                     x= 4000,
                     y= 300,
                     item= {
                         item_id= "fire_up"
                     }
                 },
                 {
                     plane_id= "green",
                     health = 5,
                     x= 1,
                     y= 100,
                     item= 
                     {
                         item_id= "add_clouds"
                     }
                 }
             },
             items   = {
                 {
                     item_id= "turn_rate",
                     time= 20
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 300,
                     velocity      = {
                         x    = -100,
                         y    = 10
                     }
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 730,
                     velocity      = {
                         x    = 20,
                         y    = -10
                     },
                     time          = 20
                 }
             }
         },
         {
             enemies   = {
                 {
                     plane_id= "red",
                     health = 7,
                     x= 1,
                     y= 600,
                     item= {
                         item_id= "add_clouds"
                     }
                 },
                 {
                     plane_id= "red",
                     health = 15,
                     time= 25,
                     x= 4000,
                     y= 200,
                     item= {
                         item_id= "random_bonus"
                     }
                 }
             },
             items   = {
                 {
                     item_id= "drunken_bullet",
                     time= 35
                 }
             }
         },
         {
             enemies   = {
                 {
                     plane_id= "yellow",
                     health = 5,
                     x= 4000,
                     y= 600
                 },
                 {
                     plane_id= "yellow",
                     health = 5,
                     time= 15,
                     x= 1,
                     y= 500
                 },
                 {
                     plane_id= "yellow",
                     health = 10,
                     time= 35,
                     x= 4000,
                     y= 200
                 }
             },
             objects = {
                 {
                     object_id     = "cloud",
                     x             = 4000,
                     y             = 700,
                     velocity      = {
                         x     = -70
                     }
                 },
                 {
                     object_id     = "cloud",
                     x             = 1,
                     y             = 900,
                     velocity      = {
                         x     = 50
                     },
                     time          = 20
                 }
             },
             items   = {
                 {
                     item_id= "speed",
                     time= 10
                 }
             }
         }
     },
     rewards= {
         state_update= {
             openedLevels= {4}
         }
     }
}
