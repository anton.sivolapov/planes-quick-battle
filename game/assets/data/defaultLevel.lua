return {
    background    = "background.png",
    ground        = "ground.png",
    enemies_waves = {{
            enemies   = {
                {
                    plane_id= "green",
                    health = 4,
                    item= {
                        item_id= "random_bonus"
                    }
                }
            }
    }},
    level_data    = {
        objects   = {
            {
                object_id     = "bg_cloud1",
                x             = 500,
                y             = -500,
                layer         = "backgroundDetails4"
            },
            {
                object_id     = "bg_cloud1",
                x             = 1200,
                y             = -600,
                layer         = "backgroundDetails4"
            },
            {
                object_id     = "bg_cloud1",
                x             = 1500,
                y             = -800,
                layer         = "backgroundDetails4"
            },
            {
                object_id     = "bg_cloud2",
                x             = 800,
                y             = 300,
                layer         = "backgroundDetails4"
            },
            {
                object_id     = "bg_cloud2",
                x             = 1900,
                y             = 400,
                layer         = "backgroundDetails4"
            },
            {
                object_id     = "hangar",
                x             = 150,
                on_ground     = true,
                layer         = "backgroundDetails3"
            },
            -- {
            --     object_id     = "cloud",
            --     x             = 1,
            --     y             = 500,
            --     velocity      = {
            --         x     = 20
            --     }
            -- },
            -- {
            --     object_id     = "cloud",
            --     x             = 4000,
            --     y             = 400,
            --     velocity      = {
            --         x     = -30
            --     },
            --     time          = 10
            -- },
            -- {
            --     object_id     = "cloud",
            --     x             = 1,
            --     y             = 620,
            --     velocity      = {
            --         x     = 40
            --     },
            --     time          = 50
            -- },
            -- {
            --     object_id     = "cloud",
            --     x             = 1,
            --     y             = 890,
            --     velocity      = {
            --         x     = 40
            --     },
            --     time          = 70
            -- }
        }
    },
    rewards= {
    }
}
