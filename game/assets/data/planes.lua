local planesData = {
    {
        plane_id      = "green",
        label         = "Green",
        hasPropeller  = true,
        startFrame    = "planeGreen/planeGreen3.png",
        bodyName      = "planeGreen2",
        speed         = 250,
        health        = 15,
        turn_rate     = 2.5,
        chanceToFollowPlayer= 70,
        chanceToLoosePlayer = 3,
        max_bullets   = 2,
        animations    = {
            flight        = {
                "planeGreen/planeGreen1.png", "planeGreen/planeGreen2.png", "planeGreen/planeGreen3.png"}
        },
        information   = {
            description   = _s("Good old green plane. Super good for vegans, but still can prepare some meat!"),
            stats         = {
                {
                    name= "damage",
                    points= 6
                },
                {
                    name= "speed",
                    points= 6
                },
                {
                    name= "control",
                    points= 6
                }
            }
        }
    },
    {
        plane_id      = "red",
        label         = "Red",
        hasPropeller  = true,
        startFrame    = "planeRed/planeRed3.png",
        bodyName      = "planeRed2",
        speed         = 300,
        bullet_speed  = 750,
        chanceToFollowPlayer= 80,
        max_bullets   = 4,
        -- red plane has huge power but reload slowly
        fire_cooldown = 5000,
        turn_rate     = 2.5,
        animations    = {
            flight        = {"planeRed/planeRed1.png", "planeRed/planeRed2.png", "planeRed/planeRed3.png"}
        },
        information   = {
            description   = _s("Red Baron comes to deal! Heavy metal dude will fight for you!"),
            stats         = {
                {
                    name= "damage",
                    points= 10
                },
                {
                    name= "speed",
                    points= 7
                },
                {
                    name= "control",
                    points= 5
                }
            }
        }
    },
    {
        plane_id      = "yellow",
        label         = "Yellow",
        hasPropeller  = true,
        bodyName      = "planeYellow2",
        speed         = 350,
        health        = 10,
        bullet_speed  = 950,
        max_bullets   = 2,
        -- yellow plane has small power but quick reload
        fire_cooldown = 1000,
        turn_rate     = 3.2,
        startFrame    = "planeYellow/planeYellow3.png",
        animations    = {
            flight        = {"planeYellow/planeYellow1.png", "planeYellow/planeYellow2.png", "planeYellow/planeYellow3.png"}
        },
        information   = {
            description   = _s("Fancy yellow chick faster then light!"),
            stats         = {
                {
                    name= "damage",
                    points= 3
                },
                {
                    name= "speed",
                    points= 7
                },
                {
                    name= "control",
                    points= 11
                }
            }
        }
    },
    {
        plane_id      = "blue",
        label         = "Blue",
        hasPropeller  = false,
        bodyName      = "planeBlue2",
        speed         = 350,
        bullet_speed  = 950,
        max_bullets   = 4,
        turn_rate     = 2.7,
        startFrame    = "planeBlue/plane_blue_small_4.png",
        animations    = {
            flight            = { "planeBlue/plane_blue_small_1.png" },
            takeoff           = { "planeBlue/plane_blue_small_4.png", "planeBlue/plane_blue_small_3.png", "planeBlue/plane_blue_small_2.png", "planeBlue/plane_blue_small_1.png" }
        },
        information   = {
            description   = _s("Nearly unbreakable plane! Who could tame him?"),
            stats         = {
                {
                    name= "damage",
                    points= 7
                },
                {
                    name= "speed",
                    points= 10
                },
                {
                    name= "control",
                    points= 7
                }
            }
        }
    },
    -- {
    --     plane_id      = "gray",
    --     label         = "Gray",
    --     hasPropeller  = true,
    --     bodyName      = "planeGray1",
    --     closedFrame   = "planeGray/planeGray1.png",
    --     speed         = 350,
    --     bullet_speed  = 950,
    --     max_bullets   = 4,
    --     turn_rate     = 2.7,
    --     startFrame    = "planeGray/planeGray1.png",
    --     animations    = {
    --         stand            = {
    --             "planeGray/planeGray1.png",
    --             "planeGray/planeGray2.png",
    --             "planeGray/planeGray3.png"
    --         },
    --         flight            = {
    --             "planeGray/planeGray10.png",
    --             "planeGray/planeGray11.png",
    --             "planeGray/planeGray12.png"
    --         },
    --         takeoff           = {
    --             "planeGray/planeGray1.png",
    --             "planeGray/planeGray2.png",
    --             "planeGray/planeGray3.png",
    --             "planeGray/planeGray4.png",
    --             "planeGray/planeGray5.png",
    --             "planeGray/planeGray6.png",
    --             "planeGray/planeGray7.png",
    --             "planeGray/planeGray8.png",
    --             "planeGray/planeGray9.png",
    --         }
    --     },
    --     information   = {
    --         description   = _s("Nearly unbreakable plane! Who could tame him?"),
    --         stats         = {
    --             {
    --                 name= "damage",
    --                 points= 7
    --             },
    --             {
    --                 name= "speed",
    --                 points= 10
    --             },
    --             {
    --                 name= "control",
    --                 points= 7
    --             }
    --         }
    --     }
    -- }
}

return planesData
