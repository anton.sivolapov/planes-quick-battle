return {
    name= "level6",
    _desc= "one plane in each wave. no clouds",
    enemies_waves = {
        {
            enemies   = {
                {
                    plane_id= "green",
                    health = 20,
                    item   = 
                    {
                        item_id= "fire_up"
                    }
                    
                }
            },
            objects = {
                {
                    object_id     = "cloud",
                    x             = 4000,
                    y             = 300,
                    velocity      = {
                        x    = -100,
                        y    = 20
                    }
                }
            }
        },
        {
            enemies   = {
                {
                    plane_id= "red",
                    health = 20,
                    x= 0,
                    y= 600,
                    item= {
                        item_id= "speed"
                    }
                }
            }
        },
        {
            enemies   = {
                {
                    plane_id= "yellow",
                    health = 20,
                    x= 4000,
                    y= 100,
                    item= {
                        item_id= "turn_rate"
                    }
                }
            }
        },
        {
            enemies   = {
                {
                    plane_id= "blue",
                    health = 20,
                    x= 4000,
                    y= 100
                }
            }
        }
    },
    rewards= {
        state_update= {
            openedPlanes= {"blue"}
        }
    }
}
