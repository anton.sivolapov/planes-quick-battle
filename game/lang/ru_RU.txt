Loading ( %s%% ) .. = Загрузка ( %s%% ) ..
Remove Ads = Убрать\nРекламу

Flight through clouds to confuse your opponents! = Залетай в облака, что бы сбить с толку противника!

Some bonuses takes permanent effect! = Некоторые бонусы действуют весь уровень!

Dive closer to ground to take more speed! = Пикируй к земле для большей скорости!

You can leave battleground to be immediatelly turned back = Можно вылететь за пределы уровня что бы развернуться мгновенно!

Your enemies make damage each other - use it! = Противники повреждают друг друга - используй это!

quick_planes_battle.png = quick_planes_battle_ru.png
quick_planes.png = quick_planes_ru_1.png
Battle.png = battle_ru_1.png
button_start_off.png = button_start_off_ru.png
button_start_on.png = button_start_on_ru.png

This plane is closed so far. Finish few levels to open it. = Это аппарат пока недоступен.\nПройди больше уровней\nчто бы разблокировать его

Good old green plane. Super good for vegans, but still can prepare some meat! = Старый добрый зелёный самолёт. \nПохож на овощ,\n но все же может поджарить врага!

Red Baron comes to deal! Heavy metal dude will fight for you! = Красный Барон идет на дело!\n Стальной пилот будет драться за тебя!

Fancy yellow chick faster then light! = Юркая желтая штучка\nлетит быстрее ветра!

Nearly unbreakable plane! Who could tame him? = Почти непобедимый самолет!\n Кто посмеет укротить его?

choose_level.png = choose_level_ru.png
choose_level_text.png = choose_level_text_ru.png
start_level_button_off.png = start_level_button_off_ru.png
start_level_button_on.png = start_level_button_on_ru.png

Get Ready! = Приготовься! 
Game Over! = Поражение!
Level Cleared! = Победа!
You Win! = Победа!
You win! = Победа!
Turn back to the battle, chicken! = Возвращайся на поле боя, трусишка!

-- pause
Pause/button_exit_off.png = Pause/button_exit_off_ru.png
Pause/button_exit_on.png = Pause/button_exit_on_ru.png
Pause/button_continue_off.png = Pause/button_continue_off_ru.png
Pause/button_continue_on.png = Pause/button_continue_on_ru.png
Pause/button_reset_off.png = Pause/button_reset_off_ru.png
Pause/button_reset_on.png = Pause/button_reset_on_ru.png
-- end pause

-- message
popup_button_on.png = popup_button_on_ru.png
popup_button_off.png = popup_button_off_ru.png
Free part of the game is over. Please buy a full game and test your skill! = Вы прошли все уровни\nбесплатной версии!\nРазблокируй все уровни и\nперсонажей!
or touch to open it now! = или разблокируй сейчас!
-- end message

-- tutorial
Controls = Управление
Touch here to dive DOWN = Нажми здесь для\nпикирования
Touch here to rise UP = .. здесь для\nнабора высоты
Touch here to FIRE = .. здесь для\nстрельбы
tap to continue.. = продолжение..
Rules = Советы
Avoid ground! It hits hard! = Не врезайся в землю\nэто больно!
Hide in clouds! = Скрывайся в\nоблаках!
Collect bonuses to become stronger! = Собирай бонусы и\nстановись сильнее!
-- end tutorial

-- ask for rating
Visit us on Store = Посети нашу страницу
How about a rating on Webstore, then? = Тогда ты не прочь выставить нам рейтинг?
Ok, sure = Да, конечно
No, thanks = Нет, спасибо
Would you mind giving us some feedback = Оставьте отзыв, если не сложно
How is going? = Как дела?
Enjoying Planes Battle? = Нравятся Сражения в Облаках?
Yes! = Да!
Not Really = Не совсем
-- end ask for rating
