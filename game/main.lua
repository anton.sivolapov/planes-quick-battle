-- remote prev scene on scene enter

-- main global object
Planes = {
    askedForRateInSession = false
}
sheet = nil
spritesheet = nil

-- hide status bar
display.setStatusBar(display.HiddenStatusBar)
display.setDefault( "anchorX", .5)
display.setDefault( "anchorY", .5)
-- init random generator
math.randomseed( os.time() )
-- init multitouch
system.activate( "multitouch" )

print '\n\n'
print 'Initialize game..'


json            = require 'json'
composer        = require 'composer'
spritesheetJSON = require 'scripts.lib.spritesheetJSON'
inspect         = require 'scripts.vendor.inspect'
utils           = require 'scripts.utils'
ObjectFactory   = require 'scripts.ObjectFactory'
_               = require 'scripts.vendor.underscore'
_m              = require 'scripts.vendor.moses'

localize  = require "scripts.vendor.mod_localize" 


Store           = require 'scripts.Store'
pex             = require 'scripts.vendor.pex'
physicsData     = (require "scripts.objects.all_physics").physicsData(1.0)
local Config    = require 'scripts.gameConfig'

-- init admob
Planes.adsConfig = {
    testMode = false,
    battleEnd = {
        id = "ca-app-pub-5316161378481999/6205683466"
    },
    gameEnd = {
        id = "ca-app-pub-5316161378481999/3891345464"
    }
}

if not utils.desktopApp() then
    Planes.ads = require("ads")

    local function adListener( event )
        for k,v in pairs( event ) do
            print("adListener ", k, v ) -- so you see everything you get
        end
    end

    Planes.ads.init("admob", Planes.adsConfig.battleEnd.id, adListener )

    local battleEndId = Planes.adsConfig.battleEnd.id
    Planes.ads.load(
        "interstitial",
        { appId = battleEndId, testMode = Planes.adsConfig.testMode }
    )
    -- Flurry Analytics
    flurryAnalytics = require( "plugin.flurry.analytics" )
else
    flurryAnalytics = {
        init = function() end,
        logEvent = function() end
    }
end


local function flurryListener( event )
    if ( event.phase == "init" ) then  -- Successful initialization
        print( event.provider )
    end
end

-- Initialize the Flurry plugin
flurryAnalytics.init( flurryListener, { apiKey="B32QSVJDRSFCS3RM7J35" } )
-- end Flurry Analytics

-- set locale
_s              = localize.str --place in shortcut var
local locale    = system.getPreference( "locale", "language" )
local originalLocale = locale
print('locale', locale)

    -- locale = 'en'
if not Config.supportedLocales[locale] then
    print(string.format('Warning: unsupported locale "%s". Use default "en"', locale))
    locale = 'en'
end

flurryAnalytics.logEvent( "Locale selected " .. locale .. " ( ".. originalLocale .. " )" )
localize:setLocale( Config.supportedLocales[locale] )
-- localize:setLocale( 'en_US' )

local fm        = require 'scripts.vendor.fontmanager'
--fm.BitmapFont.fontDirectory = "assets/fonts"
fm.FontManager:setEncoding("utf8")



Planes.Config   = Config
Planes.Audio    = {}

_W, _H = display.actualContentWidth, display.actualContentHeight
xMid, yMid = _W * .5, _H * .5

--[[
    Taking screenshot on S button. You can see screenshot in Document folder of your app.
    Press 'File'->'Show Project Sandbox' in simulator and then go to 'Document' folder
]]--
local function screenshot() 
    --I set the filename to be "widthxheight_time.png"
    --e.g. "1920x1080_20140923151732.png"
    local date = os.date( "*t" )
    local timeStamp = table.concat({date.year .. date.month .. date.day .. date.hour .. date.min .. date.sec})
    local fname = display.pixelWidth.."x"..display.pixelHeight.."_"..timeStamp..".png"
    
    --capture screen
    local capture = display.captureScreen(false)

    --make sure image is right in the center of the screen
    capture.x, capture.y = display.contentWidth * 0.5, display.contentHeight * 0.5

    --save the image and then remove
    local function save()
        display.save( capture, { filename=fname, baseDir=system.DocumentsDirectory, isFullResolution=true } )    
        capture:removeSelf()
        capture = nil
    end
    timer.performWithDelay( 100, save, 1)
            
    return true               
end


--works in simulator too
local function onKeyEvent(event)
    if event.phase == "up" then
        --press s key to take screenshot which matches resolution of the device
            if event.keyName == "s" then
                print'taking screenshot'
                screenshot()
            end
        end
end

Runtime:addEventListener("key", onKeyEvent)


-- show bg

--display.setDefault('minTextureFilter', 'nearest')
--display.setDefault('magTextureFilter', 'nearest')



composer.gotoScene('scripts.scenes.boot')
