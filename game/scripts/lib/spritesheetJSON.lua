local spritesheetJSON = {
    jsonArray2corona = function (json)
        -- add conversion logic here
        local coronaTable = { frames = {} }
        for i = 1, #json.frames do
            table.insert( coronaTable.frames, {
                x       = json.frames[i].frame.x,
                y       = json.frames[i].frame.y,
                width   = json.frames[i].frame.w,
                height  = json.frames[i].frame.h,
            })
        end
        return coronaTable
    end,
    getFrameIndex = function (json, frameName)
        -- search logic here
        for i = 1, #json.frames do
            if( json.frames[i].filename == frameName ) then
                return i
            end
        end
        return nil
    end,
    getFramesIndexes = function (json, indexes)
        -- search logic here
        local coronaFrames = {}
        for i = 1, #indexes do
            local frame = spritesheetJSON.getFrameIndex(json, indexes[i])
            if( frame ) then
                table.insert( coronaFrames, frame )
            end
        end
        return #coronaFrames ~= 0 and coronaFrames or nil
    end
}


function spritesheetJSON.new(jsonString)
    -- create new instance
    local jsonSheet = {}
    function jsonSheet:create(json)
        self.json = json
    end
    function jsonSheet:getSheet()
        return spritesheetJSON.jsonArray2corona(self.json)
    end
    function jsonSheet:getFrameIndex(frameName)
        return spritesheetJSON.getFrameIndex(self.json, frameName)
    end
    function jsonSheet:getFramesIndexes(indexes)
        return spritesheetJSON.getFramesIndexes(self.json, indexes)
    end

    -- init new instance
    jsonSheet:create( json.decode(jsonString) )
    return jsonSheet
end

return spritesheetJSON
