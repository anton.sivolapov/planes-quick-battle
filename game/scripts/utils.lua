local runtime = 0
-- print shortcut
p = function(...)
    return print(inspect(...))
end
function printf(...)
   local function wrapper(...) io.write(string.format(...)) end
   local status, result = pcall(wrapper, ...)
   if not status then error(result, 2) end
end

-- split string by pattern
function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
     table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function fitImage( displayObject, fitWidth, fitHeight, enlarge )
    --
    -- first determine which edge is out of bounds
    --
    local scaleFactor = fitHeight / displayObject.height 
    local newWidth = displayObject.width * scaleFactor
    if newWidth > fitWidth then
        scaleFactor = fitWidth / displayObject.width 
    end
    if not enlarge and scaleFactor > 1 then
        return
    end
    displayObject:scale( scaleFactor, scaleFactor )
end

local math_deg = math.deg  --localize 'math.deg' for better performance
local math_atan2 = math.atan2  --localize 'math.atan2' for better performance

function flipBody( physicsBody, scaleX, scaleY )

    local flippedBodyShapes = {}
    local scaleX = scaleX or 1
    local scaleY = scaleY or 1
    local thisBody = _m.clone(physicsBody)

    for i=1,#thisBody do

        local thisShape = thisBody[i].shape
        local flippedShape = {}

        for k=#thisShape,1,-2 do
            local pairY = thisShape[k]
            local pairX = thisShape[k-1]
            flippedShape[#flippedShape+1] = pairX*scaleX
            flippedShape[#flippedShape+1] = pairY*scaleY
        end

        flippedBodyShapes[#flippedBodyShapes+1] = thisBody[i]
        flippedBodyShapes[#flippedBodyShapes].shape = flippedShape
    end

    return flippedBodyShapes
end

function flipBodyX( thisBody )
    return flipBody( thisBody, -1, 1 )
end

function flipBodyY( thisBody )
    return flipBody( thisBody, 1, -1 )
end

function pack(...)
    return arg
end

-- extend
function math.pickRandom( array )
    return array[ math.random( 1, #array ) ]
end
function math.pick(...)
    return math.pickRandom(...)
end
function table.contains(t, val)
    local found = false
    table.foreach(t, function(_, v)
        if v == val then
            found = true
        end
    end)
    return found
end

function table.deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[table.deepcopy(orig_key)] = table.deepcopy(orig_value)
        end
        setmetatable(copy, table.deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

local utils = {
    forEachChildren = function(groupElement, cb)
        if not groupElement.numChildren then
            print'Warning: groupElement not a group'
        else 
            for i = 1, groupElement.numChildren do
                cb(groupElement[i])
            end
        end
    end,
    convertColor = function(colorTable)
        if colorTable then
            return {colorTable.r/255.0, colorTable.g/255.0, colorTable.b/255.0 }
        else
            return {1, 1, 1}
        end
    end,
    convertColorTable = function(colorTable)
        if colorTable then
            return {r = colorTable.r/255.0, g = colorTable.g/255.0, b = colorTable.b/255.0}
        else
            return {1, 1, 1}
        end
    end,
    chanceRoll = function(chance)
        if chance == nil then chance = 50 end

        local isPositive = chance > 0
        local chancePassed = (math.random() * 100 <= chance)

        return isPositive and chancePassed
    end,
    filterOutOfBorderElements = function(border, collection, offset)
        --- clear bullets when they reach end of the game screen
        for i=#collection,1,-1 do
            local el = collection[i];
            local out = utils.outOfBorder(
                border, el, offset
            )
            if out then
                table.remove(collection, i)
                el:removeSelf()
                el = nil
--                print('kill out of border', #collection)
            end
        end
    end,
    inCamera = function(point, offset)
        offset = offset or 0
        local _W, _H = display.actualContentWidth, display.actualContentHeight
        local sX, sY = point:localToContent(0, 0)
        local outLeft     = sX < 0
        local outRight    = sX > _W
        local outBottom   = sY > _H
        local outTop      = sY < 0

        if point.contentBounds then
            local bounds = point.contentBounds
            outLeft     = ( bounds.xMax + offset ) < 0
            outRight    = ( bounds.xMin - offset ) > _W
            outTop      = ( bounds.yMax + offset ) < 0
            outBottom   = ( bounds.yMin - offset ) > _H
        end

        local inCamera    = not ( outRight or outLeft or outBottom or outTop )

        return inCamera, {
            inCamera    = inCamera,
            outRight    = outRight,
            rightDist   = _W - sX,
            outLeft     = outLeft,
            leftDist    = sX,
            outTop      = outTop,
            topDist     = sY,
            outBottom   = outBottom,
            bottomDist  = _H - sY,
            cameraX     = sX,
            cameraY     = sY,
        }
    end,
    outOfBorder = function(border, sprite, offset)
        offset = offset or 0
        local left, right = (0 - offset), (border.width + offset)
        local top, bottom = (0 - offset), (border.height + offset)
        if sprite.x < left or sprite.x > right then
            return true
        end
        if sprite.y < top or sprite.y > bottom then
            return true
        end
        return false
    end,
    getFileContent = function (path)
        local content = nil
        -- Path for the file to read
        local path = system.pathForFile( path, system.ResourceDirectory )
        -- Open the file handle
        local file, errorString = io.open( path, "r" )
        if not file then
            -- Error occurred; output the cause
            print( "File error: " .. errorString )
        else
            -- Read data from file
            local contents = file:read( "*a" )
            -- Output the file contents
            content = contents
            -- Close the file handle
            io.close( file )
        end

        file = nil
        return content
    end,
    getJSON = function (path)
        local rawContent = utils.getFileContent(path)
        return json.decode(rawContent)
    end,
    angleBetweenTwoPoints = function ( x1, y1, x2, y2 )

        local angle = math_deg(math_atan2(y2 - y1, x2 - x1))
--        local angle = ( math_deg( math_atan2( dstY-srcY, dstX-srcX ) ) ) --; return angle
--        if ( angle < 0 ) then angle = angle + 360 end ; return angle % 360

        return angle
    end,
    parseColliderObj = function (obj1, obj2, type)
        if obj1.type == type or obj2.type == type then
            if obj1.type == type then
                return obj1, obj2
            else
                return obj2, obj1
            end
        else
            return false
        end
    end,
    openPaymentModal = function(params)
        local options = {
            isModal = true,
            effect = "fade",
            time = 500,
            params = params
        }
        composer.showOverlay( "scripts.scenes.message", options )
    end,
    buyItem     = function(params)
        if not utils.desktopApp() then
            local store         = require( "plugin.google.iap.v3" )
            local purchaseCallback = params.onPurchased
            local cancelCallback = params.onCanceled or function() end

            local transactionCallback = function(event)
                local transaction = event.transaction

                if ( transaction.state == "purchased" ) then
                    print( "Transaction succuessful!" )
                    purchaseCallback()

                elseif ( transaction.state == "consumed" ) then

                    print( "Transaction consumed:" )
                    print( "product identifier", transaction.productIdentifier )
                    print( "receipt", transaction.receipt )
                    print( "transaction identifier", transaction.identifier )
                    print( "date", transaction.date )
                    print( "original receipt", transaction.originalReceipt )
                    print( "original transaction identifier", transaction.originalIdentifier )
                    print( "original date", transaction.originalDate )

                    purchaseCallback()
                elseif ( transaction.state == "cancelled" ) then
                    print( "User cancelled transaction." )
                    cancelCallback()
                elseif ( transaction.state == "failed" ) then
                    print( "Transaction failed:", transaction.errorType, transaction.errorString )
                    if transaction.errorType == 7 then
                        -- already owned
                        purchaseCallback()
                    end

                else
                    print( "(unknown event)" )
                end
            end

            store.init( 'google', transactionCallback )

            local env = system.getInfo( 'environment' )
            if env == 'simulator' then
                print('================= runing in simulator, run callback with success state')
                transactionCallback( {
                        transaction = {
                            state = 'purchased'
                        }
                })
            else
                store.purchase( params.product_id )
            end
            print('================= END Debuging part')
        end
    end,
    pulsating   = function(el)
        local function1, function2
        local trans

        function function1(e)
            trans = transition.to(
                el,
                {
                    time=200,
                    xScale=1.01,
                    yScale=1.01,
                    onComplete=function2
                }
            )
        end
        function function2(e)
            trans = transition.to(
                el,
                {
                    time=200,
                    xScale=1,
                    yScale=1,
                    onComplete=function1
                }
            )
        end

        function1()
        return trans
    end,

    newTileSprite = function(width, height, sheet, frame)
        local tileSprite = display.newGroup()
        tileSprite.anchorChildren = true
        -- insert sprite first time
        local firstImage = display.newImage(sheet, frame)
        local oneWidth = firstImage.width
        firstImage:removeSelf()
        
        local numSprites = math.ceil(width / oneWidth)

        for i = 1, numSprites do
            local nextImage = display.newImage(sheet, frame)
            nextImage.anchorX = 0
            nextImage.x = tileSprite.width
            tileSprite:insert(nextImage)
        end

        return tileSprite
    end,
    placeNavigation = function(self, conf )
        local widget     = require 'widget'
        local nextButton = Planes.graphics.getFrame('button_forward.png')
        local prevButton = Planes.graphics.getFrame('button_back.png')
        local y = 50
        local forward = nil

        local back = widget.newButton({
            x = 50,
            y = y,
            sheet = prevButton.sheet,
            defaultFrame    = prevButton.frame,
            overFrame       = prevButton.frame,
            onPress       = function ()
                audio.play(Planes.Audio.click)
            end,
            onRelease       = function ()
                composer.gotoScene(conf.prevState)
            end
        })
        back.anchorX = 0
        back.anchorY = 0

        if conf.nextState then
            forward = widget.newButton({
                    x = _W - 50,
                    y = y,
                    sheet = nextButton.sheet,
                    defaultFrame    = nextButton.frame,
                    overFrame       = nextButton.frame,
                    onRelease       = function ()
                        audio.play(Planes.Audio.click)
                        composer.gotoScene(conf.nextState)
                    end
            })
            forward.anchorX = 1
            forward.anchorY = 0
            self.layers.interfaceLayer:insert(forward)
        end
        

        if conf.mainText then
            local style = Planes.Config.subTitleStyle
            local titleText = display.newBitmapText(
                conf.mainText,
                xMid,
                back.y,
                style.font,
                style.size
            )
            self.layers.interfaceLayer:insert(titleText)
        end
        if conf.title then
            self.layers.interfaceLayer:insert(conf.title)
            conf.title.anchorY = 0
            conf.title.x = xMid
            conf.title.y = y
        end
        

        self.layers.interfaceLayer:insert(back)

        return {
            hideForwardButton = function()
                if forward then
                    forward.isVisible = false
                end
            end,
            showForwardButton      = function()
                if forward then
                    forward.isVisible = true
                end
            end
        }
    end,
    getDeltaTime = function()
        local temp = system.getTimer()  -- Get current game time in ms
        local dt = (temp-runtime) / (1000/60)  -- 60 fps or 30 fps as base
        runtime = temp  -- Store game time
        return dt
    end,
    desktopApp = function()
        local platformName = system.getInfo( "platformName" )
        return ( platformName == "Mac OS X" ) or ( platformName == "Win" )
    end,
    trackCurrentScene = function()
        -- Log the screen/scene name with Google Analytics
        local sceneName = composer.getSceneName( "current" )
        -- flurryAnalytics.logEvent( "Enter scene", { location=sceneName } )
    end,
    goToPrevScene = function(scene)
        return function(event)
            if ( event.keyName == "back" ) then
                if event.phase == 'up' then
                    local platformName = system.getInfo( "platformName" )
                    if ( platformName == "Android" ) or ( platformName == "WinPhone" ) then
                        composer.gotoScene(scene)
                        return true
                    end
                end
            end
            return false
        end
    end,

    enableMusic = function()
        audio.setVolume(.5, {
                            channel = Planes.Config.audioChannels.bgMusic
        })
    end,
    disableMusic = function()
        audio.setVolume(0, {
                            channel = Planes.Config.audioChannels.bgMusic
        })
    end,
    initMusic = function(gameState)
        if not audio.isChannelPlaying(1) then
            audio.play(Planes.Audio.ui, {
                           channel = Planes.Config.audioChannels.bgMusic,
                           loops = -1,
            })
        end
        if gameState.enableMusic then
            utils.enableMusic()
        else
            utils.disableMusic()
        end
    end,
    loadAd = function(...)
        local state = Store.getState()
        if not utils.desktopApp() and not state.fullVersion then
            Planes.ads.load(...)
        end
    end,
    showAd = function(appId)
        local state = Store.getState()
        if not utils.desktopApp() and not state.fullVersion then
            if Planes.ads.isLoaded( "interstitial" ) then
                Planes.ads.show(
                    "interstitial",
                    { appId = appId, testMode = Planes.adsConfig.testMode }
                )
            end
        end
    end,
    networkConnection = function()
        local socket = require("socket")
        local test = socket.tcp()
        test:settimeout(2000)  -- Set timeout to 1 second
        local netConn = test:connect("www.google.com", 80)
        if netConn == nil then
            return false
        end
        test:close()
        return true
    end,
    createSceneLayers = function( scene, camera )
        local perspective   = require 'scripts.vendor.perspective'
        local camera        = perspective.createView()
--[[        camera:appendLayer()
        camera:appendLayer()
]]
        local layers = {
            backgroundLayer     = camera:layer(8),
            backgroundDetails1  = camera:layer(7),
            backgroundDetails2  = camera:layer(6),
            backgroundDetails3  = camera:layer(5),
            backgroundDetails4  = camera:layer(4),
            backgroundDetail    = camera:layer(3),
            playerLayer         = camera:layer(2),
            frontLayer          = camera:layer(1),
            interfaceLayer      = display.newGroup(),
            pauseLayer          = display.newGroup(),
            superFrontLayer     = display.newGroup(),
        }
        layers.backgroundDetails1.xParallax = .2
        layers.backgroundDetails2.xParallax = .5

        layers.backgroundDetails3.xParallax = .7

        layers.backgroundDetails4.xParallax = .9
        layers.backgroundDetails4.yParallax = .9

        scene.view:insert(layers.backgroundLayer)
        scene.view:insert(layers.backgroundDetails1)
        scene.view:insert(layers.backgroundDetails2)
        scene.view:insert(layers.backgroundDetails3)
        scene.view:insert(layers.backgroundDetails4)
        scene.view:insert(layers.backgroundDetail)
        scene.view:insert(layers.playerLayer)
        scene.view:insert(layers.frontLayer)
        scene.view:insert(layers.interfaceLayer)
        scene.view:insert(layers.pauseLayer)

        return layers, camera
    end
}

    --[[
    local function tileSprite1_deprecated(x, y, width)
        -- insert sprite first time
        local firstImage = display.newImage("assets/graphics/grass.png")
        local oneWidth = firstImage.width
        local oneHeight = firstImage.height
        firstImage:removeSelf()
        print('first sprite inserted. width', oneWidth)

        display.setDefault( "textureWrapX", "repeat" )
        local ground  = display.newRect( x, y, width, oneHeight )
        ground.fill = {
            type="image",
            filename="assets/graphics/grass32.png"
        }
        ground.fill.scaleX = .02
        display.setDefault( "textureWrapX", "clampToEdge" )

        return ground
    end
    ]]--
return utils
