local ObjectFactory = {}
ObjectFactory.allObjects = {}

local function create( factory, objectName, ... )
    local contructors = {}
    contructors.Enemy         = require 'scripts.objects.Enemy'
    contructors.Plane         = require 'scripts.objects.Plane'
    contructors.Player        = require 'scripts.objects.Player'
    contructors.Border        = require 'scripts.objects.Border'
    contructors.Bullet        = require 'scripts.objects.Bullet'
    contructors.Pointer       = require 'scripts.objects.Pointer'
    contructors.Bullet        = require 'scripts.objects.Bullet'
    contructors.Item          = require 'scripts.objects.Item'
    contructors.Cloud         = require 'scripts.objects.Cloud'
    contructors.Blast         = require 'scripts.objects.Blast'
    contructors.GroundBlast   = require 'scripts.objects.GroundBlast'
    contructors.Timer         = require 'scripts.objects.Timer'
    contructors.Image         = require 'scripts.objects.Image'
    contructors.ImageRect     = require 'scripts.objects.ImageRect'

    if contructors[objectName] then
        local obj = contructors[objectName]:new(...)
        obj._name = objectName
        table.insert(factory.allObjects, obj)
--        printf('Create %s. Total objects %s', obj._name, #factory.allObjects)
        return obj
    else
        printf(
            'ObjectFactory: can\'t find constructor for "%s"',
            objectName
        )
        return false
    end
end

local function onFrame(object)
    local scene = ObjectFactory.scene

    if object.shouldBeKilled then
        return false
    end

    if object.outOfBoundsKill then
        local border = scene.gameWorld
        local out = utils.outOfBorder(
            border, object, 200
        )
        if out then
--            printf('kill object "%s" since out of border', object._name)
            return false
        end
    end

    if object.update then
        object:update()
    end

    return object
end

local function updateAll(factory)
    if factory.paused then
        return
    end

    local collection = factory.allObjects

    for i=#collection, 1, -1 do
        local el = collection[i]
        local res = onFrame(el)
        if not res then
            table.remove(collection, i)

--[[            if el.onKill then
                el:onKill()
            end]]

            if el.removeSelf then
                el:removeSelf()
            end
            el = nil
        end
    end
end

local function pauseAll(factory)
    factory.paused = true
    local function pauseObject( object )
        if object._pause then
            object:_pause()
        elseif object.pause then
            object:pause()
        end

        if object.onPause then
            object:onPause()
        end
    end
    _.each(factory.allObjects, pauseObject)
end

local function resumeAll(factory)
    factory.paused = false
    local function resumeObject( object )
        if object._play then
            object:_play()
        elseif object.play then
            object:play()
        end
        if object.onResume then
            object:onResume()
        end
    end
    _.each(factory.allObjects, resumeObject)
end

local function kill(factory, object)
    -- mark object for removing, object will be actually removed on next update tick
    object.shouldBeKilled = true
end

local function killAll(factory, object)
    -- mark object for removing, object will be actually removed on next update tick
    _.each(factory.allObjects, function(el)
        ObjectFactory:kill(el)
    end)
end

local function removeAll(factory, object)
    -- mark object for removing, object will be actually removed on next update tick
    _.each(factory.allObjects, function(el)
        if el.removeSelf then
            el:removeSelf()
        end
    end)
    factory.allObjects = {}
end

ObjectFactory.updateAll     = updateAll
ObjectFactory.pauseAll      = pauseAll
ObjectFactory.resumeAll     = resumeAll
ObjectFactory.create        = create
ObjectFactory.kill          = kill
ObjectFactory.killAll       = killAll
ObjectFactory.removeAll     = removeAll
return ObjectFactory
