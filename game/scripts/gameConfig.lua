local versionStr = (system.getInfo( 'appVersionString' ) == '') and '1.0.24a' or system.getInfo( 'appVersionString' )
local Config = {
    version             = versionStr,
    googleStoreUrl      = 'https://play.google.com/store/apps/details?id=com.cheese.life.planes.free',
    mainBtn             = 'button-grey/grey_button01.png',
    mainBtnPressed      = 'button-grey/grey_button02.png',
    physicsDrawMode     = 'normal',
    numberOfFreeLevels  = 3,
    audioChannels       = {
        bgMusic         = 1,
    },
    physics             = {
        planeCategory       = 2,
        planeMask           = 1,
        groundCategory      = 1,
        interactiveCategory = 3,
        onlyPlaneMask       = 2,
        allMask             = 7,
    },
    mainTitleStyle      = {
        font    = 'Souse0',
        size    = 60,
        fill    = { r=81, g=161, b=255 },
        stroke  = { r=2, g=60, b=127 },
        strokeThickness = 2
    },
    subTitleStyle   = {
        font    = 'pfsquaresanspro',
        size    = 30,
        fill    = { r=255, g=204, b=0 },
        stroke  = { r=191, g=153, b=0 },
        shadow  = { r=0, g=0, b=0, a=.5 },
        strokeThickness = 5
    },
    mainTitleSmallStyle   = {
        font    = 'obelix-blue',
        size    = 60,
        fill    = {
            -- r=255, g=204, b=0
        },
        stroke  = { r=191, g=153, b=0 },
        strokeThickness = 2
    },
    menuStyle   = {
        font    = 'pfsquaresanspro',
        size    = 20,
        fill    = { r=255, g=204, b=0 },
        stroke  = { r=191, g=153, b=0 },
        strokeThickness = 1
    },
    mainTextStyle   = {
        font    = 'souse',
        size    = 60,
        fill    = { r=255, g=204, b=0 },
        stroke  = { r=191, g=153, b=0 },
        strokeThickness = 1
    },
    popupTextStyle   = {
        font    = 'souse',
        size    = 50,
        fill    = { r=255, g=204, b=0 },
        stroke  = { r=191, g=153, b=0 },
        strokeThickness = 1
    },
    techTextStyle   = {
        font    = 'opensans',
        size    = 20,
        fill    = { r=102, g=102, b=102 },
    },
    cloudSpeed  = 20,
    regularBlastAnimation   = {
        'big_explosion/Boom2_1.png',
        'big_explosion/Boom2_2.png',
        'big_explosion/Boom2_3.png',
        'big_explosion/Boom2_4.png',
        'big_explosion/Boom2_5.png',
        'big_explosion/Boom2_6.png',
        'big_explosion/Boom2_7.png',
        'big_explosion/Boom2_8.png',
        'big_explosion/Boom2_9.png'
    },
    groundBlastAnimation    = {
        'big_ground_explosion/Boom_3_1.png',
        'big_ground_explosion/Boom_3_2.png',
        'big_ground_explosion/Boom_3_3.png',
        'big_ground_explosion/Boom_3_4.png',
        'big_ground_explosion/Boom_3_5.png',
        'big_ground_explosion/Boom_3_6.png',
        'big_ground_explosion/Boom_3_7.png',
        'big_ground_explosion/Boom_3_8.png',
        'big_ground_explosion/Boom_3_9.png'
    },
    simpleBlastAnimation   = {
        'small_explosion/Boom_1.png',
        'small_explosion/Boom_2.png',
        'small_explosion/Boom_3.png',
        'small_explosion/Boom_4.png',
        'small_explosion/Boom_5.png',
        'small_explosion/Boom_6.png',
        'small_explosion/Boom_7.png',
        'small_explosion/Boom_8.png',
        'small_explosion/Boom_9.png'
    }
}

Config.bigNotificationStyle = _.extend(table.deepcopy(Config.mainTitleSmallStyle), {
    -- fill    = { r=0, g=0, b=155 }
});

Config.gameOverStyle = _.extend(table.deepcopy(Config.mainTitleSmallStyle), {
        font    = 'obelix-red',
    -- fill    = { r=10, g=0, b=0 }
});

Config.winLevelStyle = _.extend(table.deepcopy(Config.mainTitleSmallStyle), {
    fill    = { r=0, g=204, b=0 }
});

Config.supportedLocales = {}
Config.supportedLocales['en']    = 'en_US'
Config.supportedLocales['ru']    = 'ru_RU'
Config.supportedLocales['be']    = 'ru_RU'
Config.supportedLocales['uk']    = 'ru_RU'
Config.supportedLocales['русский']  = 'ru_RU'

return Config
