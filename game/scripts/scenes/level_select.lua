print 'Loading level select scene..'

local scene         = composer.newScene()
local widget        = require 'widget'
local Store         = require 'scripts.Store'

local physics = require("physics")
scene.physics = physics

local prevScene = 'scripts.scenes.character_select'
local goToPrevScene = utils.goToPrevScene(prevScene)


function init(scene)
    local state = Store.getState()

    ObjectFactory.scene = scene

    scene.layers, scene.camera = utils.createSceneLayers(scene)

    local bg = ObjectFactory:create(
        'Image',
        scene.layers.interfaceLayer,
        Planes.graphics.getFrame('ui_background.png').sheet,
        Planes.graphics.getFrame('ui_background.png').frames
    )

    bg.width    = _W
    bg.height   = _H
    bg.anchorX  = 0
    bg.anchorY  = 0

    utils.initMusic(state)
    utils.trackCurrentScene()
end


function createLevelGroup(levelNum, levelJustOpened)
    local state = Store.getState()
    local openedLevels = state.openedLevels

    local levelOpened = table.contains(openedLevels, levelNum)

    local levelGroup = display.newGroup()
    levelGroup.anchorChildren = true
    local diaFramesNames = {
        'diaphragm/diaphragm1.png',
        'diaphragm/diaphragm2.png',
        'diaphragm/diaphragm3.png',
        'diaphragm/diaphragm4.png'
    }
    local diaFrames = Planes.graphics.getFrame(diaFramesNames)

    local openSequence = {
        name    = 'open',
        sheet   = diaFrames.sheet,
        time    = 300,
        frames  = diaFrames.ids,
        loopCount = 1
    }
    local closeSequence = {
        name    = 'close',
        sheet   = diaFrames.sheet,
        time    = 300,
        frames  = _m.reverse(diaFrames.ids),
        loopCount = 1
    }
    local sequenceData = {
        closeSequence,
        openSequence
    }
    local diaphragm = display.newSprite(
        diaFrames.sheet, 
        sequenceData
    )
    -- all dias closed by default
    diaphragm.opened = false
    diaphragm.levelNum = levelNum

    diaphragm:setSequence( "open" )

    local startLevelButtonBottom = ObjectFactory:create(
        'Image',
        Planes.graphics.getFrame('start_level_button_bottom.png').sheet,
        Planes.graphics.getFrame('start_level_button_bottom.png').frames
    )

    local frame1 = Planes.graphics.getFrame(_s('start_level_button_off.png'))
    local frame2 = Planes.graphics.getFrame(_s('start_level_button_on.png'))

    local startLevelButton = widget.newButton({
            label = '',
            sheet = frame1.sheet,
            x = startLevelButtonBottom.x,
            y = startLevelButtonBottom.y,
            defaultFrame = frame1.frames,
            overFrame = frame2.frames,
            onRelease = function ()

                audio.play(Planes.Audio.click);
                if levelOpened and diaphragm.opened then
                    if levelNum == 1 then
                        local options = {
                            isModal = true,
                            effect = "fromRight",
                            time = 700,
                            params = {
                                nextScene = 'scripts.scenes.game',
                                sceneOptions = {
                                    params = { level=levelNum }
                                }
                            }
                        }
                        composer.showOverlay( "scripts.scenes.tutorial", options )
                    else
                        local options = {
                            params = { level=levelNum }
                        }
                        composer.gotoScene('scripts.scenes.game', options)
                    end
                end
            end
    })
    startLevelButton:setEnabled(false)


    local levelNumFile = 'N' .. levelNum .. '.png'
    local levelNumImage = ObjectFactory:create(
        'Image',
        Planes.graphics.getFrame(levelNumFile).sheet,
        Planes.graphics.getFrame(levelNumFile).frames
    )
    local function openDia()
        if levelOpened then
            if not diaphragm.opened then

                diaphragm.opened = true
                diaphragm:setSequence( "open" )
                diaphragm:play()
                levelNumImage.isVisible = false
                -- notify other elements, that one level selected
                Runtime:dispatchEvent{ name='levelSelected', levelNum=levelNum}
                startLevelButton:setEnabled(true)
            end
        end
    end

    levelGroup.openDia = openDia
    
    local function closeDia()
        if levelOpened then
            if diaphragm.opened then
                diaphragm.opened = false
                diaphragm:setSequence( "close" )
                diaphragm:play()
                levelNumImage.isVisible = true
                startLevelButton:setEnabled(false)
            else
                -- print(levelNum, 'already closed')
            end
        end
    end
    local function tapOnLevel(event)
        if event.phase == "ended" then
            openDia()
        end
    end

    local function closeNonSelected(evt)
        if evt.levelNum ~= levelNum then
            closeDia()
        end
    end

    scene.allListeners = _m.push( scene.allListeners, closeNonSelected )

    Runtime:addEventListener('levelSelected', closeNonSelected)
    diaphragm.isHitTestable = true
    diaphragm:addEventListener( "touch", tapOnLevel)
    
    local lock = ObjectFactory:create(
        'Image',
        Planes.graphics.getFrame('lock.png').sheet,
        Planes.graphics.getFrame('lock.png').frames
    )

    local levelFrame = ObjectFactory:create(
        'Image',
        Planes.graphics.getFrame('El_Level.png').sheet,
        Planes.graphics.getFrame('El_Level.png').frames
    )

    -- insert stuff
    levelGroup:insert(startLevelButtonBottom)
    levelGroup:insert(startLevelButton)
    levelGroup:insert(diaphragm)

    levelGroup:insert(lock)
    if levelOpened and not levelJustOpened then
        lock.isVisible = false
    end

    levelGroup:insert(levelNumImage)
    levelGroup:insert(levelFrame)

    -- level just opened? if so, animate opening
    if levelJustOpened then
        lock:toFront()
        lock.rotation = -15
        transition.to(
            lock,
            {
                rotation = 15,
                xScale = .3,
                yScale = .3,
                time = 1500,
                onComplete = function()
                    lock:removeSelf()
                    openDia()
                end,
                transition=easing.inBounce
            }
        )
    end

    return levelGroup
end

function placeTableWithLevels(newLevelToOpen)
    local ROWS = 2
    local COLS = 3
    local tGroup = display.newGroup()
    tGroup.anchorChildren = true

    local vPadding = 300
    local hPadding = 300
    local levelNum = 1

    for i = 1, ROWS do
        for y = 1, COLS do
            local levelJustOpened = newLevelToOpen == levelNum
            local levelGroup = createLevelGroup(levelNum, levelJustOpened)
            levelGroup.x = y * hPadding
            levelGroup.y = i * vPadding
            tGroup:insert(levelGroup)
            levelNum = levelNum + 1
        end
    end
    tGroup.x = xMid
    tGroup.y = yMid + 50
    scene.layers.interfaceLayer:insert(tGroup)
end

function scene:show( event )
    local phase = event.phase
    local params = event.params or {
        newLevelToOpen = nil
    }
    local newLevelToOpen = params.newLevelToOpen

    if ( phase == "will" ) then
        scene.allListeners = {}
        -- Called when the scene is still off screen (but is about to come on screen).
        init(scene)

        local title = ObjectFactory:create(
            'Image',
            scene.layers.interfaceLayer,
            Planes.graphics.getFrame(_s('choose_level.png')).sheet,
            Planes.graphics.getFrame(_s('choose_level.png')).frames
        )

        self.placeNavigation = utils.placeNavigation
        self:placeNavigation({
            prevState   = prevScene,
            title       = title
        })
        Runtime:addEventListener( "key", goToPrevScene )

        placeTableWithLevels(newLevelToOpen)

        local state = Store.getState()

        local function askToRateApp()
            local function onComplete2(event)
                if "clicked" == event.action then
                    if event.index == 1 then
                        flurryAnalytics.logEvent( "Like app - rate app" )
                        local setAskedForRating = function(state) 
                            state.askedForRating = true
                            return state
                        end
                        Store.updateStore(setAskedForRating)

                        system.openURL( Planes.Config.googleStoreUrl )
                    else
                        flurryAnalytics.logEvent( "Like app - not rate app" )
                    end
                end
            end
            local function onComplete3(event)
                if "clicked" == event.action then
                    if event.index == 1 then
                        flurryAnalytics.logEvent( "Dislike app - rate app" )
                        local setAskedForRating = function(state) 
                            state.askedForRating = true
                            return state
                        end
                        Store.updateStore(setAskedForRating)

                        system.openURL( Planes.Config.googleStoreUrl )
                    else
                        flurryAnalytics.logEvent( "Dislike app - not rate app" )
                    end
                end
            end

            local function onComplete( event )
                if "clicked" == event.action then
                    if event.index == 1 then
                        timer.performWithDelay(1, function()
                            native.showAlert(
                                _s("Visit us on Store"),
                                _s("How about a rating on Webstore, then?"),
                                { _s("Ok, sure"), _s("No, thanks") },
                                onComplete2
                            )
                        end)
                    else
                        timer.performWithDelay(1, function()
                            native.showAlert(
                                _s("Visit us on Store"),
                                _s("Would you mind giving us some feedback"),
                                { _s("Ok, sure"), _s("No, thanks") },
                                onComplete3
                            )
                        end)
                    end
                end
            end

            local alert = native.showAlert(
                _s("How is going?"),
                _s("Enjoying Planes Battle?"),
                { _s("Yes!"), _s("Not Really") },
                onComplete
            )
        end
        
        if params.levelWin then
            local allowed = ( state.numberOfRuns % 3 ) == 0 and not state.askedForRating
            if not Planes.askedForRateInSession and allowed then
                Planes.askedForRateInSession = true
                askToRateApp()
            end
        end
    end
end

function scene:hide( event )
    local phase = event.phase
    if ( phase == "will" ) then
        Runtime:removeEventListener( "enterFrame", update )
        Runtime:removeEventListener( "collision", handleCollision )
        Runtime:removeEventListener( "key", goToPrevScene )
        ObjectFactory:removeAll()

        self.camera.cancel()

        _m.each(scene.allListeners, function(i, listener)
            Runtime:removeEventListener( "levelSelected", self.allListeners[i] )
        end)

        for key, layer in pairs(self.layers) do
            layer:removeSelf()
            layer = nil
        end
    end
end

function scene:destroy( event )
end

function scene:onPurchaseAccepted()
    local levelToOpen = Planes.Config.numberOfFreeLevels + 1
    local state = Store.getState()
    _m.push(state.openedLevels, levelToOpen)
    Store.setState(state)

    composer.gotoScene('scripts.scenes.level_select', {
        params = {
            newLevelToOpen = levelToOpen
        }
    })
end

scene:addEventListener('create' , scene)
scene:addEventListener('show'   , scene)
scene:addEventListener('hide'   , scene)
scene:addEventListener('destroy', scene)

return scene
