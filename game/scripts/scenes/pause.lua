local composer      = require( "composer" )
local widget        = require 'widget'

local scene         = composer.newScene()

-- Called when the scene's view does not exist:
function scene:show( event )
    if ( event.phase == "will" ) then
        -- print('== create pause overlay ==')
        local layer = event.parent.layers.pauseLayer
        local scene = event.parent

        local backgroundOverlay = display.newRect (0, 0, 10000, 10000)
        backgroundOverlay:setFillColor( black )
        backgroundOverlay.alpha = 0.6
        backgroundOverlay.isHitTestable = true
--        backgroundOverlay:toFront()

        local frame1 = Planes.graphics.getFrame(_s('Pause/button_exit_off.png'))
        local frame2 = Planes.graphics.getFrame(_s('Pause/button_exit_on.png'))
        local exitButton = widget.newButton({
            label = '',
            x = xMid,
            y = yMid + 150,
            sheet = frame1.sheet,
            defaultFrame = frame1.id,
            overFrame = frame2.id,
            onRelease = function ()
                -- print'exit clicked'
                audio.play(Planes.Audio.click);
                scene:togglePause()
                composer.gotoScene('scripts.scenes.home')
            end
        })

        local frame1 = Planes.graphics.getFrame(_s('Pause/button_continue_off.png'))
        local frame2 = Planes.graphics.getFrame(_s('Pause/button_continue_on.png'))
        local continueButton = widget.newButton({
            label = '',
            x = xMid,
            y = yMid - 150,
            sheet = frame1.sheet,
            defaultFrame = frame1.id,
            overFrame = frame2.id,
            onRelease = function ()
                -- print'continue clicked'
                audio.play(Planes.Audio.click);
                scene:togglePause()
            end
        })

        local frame1 = Planes.graphics.getFrame(_s('Pause/button_reset_off.png'))
        local frame2 = Planes.graphics.getFrame(_s('Pause/button_reset_on.png'))
        local resetButton = widget.newButton({
            label = '',
            x = xMid,
            y = yMid,
            sheet = frame1.sheet,
            defaultFrame = frame1.id,
            overFrame = frame2.id,
            onRelease = function ()
                -- print( 'reset clicked', scene.currentLevel )
                audio.play(Planes.Audio.click);

                scene:togglePause()

                local options = {
                    params = { level=scene.currentLevel }
                }
                composer.gotoScene('scripts.scenes.game', options)
            end
        })

        local buttonBottom = ObjectFactory:create(
           'Image',
            Planes.graphics.getFrame('Pause/button_bottom.png').sheet,
            Planes.graphics.getFrame('Pause/button_bottom.png').frames
        )
        buttonBottom.x = exitButton.x
        buttonBottom.y = exitButton.y + 5

        local buttonBottom1 = ObjectFactory:create(
           'Image',
            Planes.graphics.getFrame('Pause/button_bottom.png').sheet,
            Planes.graphics.getFrame('Pause/button_bottom.png').frames
        )
        buttonBottom1.x = continueButton.x
        buttonBottom1.y = continueButton.y + 5

        local buttonBottom2 = ObjectFactory:create(
           'Image',
            Planes.graphics.getFrame('Pause/button_bottom.png').sheet,
            Planes.graphics.getFrame('Pause/button_bottom.png').frames
        )
        buttonBottom2.x = resetButton.x
        buttonBottom2.y = resetButton.y + 5

        self.grp = display.newGroup()
        self.grp:insert(backgroundOverlay)

        self.grp:insert(buttonBottom)
        self.grp:insert(buttonBottom1)
        self.grp:insert(buttonBottom2)

        self.grp:insert(exitButton)
        self.grp:insert(continueButton)
        self.grp:insert(resetButton)

        self.view:insert(self.grp)

        layer:insert(self.view)
        scene.menuIcon:toFront()
        utils.trackCurrentScene()
    end
end

function scene:hide( event )
    if ( event.phase == "will" ) then
        -- print('=== hide pause ==')

        self.grp:removeSelf()

        -- Call the "resumeGame()" function in the parent scene
--        parent:resumeGame()
    end
end

-- By some method (a "resume" button, for example), hide the overlay
--composer.hideOverlay( "fade", 400 )

scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
return scene
