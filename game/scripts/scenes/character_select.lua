print 'Loading character select scene..'

local perspective   = require 'scripts.vendor.perspective'
local scene         = composer.newScene()
local widget        = require 'widget'
local Plane         = require 'scripts.objects.Plane'
local Store         = require 'scripts.Store'
local wheelChooser  = require 'scripts.wheelChooser'

local physics = require("physics")
local prevScene = 'scripts.scenes.home'
local goToPrevScene = utils.goToPrevScene(prevScene)

scene.physics = physics

scene.layers = {
}

scene.gameWorld = {
    width   = _W,
    height  = _H,
}

scene.statsIcons = {
    damage    = "damage_icon.png",
    speed     = "speed_icon.png",
    control   = "control_icon.png"
}

function init(scene)

    ObjectFactory.scene = scene

    scene.layers, scene.camera = utils.createSceneLayers(scene)

    local bg = ObjectFactory:create(
        'Image',
        scene.layers.interfaceLayer,
        Planes.graphics.getFrame('ui_background.png').sheet,
        Planes.graphics.getFrame('ui_background.png').frames
    )

    bg.width    = _W
    bg.height   = _H
    bg.anchorX  = 0
    bg.anchorY  = 0
end

function placeWheelChooser(scene, state, currentPlane, justOpened, paramsForNextScene)
    local items = {}
    local openedPlanes = state.openedPlanes
    local startIndex = 1

    _m.each(Planes.planesData, function(index, planeData)
                local planeOpened = _m.indexOf(openedPlanes, planeData.plane_id)
                local planeJustOpened = justOpened == planeData.plane_id

                if planeData.plane_id == currentPlane then
                    startIndex = index
                end

                local item = ObjectFactory:create(
                    'Image',
                    Planes.graphics.getFrame('EL_BIG.png').sheet,
                    Planes.graphics.getFrame('EL_BIG.png').frames
                )
                local plane = nil
                local itemg = display.newGroup()

                local createClosed = function()
                    local fileName = 'plane_' .. planeData.plane_id .. '_closed.png'
                    fileName = planeData.closedFrame or fileName
                    return ObjectFactory:create(
                        'Image',
                        Planes.graphics.getFrame(fileName).sheet,
                        Planes.graphics.getFrame(fileName).frames
                    )
                end

                local createOpened = function()
                    return ObjectFactory:create(
                        'Plane',
                        scene,
                        {
                            preview = true,
                            plane_id = planeData.plane_id,
                            orientation = 'E',
                            x = item.x,
                            y = item.y
                        }
                    )
                end

                if planeJustOpened and planeOpened then
                    print'just opened..'
                    local closedPlane = createClosed()

                    itemg:insert(closedPlane)
                    plane = createOpened()
                    plane.xScale = .2
                    plane.yScale = .2

                    
                    local disappearing = function()
                        closedPlane:removeSelf()
                        closedPlane = nil
                        -- transition.to(
                        --     plane,
                        --     {
                        --         xScale = 1,
                        --         yScale = 1 ,
                        --         time = 1000,
                        --         transition=easing.inOutBack
                        --     }
                        -- )
                    end

                    transition.to(
                        plane,
                        {
                            xScale = 1.5,
                            yScale = 1.5 ,
                            time = 1000,
                            onComplete = disappearing,
                            transition=easing.inOutBack
                        }
                    )
                elseif planeOpened then
                    plane = createOpened()
                    plane.xScale = 1.5
                    plane.yScale = 1.5
                else
                    plane = createClosed()
                    plane.closed = true
                    plane.planeData = planeData
                end

                itemg.anchorChildren = true
                itemg:insert(plane)
                itemg:insert(item)
                scene.layers.interfaceLayer:insert(itemg)
                itemg.plane_id  = planeData.plane_id
                itemg.plane     = plane

                _m.push(items, itemg)
    end)

    

    local style = Planes.Config.mainTextStyle
    local planeDescription = display.newBitmapText(
        {
            text    = '',
            align   = 'center',
            x       = xMid,
            y       = yMid + 55,
            font    = style.font,
            fontSize  = style.size
        }
    )
    planeDescription.anchorY = 1
    
    local statsGroup

    local goNext = function(e)
        if e.phase == 'began' then
            audio.play(Planes.Audio.click);
        end
        if e.phase == 'ended' then
            print'go to next scene..'
            composer.gotoScene('scripts.scenes.level_select', {
                                   params = paramsForNextScene
            })
            return false
        end
        return true
    end
    local openPlane = function(e)
        if e.phase == 'ended' then
            local plane_id = e.target.plane_id
            local product_id = 'plane_' .. plane_id
            print( 'buying', product_id )

            utils.buyItem({
                    text = 'hello',
                    product_id = product_id,
                    onPurchased = function()
                        flurryAnalytics.logEvent( "Plane purchased " .. plane_id )
                        print(plane_id, 'purchased')
                        local addPlane = function(state)
                            _m.push(state.openedPlanes, plane_id)
                            state.currentPlane = plane_id
                            return state
                        end

                        Store.updateStore(addPlane)
                        composer.gotoScene('scripts.scenes.character_select', {
                            params = {
                                justOpened = plane_id
                            }
                        })
                    end,
                    onCanceled = function()
                        flurryAnalytics.logEvent( "Cancel buy " .. plane_id )
                    end
            })
        end
    end
    local wheel = wheelChooser.init({
            x       = xMid,
            y       = 200,
            items       = items,
            startIndex  = startIndex,
            parentGroup = scene.layers.interfaceLayer,
            onSwipe     = function(selectedItem)
                print('selected: ',  selectedItem.plane_id)
                transition.cancel(selectedItem)
            end,
            onSelect    = function(selectedItem) 
                print('selected: ',  selectedItem.plane_id)

                selectedItem.trans = utils.pulsating(selectedItem)
                -- save new selected plane
                state['currentPlane'] = selectedItem.plane_id

                _m.each(items, function(index, item) 
                            if not item.plane.closed then
                                if item.plane.pause then
                                    item.plane:pause()
                                end
                                item:removeEventListener( "touch", goNext )
                            else
                                if item.bigLabel then
                                    item.bigLabel:removeEventListener( "touch", openPlane )
                                    item.bigLabel:removeSelf()
                                    item.bigLabel = nil
                                end
                                item:removeEventListener( "touch", openPlane )
                            end
                end)

                if not selectedItem.plane.closed then
                    selectedItem.plane:startPropeller()
                    planeDescription:setText(
                        selectedItem.plane.planeData.information.description
                    )
                else
                    planeDescription:setText(
                        _s('This plane is closed so far. Finish few levels to open it.')
                    )
                end


                if not selectedItem.plane.closed then
                    selectedItem:addEventListener( "touch", goNext )

                    if statsGroup then
                        statsGroup:removeSelf()
                    end
                    statsGroup = getStatsForPlane(scene, selectedItem.plane.planeData)

                    statsGroup.anchorY = 1
                    statsGroup.x = xMid
                    statsGroup.y = _H - 50
                    scene.layers.interfaceLayer:insert(statsGroup)

                    Store.setState(state)
                else

                    local style = Planes.Config.winLevelStyle
                    local bigLabel = display.newBitmapText(_s('or touch to open it now!'), xMid, _H - 150, style.font, 70)
                    bigLabel.plane_id = selectedItem.plane_id
                    selectedItem.bigLabel = bigLabel
                    bigLabel.rotation = -1 * math.random(0, 10)
                    bigLabel:setTintColor(style.fill.r,style.fill.g,style.fill.b)
                    scene.layers.interfaceLayer:insert(bigLabel)

                    local disappearing = function()

                        transition.to(
                         bigLabel,
                         {
                             xScale = 1,
                             yScale = 1,
                             rotation = -1 * math.random(0, 10),
                             time = 1000,
                             transition=easing.inOutBack
                         }
                        )
                    end

                    transition.to(
                        bigLabel,
                        {
                            xScale = 1.2,
                            yScale = 1.2 ,
                            rotation = math.random(0, 10),
                            time = 1000,
                            onComplete = disappearing,
                            transition=easing.inOutBack
                        }
                    )

                    selectedItem:addEventListener( "touch", openPlane )
                    bigLabel:addEventListener( "touch", openPlane )

                    if statsGroup then
                        statsGroup:removeSelf()
                    end
                end

                scene.layers.interfaceLayer:insert(planeDescription)
            end        
    })
end

function getStatsForPlane(scene, planeData)

    local stats = display.newGroup()
    stats.anchorChildren = true

    local createRow = function(index, stat)
        local fileName = scene.statsIcons[stat.name]
        local y = 60 * (index - 1)
        local row = display.newGroup()
        local statIcon = ObjectFactory:create(
            'Image',
            Planes.graphics.getFrame(fileName).sheet,
            Planes.graphics.getFrame(fileName).frames
        )
        statIcon.x = 0
        statIcon.y = y
        statIcon:scale(1.2,1.2)

        row:insert(statIcon)

        local statBar = ObjectFactory:create(
            'Image',
            Planes.graphics.getFrame('line/line.png').sheet,
            Planes.graphics.getFrame('line/line.png').frames
        )
        statBar.anchorX = 0
        statBar.x = 50
        statBar.y = y
        row:insert(statBar)
        
        for i=1, stat.points do
            -- insert green bars
            local fileName = 'line/' .. i .. '.png'
            local greenBar = ObjectFactory:create(
                'Image',
                Planes.graphics.getFrame(fileName).sheet,
                Planes.graphics.getFrame(fileName).frames
            )
            greenBar.x = statBar.x + 36 + (i - 1) * 57
            greenBar.y = y
            row:insert(greenBar)
        end

        stats:insert(row)
    end

    _m.each(planeData.information.stats, createRow)
    
    return stats
end

function scene:create()
end

function scene:show( event )
    local phase = event.phase
    scene.physics.start()
    scene.physics.setDrawMode( Planes.Config.physicsDrawMode )

    if ( phase == "will" ) then
        init(scene)
        self.placeNavigation = utils.placeNavigation
        self.navigation = self:placeNavigation({
            prevState   = prevScene
        })

        local state = Store.getState()
        local currentPlane = state['currentPlane']
        if event.params then
            currentPlane = event.params['justOpened'] or currentPlane
        end

        local justOpened = nil
        if event.params then
            justOpened = event.params['justOpened']
        end
        placeWheelChooser(scene, state, currentPlane, justOpened, event.params)

        Runtime:addEventListener( "key", goToPrevScene )
        utils.initMusic(state)
        utils.trackCurrentScene()
    end
end

function scene:hide( event )
    local phase = event.phase
    if ( phase == "will" ) then
        ObjectFactory:killAll()

        self.camera.cancel()

        for key, layer in pairs(self.layers) do
            layer:removeSelf()
            layer = nil
        end
        Runtime:removeEventListener( "key", goToPrevScene )
    end
end

function scene:destroy( event )
end

scene:addEventListener('create' , scene)
scene:addEventListener('show'   , scene)
scene:addEventListener('hide'   , scene)
scene:addEventListener('destroy', scene)

return scene
