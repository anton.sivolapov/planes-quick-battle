print 'Loading credits scene..'

local perspective   = require 'scripts.vendor.perspective'
local scene         = composer.newScene()
local widget        = require 'widget'
local physics       = require 'physics'
local Store         = require 'scripts.Store'


scene.physics = physics

scene.layers = {
}
-- all timers
scene.timers = {}

scene.gameWorld = {
    width   = _W,
    height  = _H,
}

function scene:create(evt)
end

-- Custom function for resuming the game (from pause state)
function scene:resumeGame()
    --code to resume game
end

function scene:show( evt )
    local phase = evt.phase

    if ( phase == "will" ) then

        local gameEndId = Planes.adsConfig.gameEnd.id
        utils.loadAd(
            "interstitial",
            { appId = gameEndId, testMode = Planes.adsConfig.testMode }
        )
        -- Called when the scene is still off screen (but is about to come on screen).
        self.physics.start()
        self.physics.setDrawMode( Planes.Config.physicsDrawMode )

        local params    = evt.params or {}
        local gameState = Store.getState()

        ObjectFactory.scene = scene

        self.layers, self.camera = utils.createSceneLayers(self)

        -- Initialize the scene here.
        -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        local bg = ObjectFactory:create(
            'Image',
            self.layers.interfaceLayer,
            Planes.graphics.getFrame('ui_background.png').sheet,
            Planes.graphics.getFrame('ui_background.png').frames
        )

        bg.width    = _W
        bg.height   = _H
        bg.anchorX  = 0
        bg.anchorY  = 0

        -- Planes.graphics.getFrame(_s('quick_planes_battle.png')).sheet,
        -- Planes.graphics.getFrame(_s('quick_planes_battle.png')).frames
        local title1 = ObjectFactory:create(
            'Image',
            self.layers.interfaceLayer,
            Planes.graphics.getFrame(_s('quick_planes_battle.png')).sheet,
            Planes.graphics.getFrame(_s('quick_planes_battle.png')).frames
        )

        title1.anchorY  = 0
        title1.x = xMid
        title1.y = 20

        local style = Planes.Config.mainTextStyle
        local txt = [[

Creator         : Anton Sivolapov
Graphics       : Vadim Kozin
                1artistman@gmail.com 
Tests           : dekasoftware 
                dekasoftware@rambler.ru

Thanks for playing game!
            ]]
        local text = display.newBitmapText(
            {
                text    = txt,
                x       = xMid + 50,
                y       = yMid + 120,
                font    = style.font,
                fontSize  = 50
            }
        )
        self.layers.interfaceLayer:insert( text )

        if not params.fromHomePage then
            local style = Planes.Config.winLevelStyle
            local bigLabel = display.newBitmapText(_s('You Win!'), xMid, 200, style.font, 120)
            bigLabel.rotation = -20
            bigLabel:setTintColor(style.fill.r,style.fill.g,style.fill.b)
            self.layers.interfaceLayer:insert( bigLabel )
        end
        

        local goHome = function()
            composer.gotoScene('scripts.scenes.home', {})
        end

        -- allow closing after 2 seconds
        timer.performWithDelay(1000, function()
                                   self.layers.interfaceLayer:addEventListener( "touch", goHome )
        end)


        -- version text
        local style = Planes.Config.techTextStyle
        local _versionText = 'Music CleytonRX. Created by Anton Sivolapov anton.sivolapov@gmail.com      version '
        _versionText = _versionText .. Planes.Config.version

        if gameState.fullVersion then
            _versionText = _versionText .. ' (full)'
        else
            _versionText = _versionText .. ' (free)'
        end
        _versionText = _versionText .. ' ' .. localize.locale

        local versionText = display.newEmbossedText(_versionText, _W - 20, _H - 20, style.font, style.size)
        versionText:setFillColor( unpack(utils.convertColor(style.fill)) )
        versionText.anchorX = 1
        versionText.anchorY = 1

        self.layers.interfaceLayer:insert( versionText )
        
        flurryAnalytics.logEvent( "Game finished" )
    elseif ( phase == "did" ) then
    end
end

function scene:hide( event )
    local phase = event.phase
    if ( phase == "will" ) then
        ObjectFactory:removeAll()
        scene.physics.stop()

        for key, layer in pairs(self.layers) do
            layer:removeSelf()
            layer = nil
        end
    end
end

function scene:destroy( event )
end

scene:addEventListener('create' , scene)
scene:addEventListener('show'   , scene)
scene:addEventListener('hide'   , scene)
scene:addEventListener('destroy', scene)

return scene
