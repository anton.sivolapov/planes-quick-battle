local composer      = require( "composer" )
local scene         = composer.newScene()
local Store         = require 'scripts.Store'
local loadingHints        = {
    _s('Flight through clouds to confuse your opponents!'),
    _s('Some bonuses takes permanent effect!'),
    _s('Dive closer to ground to take more speed!'),
    _s('You can leave battleground to be immediatelly turned back'),
    _s('Your enemies make damage each other - use it!')
}

function scene:show( evt )
    local phase = evt.phase
    if ( phase == "will" ) then

        local logo = display.newImage('assets/graphics/logo.png')
        -- logo:scale(.8,.8)
        logo.x = xMid
        logo.y = yMid - 100

        self.view:insert(logo)

        -- loading percentage
        local style = Planes.Config.techTextStyle

        local loadingText   = display.newEmbossedText(
            '', xMid, logo.y + 100, style.font, style.size
        )
        self.view:insert(loadingText)

        local loadingHintText   = display.newEmbossedText(
            '', xMid, loadingText.y + 60, style.font, style.size
        )
        self.view:insert(loadingHintText)

        local updateProgressText = function(prc)
            local _loadingTemplate = _s('Loading ( %s%% ) ..')
            local _loadingText  = _loadingTemplate:format(prc)
            loadingText:setText(_loadingText)
        end

        local updateHintText = function(text)
            loadingHintText:setText(text)
        end

        -- init graphics
        local getFrame = function( frame )
            if frame == nil then
                print( 'Warning: frames is nil')
            end
            local _searchFrames = function( total, el )
                -- print('searching for ', frame)
                -- print('searching in ', el.name)
                -- p(el.data)
                -- p(frame)
                if total then
                    return total
                else
                    local frames
                    if type( frame ) == 'table' then
                        frames = el.data:getFramesIndexes( frame )
                    else
                        frames = el.data:getFrameIndex( frame )
                    end
                    -- print('frames', frames)
                    if frames then
                        return {
                            frames  = frames,
                            frame   = frames,
                            ids     = frames,
                            id      = frames,
                            sheet   = el.sheet,
                            sheet_name   = el.name
                        }
                    else
                        return nil
                    end
                end
            end

            local frames = _.reduce( Planes.spritesheets, nil, _searchFrames)
            if not frames then
                if type( frame ) == 'table' then
                    print( 'Warning: frames wasn\'t found')
                    p(frame)
                else
                    printf( 'Warning: frame "%s", wasn\'t found', frame)
                end

            end
            return frames
        end

        Planes.graphics = {
            getFrame = getFrame
        }

        local spritesheets = {
            {
                name    = 'UI1',
                json    = 'assets/graphics/graphics_UI1.json',
                image   = 'assets/graphics/graphics_UI1.png'
            },
            -- {
            --     name    = 'UI2',
            --     json    = 'assets/graphics/graphics_UI2.json',
            --     image   = 'assets/graphics/graphics_UI2.png'
            -- },
            {
                name    = 'gameplay',
                json    = 'assets/graphics/graphics_gameplay.json',
                image   = 'assets/graphics/graphics_gameplay.png'
            },
            {
                name    = 'misc',
                json    = 'assets/graphics/graphics_misc.json',
                image   = 'assets/graphics/graphics_misc.png'
            },
            {
                name    = 'buttons',
                json    = 'assets/graphics/graphics_buttons.json',
                image   = 'assets/graphics/graphics_buttons.png'
            }
        }

        local loadImages = function()

            function chunk1()
                Planes.spritesheets = {}

                for i = 1, #spritesheets do
                    local sheet     = spritesheets[i]

                    local jsonPath  = sheet.json
                    local imagePath = sheet.image
                    -- parse json
                    local rawJSON = utils.getFileContent(jsonPath)


                    local data = spritesheetJSON.new(rawJSON)

                    _.push(Planes.spritesheets, {
                            sheet    = graphics.newImageSheet( imagePath, data:getSheet() ),
                            data     = data,
                            name     = sheet.name
                    })
                end

                -- Planes.defaultLevelData     = utils.getJSON('assets/data/_defaultLevel.json')
                Planes.defaultLevelData     = require 'assets.data.defaultLevel'
                -- Planes.levelsData           = utils.getJSON('assets/data/levels.json')
                Planes.levelsData           = require 'assets.data.levels'
                Planes.itemsData            = utils.getJSON('assets/data/items.json')
                Planes.objectsData          = utils.getJSON('assets/data/objects.json')
                Planes.planesData           = require 'assets.data.planes'

                updateProgressText(33)

                updateHintText(math.pickRandom(loadingHints))

                timer.performWithDelay( 100, chunk2 )
            end

            function chunk2()
                Planes.Audio.battle             = audio.loadSound( "assets/audio/battle.mp3" )
                Planes.Audio.consumeBonus       = audio.loadSound( "assets/audio/bonus_consume.mp3" )

                Planes.Audio.bulletExplosion    = audio.loadSound( "assets/audio/bullet_explosion.mp3" )

                Planes.Audio.enginePlane        = audio.loadSound( "assets/audio/engine-plane.mp3" )
                Planes.Audio.lossLevel          = audio.loadSound( "assets/audio/loss-level.mp3" )

                updateProgressText(45)

                updateHintText(math.pickRandom(loadingHints))
                timer.performWithDelay( 100, chunk3 )
            end

            function chunk3()
                -- preload audio stuff
                Planes.Audio.ui                 = audio.loadSound( "assets/audio/menu.mp3" )

                Planes.Audio.click              = audio.loadSound( "assets/audio/menu-click.mp3" )

                Planes.Audio.planeExplosion     = audio.loadSound( "assets/audio/plane-explosion.mp3" )

                updateProgressText(60)
                updateHintText(math.pickRandom(loadingHints))

                timer.performWithDelay( 100, chunk4 )
            end

            function chunk4()
                Planes.Audio.shot               = audio.loadSound( "assets/audio/shot.mp3" )

                Planes.Audio.winLevel           = audio.loadSound( "assets/audio/win-level.mp3" )
                updateProgressText(100)


                timer.performWithDelay( 100, chunk5 )
            end

            function chunk5()
                -- update store, increase numbers of runs
                local updater = function(currentState)
                    local newState = currentState
                    if not newState.numberOfRuns then
                        newState.numberOfRuns = 1
                    else
                        newState.numberOfRuns = newState.numberOfRuns + 1
                    end
                    return newState
                end

                Store.updateStore(updater)

                chunk6()
            end

            function chunk6()
                composer.gotoScene('scripts.scenes.home', {})
                -- composer.gotoScene('scripts.scenes.tutorial', {})
                -- composer.gotoScene('scripts.scenes.character_select', {
                --                        params = {
                --                            justOpened = 'gray'
                --                        }
                -- })
                -- composer.gotoScene('scripts.scenes.level_select', {
                --                     params = {
                --                         newLevelToOpen = 2
                --                     }
                -- })
                -- composer.gotoScene('scripts.scenes.level_select', {
                --                     params = {
                --                         freeVersionIsOver = true,
                --                         newLevelToOpen = 4
                --                     }
                -- })
                -- composer.gotoScene('scripts.scenes.game', {})

                -- composer.gotoScene('scripts.scenes.level_select', {
                --                     params = {
                --                         freeVersionIsOver = true
                --                     }
                -- })
                -- composer.gotoScene('scripts.scenes.credits', {})
            end

            local createActualScene = function()
                updateProgressText(0)
                chunk1()
            end

            timer.performWithDelay( 100, createActualScene )

        end

        loadImages()
    end
end

function scene:hide( event )
    if ( event.phase == "will" ) then
    end
end


scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
return scene
