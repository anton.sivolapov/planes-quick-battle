--
-- Created by IntelliJ IDEA.
-- User: c301
-- Date: 18.01.2016
-- Time: 0:54
-- To change this template use File | Settings | File Templates.
--
local level = {}
local Plane         = require 'scripts.objects.Plane'
local GUI           = require("scripts.scenes.game.GUI")


function level:loadWave( config, scene, waveIndex )
    local wave = {}
    local defaultWaveConfig = {
        enemies = {},
        items   = {},
        objects = {},
    }
    local waveConfig = _.extend( defaultWaveConfig, config )

    function wave:handleEnemies(enemies)
        local enemyNum = 1
        local wave = self
        local handleEnemy = function(enemyConfig)
            enemyConfig.onEnemyKill = function(evt)
                local enemy = evt.target
                wave.enemiesKilled = wave.enemiesKilled + 1
                wave.scene.onEnemyKill(enemy)
                if enemyConfig.item then
                    local itemConfig = enemyConfig.item
                    itemConfig.x = enemy.x
                    itemConfig.y = enemy.y

                    timer.performWithDelay(1, function()
                        wave.scene:createItem(itemConfig)
                    end)
                end
                if #enemies == wave.enemiesKilled then

                    timer.performWithDelay(1, function()
                        wave.onEnd()
                    end)
                end
            end
            enemyConfig.onEnemyFire = function(enemy)
                wave.scene.onEnemyFire(enemy);
            end
            local createEnemy = function()
                wave.scene:createEnemy(enemyConfig)
            end
            if enemyConfig.time then
                local creationTime = enemyConfig.time * 1000

                ObjectFactory:create('Timer', creationTime, createEnemy)
            else
                createEnemy()
            end

            enemyNum = enemyNum + 1
        end

        _.each(enemies, handleEnemy)
    end

    function wave:handleItems(items)
        print '... handle items ...'
        local wave = self
        local handleItem = function(itemConfig)
            local createItem = function()
                wave.scene:createItem(itemConfig)
            end
            if itemConfig.time then
                local creationTime = itemConfig.time * 1000
                ObjectFactory:create('Timer', creationTime, createItem)
            else
                createItem()
            end
        end

        _.each(items, handleItem)
    end

    function wave:handleObjects(objects)
        print '... handle objects ...'
        local wave = self
        local handleObject = function(objectConfig)
            local createObject = function()
                wave.scene:createObject(objectConfig)
            end
            if objectConfig.time then
                local creationTime = objectConfig.time * 1000

                ObjectFactory:create('Timer', creationTime, createObject)
            else
                createObject()
            end
        end

        _.each(objects, handleObject)
    end

    function wave:start()
        print '... start wave ...'
        self.startTime = system.getTimer()

        self:handleEnemies(self.config.enemies)
        self:handleItems(self.config.items)
        self:handleObjects(self.config.objects)
    end

    wave.currentEnemyIndex   = 0
    wave.enemiesKilled       = 0
    wave.config              = waveConfig
    wave.scene               = scene
    wave.onEnd               = function() print( 'default onEnd' ) end

    return wave
end

--[[
    init level
--]]


local loadNextWave = nil

local function createGround(scene)

    local groundImage = Planes.graphics.getFrame('grass.png')
    local ground = utils.newTileSprite(
        scene.gameWorld.width + 600,
        scene.gameWorld.height,
        groundImage.sheet,
        groundImage.id
    )
    ground.y = scene.gameWorld.height
    ground.x = -300


    local offsetRectParams = {
        halfWidth=ground.width*.5,
        halfHeight=ground.height*.5,
        x=ground.width*.5,
        y=ground.height*-.5 + 10,
        friction=0.5,
        -- ,
        -- bounce=0.3 
    }
    scene.physics.addBody(ground, "static"
                          , {
                              box=offsetRectParams,
                              filter = {
                                  categoryBits    = Planes.Config.physics.groundCategory,
                                  maskBits        = Planes.Config.physics.allMask,
                              }
                            }
    )

    ground.anchorX = 0
    ground.anchorY = 1
    -- ground.isSensor = true
    ground._name = 'Ground'
    ground.collision = scene.onLocalCollision
    ground:addEventListener( "collision", ground )

    return ground
end

local function createBackground(scene)
    -- add 100px to fill gap after autoresize
    local bg = display.newRect(0, 0, scene.gameWorld.width + 100, scene.gameWorld.height + 100)

    print('creating bg', scene.gameWorld.width, scene.gameWorld.height)

    bg.anchorX = 0
    bg.anchorY = 0

    local paint = {
        type = "gradient",
        color1 = utils.convertColor{ r=161, g=208, b=254 },
        color2 = utils.convertColor{ r=153, g=203, b=252 },
        direction = "down"
    }

    local dark_paint = {
        type = "gradient",
        color1 = utils.convertColor{ r=63, g=76, b=107 },
        color2 = utils.convertColor{ r=81, g=135, b=137 },
        direction = "down"
    }
    
    if scene.levelData.night then
        bg.fill = dark_paint
    else
        bg.fill = paint
    end

    return bg
end

local function createForeground(scene)
    -- temporary set default anchor
    display.setDefault( "anchorX", 0 )
    display.setDefault( "anchorY", 1 )

    local width = scene.gameWorld.width + 100

    local foreground1_frames = Planes.graphics.getFrame('mount.png')
    local foreground1 = utils.newTileSprite(
        width,
        scene.gameWorld.height,
        foreground1_frames.sheet,
        foreground1_frames.id
    )
    foreground1.x = 0
    foreground1.y = scene.gameWorld.height

    local foreground2_frames = Planes.graphics.getFrame('trees_3.png')
    local foreground2 = utils.newTileSprite(
        width,
        scene.gameWorld.height,
        foreground2_frames.sheet,
        foreground2_frames.id
    )
    foreground2.x = 0
    foreground2.y = scene.gameWorld.height

    local foreground3_frames = Planes.graphics.getFrame('trees_2.png')
    local foreground3 = utils.newTileSprite(
        width,
        scene.gameWorld.height,
        foreground3_frames.sheet,
        foreground3_frames.id
    )
    foreground3.x = 0
    foreground3.y = scene.gameWorld.height

    local foreground4_frames = Planes.graphics.getFrame('trees_1.png')
    local foreground4 = utils.newTileSprite(
        width,
        scene.gameWorld.height,
        foreground4_frames.sheet,
        foreground4_frames.id
    )
    foreground4.x = 0
    foreground4.y = scene.gameWorld.height

    -- restore default anchor
    display.setDefault( "anchorX", .5)
    display.setDefault( "anchorY", .5)


    return foreground1, foreground2, foreground3, foreground4
end

local function createPlayer(scene)
    -- player
    local playerPlaneData = Plane.getPlane( scene.currentState.currentPlane )
    local playerData = {
        plane_id    = playerPlaneData.plane_id,
        orientation = "E",
        health      = playerPlaneData.health or 20,
        turn_rate   = 2
    };
    local player    = ObjectFactory:create('Player', scene, playerData)
    player:startEngine()
    -- TODO remove
    -- player:stop()

    timer.performWithDelay(2 * 1000, function()
                               player:takeoff()
    end)
    -- ObjectFactory:create('Pointer', scene, player)
    -- player:showAim()


    player:addEventListener( "damage", scene.updateGUI )

    local function onPostCollision( self, event )
        if event.other._name == 'Ground' then
            if event.force > 50 then
                -- player takes big hit, so kill player
                -- self:killByGround(self.x, event.other.contentBounds.yMax)

                local x, y = event.other:localToContent(self.x, event.other.contentBounds.yMin)
                self:killByGround(self.x, y)

            end
        end
    end

    player.postCollision = onPostCollision

    ObjectFactory:create('Timer', 3000, function()
                             player:addEventListener( "postCollision", player )
    end)

    return player
end

function level:new( scene, levelNum )
    local tmp = _.extend(
        {}, Planes.defaultLevelData
    )
    local levelData = _.extend(
        tmp, Planes.levelsData[levelNum]
    )
    scene.levelData = levelData

    self.scene = scene
    -- background

    local bg = createBackground(scene)
    scene.layers.backgroundLayer:insert(bg)

    local fg1, fg2, fg3, fg4 = createForeground(scene)
    scene.layers.backgroundDetails1:insert(fg1)
    scene.layers.backgroundDetails2:insert(fg2)
    scene.layers.backgroundDetails3:insert(fg3)
    scene.layers.backgroundDetails3:insert(fg4)

    
    local ground = createGround(scene)
    scene.ground = ground
    scene.layers.playerLayer:insert(ground)


    local camera = scene.camera
    -- set camera offset to track player on the center of the screen
    camera:track() -- Begin auto-tracking

    local player = createPlayer(scene)
    scene.player = player
    player.x = 0
    player.y = 0
    camera:setFocus(player) -- Set the focus to the player
    local sX = player:localToContent(0, 0)
    camera:setBounds(
        xMid, scene.gameWorld.width - xMid,
        yMid, scene.gameWorld.height - yMid
    )
    player.x = 350
    player.y = scene.gameWorld.height - 50

    player:stop()
    ground:toFront()


    player:addEventListener( "kill", function()
        scene.player = nil
        scene:onPlayerKill()
    end )



    -- load waves
    local currentWaveIndex = 1;
    level:loadWave(levelData.level_data, scene, currentWaveIndex):start()
    
    loadNextWave = function ( )
        print '=== loadNextWave'
        currentWaveIndex = currentWaveIndex + 1
        local nextWave = levelData.enemies_waves[currentWaveIndex]
        if nextWave then
            currentWave = level:loadWave(nextWave, scene, currentWaveIndex)
            currentWave.onEnd = loadNextWave
            currentWave:start()
        else
            -- end game latest wave reached
            self.scene:endGame(_s('You win!'), 'win')
        end
    end


    currentWave = level:loadWave(
        levelData.enemies_waves[currentWaveIndex],
        scene,
        currentWaveIndex
    )
    currentWave.onEnd = loadNextWave
    currentWave:start()
    
    --[[
    level:loadWave(levelData.level_data, scene, currentWaveIndex):start()

    loadNextWave = function ( )
        print '=== loadNextWave'
        currentWaveIndex = currentWaveIndex + 1
        local nextWave = levelData.enemies_waves[currentWaveIndex]
        if nextWave then
            currentWave = level:loadWave(nextWave, scene, currentWaveIndex)
            currentWave.onEnd = loadNextWave
            currentWave:start()
        else
            -- end game latest wave reached
            self.scene:endGame(_s('You win!'), 'win')
        end
    end


    currentWave = level:loadWave(
        levelData.enemies_waves[currentWaveIndex],
        scene,
        currentWaveIndex
    )
    currentWave.onEnd = loadNextWave
    currentWave:start()
    ]]--

    local UI = GUI:new(scene)
    scene.GUI = GUI

    self.initialized = true
end


function level:update()
    if self.initialized then
        local minDistance = 150
        local player = self.scene.player
        if player and not player.died then
            local ___, distance = utils.inCamera(player)
            local showWarning, warningDirection
            local distToBorder = minDistance + 10

            if distance.leftDist < minDistance then
                --        print'to close to left'
                showWarning = true
                if distToBorder > distance.leftDist then
                    distToBorder = distance.leftDist
                    warningDirection = 'left'
                end
            end
            if distance.rightDist < minDistance then
                --        print'to close to right'
                showWarning = true
                if distToBorder > distance.rightDist then
                    distToBorder = distance.rightDist
                    warningDirection = 'right'
                end
            end
            if distance.topDist < minDistance then
                --        print'to close to top'
                showWarning = true
                if distToBorder > distance.topDist then
                    distToBorder = distance.topDist
                    warningDirection = 'top'
                end
            end
            -- don't show warnings on first 10 seconds

            --        GUI:showWarning('left')
            if (system.getTimer() - self.scene.levetStartTime) > 10 * 1000 then
                if showWarning then
                    GUI:showWarning(warningDirection)
                else
                    GUI:stopWarn()
                end
            end
        end
    else
        print 'Level wan\'t initialized. Please call level:new() first'
    end
end

return level

