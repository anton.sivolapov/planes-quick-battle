--
-- Created by IntelliJ IDEA.
-- User: c301
-- Date: 18.01.2016
-- Time: 0:25
-- To change this template use File | Settings | File Templates.
--
local GUI = {}

function GUI:new(scene)
    -- init on screen controls
    local interfaceLayer = scene.layers.interfaceLayer
    local up = display.newRect(0, 0, xMid, yMid)
    local player = scene.player

    self.scene = scene
    up.anchorX = 0
    up.anchorY = 0
    up.isHitTestable = true
    up.isVisible = false
    up.id = 'up'
    local upFrames = Planes.graphics.getFrame('down_control.png')
    local upIcon = display.newImage(upFrames.sheet, upFrames.ids)
    upIcon.x = xMid * .25
    upIcon.y = yMid * .5

    interfaceLayer:insert(upIcon)

    local down = display.newRect(0, yMid, xMid, yMid)
    down.anchorX = 0
    down.anchorY = 0
    down.isHitTestable = true
    down.isVisible = false
    down.id = 'down'
    local downFrames = Planes.graphics.getFrame('up_control.png')
    local downIcon = display.newImage(downFrames.sheet, downFrames.ids)
    downIcon.x = xMid * .25
    downIcon.y = _H - yMid * .5
    interfaceLayer:insert(downIcon)

    local fireFrames = Planes.graphics.getFrame('fire_control.png')
    local fire = display.newRect(xMid, 0, xMid, _H)
    fire.isHitTestable = true
    fire.anchorX = 0
    fire.anchorY = 0
    fire.isVisible = false
    fire.id = 'fire'
    -- fire.alpha = .5

    local fireIcon = display.newImage(fireFrames.sheet, fireFrames.ids)

    fireIcon.x = _W - xMid * .25
    fireIcon.y = _H - xMid * .25
    fireIcon.anchorX = .5
    fireIcon.anchorY = .5

    local upTouchId, downTouchId, fireTouchId

    local function handleUp(event)
        if event.phase == "began" then
            player.goUp = true
        elseif event.phase == "ended" then
            player.goUp = false
            upTouchId = nil
        end
    end

    local function handleDown(event)
        if event.phase == "began" then
            player.goDown = true
        elseif event.phase == "ended" then
            player.goDown = false
            downTouchId = nil
        end
    end

    local function handleFire(event)
        if event.phase == "began" then
            player:startFire()
        elseif event.phase == "ended" then
            player:stopFire()
            fireTouchId = nil
        end
    end
    
    local function movePlayer( event )
        if event.phase == "began" then
            if(event.target.id == "up") then
                upTouchId = event.id
            end
            if(event.target.id == "down") then
                downTouchId = event.id
            end
            if(event.target.id == "fire") then
                fireTouchId = event.id
            end
        end

        if event.id == upTouchId then
            handleUp(event)
        end
        if event.id == downTouchId then
            handleDown(event)
        end
        if event.id == fireTouchId then
            handleFire(event)
        end
    end

    up:addEventListener( "touch", movePlayer)
    down:addEventListener( "touch", movePlayer)
    fire:addEventListener( "touch", movePlayer)

    interfaceLayer:insert(up)
    interfaceLayer:insert(down)
    interfaceLayer:insert(fire)
    interfaceLayer:insert(fireIcon)

    player:addEventListener('kill', function ()
        up:removeEventListener( "touch", movePlayer)
        down:removeEventListener( "touch", movePlayer)
        fire:removeEventListener( "touch", movePlayer)
    end)

    --add menu icon

    local menuFrames = Planes.graphics.getFrame('pause_button.png')
    local menuIcon = display.newImage(menuFrames.sheet, menuFrames.id)
    menuIcon:addEventListener("touch", function(e)
        if e.phase == 'ended' then
            print'toggle pause by clicking menu icon'
            scene:togglePause()
        end
        -- return true to stop event bubling 
        return true
    end)
    menuIcon.x = _W - xMid * .12
    menuIcon.y = xMid * .12
    scene.menuIcon = menuIcon

    scene.layers.superFrontLayer:insert( menuIcon );

    --show live icon

    local healthFrame = Planes.graphics.getFrame('life_icon.png')
    local healthIcon = display.newImage(healthFrame.sheet, healthFrame.id)
    healthIcon.x = xMid * .12
    healthIcon.y = xMid * .12
    -- healthIcon:scale( 0.3, 0.3 )
    self.healthIcon = healthIcon
    interfaceLayer:insert( healthIcon )

    
    -- handle available bullets in UI
    local bulletsUI = {
        bulletsGroup    = nil,
        bullets         = {}
    }
    function bulletsUI:create(numberOfBullets)
        if not self.bulletsGroup then
            self.bulletsGroup = display.newGroup()
            self.bulletsGroup.anchorChildren = true
        end

        -- clear all
        _m.each(self.bullets, function(k, v) display.remove(v) end)

        -- build icons
        for i = 1, numberOfBullets do
            local bulletIcon = self:createIcon()
            bulletIcon.x = i * 10

            _m.push(self.bullets, bulletIcon)
            self.bulletsGroup:insert(bulletIcon)
        end

        return self.bulletsGroup
    end

    function bulletsUI:createIcon()
        local bulletFrame = Planes.graphics.getFrame('bullet.png')
        local bulletIcon = display.newImage(bulletFrame.sheet, bulletFrame.id)
        bulletIcon.rotation = -90
        return bulletIcon
    end

    local bulletsGroup = bulletsUI:create( player:getAvailableBullets() )
    bulletsGroup.x = healthIcon.x + 150
    bulletsGroup.y = healthIcon.y
    interfaceLayer:insert(bulletsGroup)

    player:addEventListener('availableBulletsChanged', function(event)
                                bulletsUI:create( player:getAvailableBullets() )
                                                       end
    )
    -- END handle available bullets in UI

    --show lives

    local style = Planes.Config.mainTextStyle
    local hX        = healthIcon.x + 80
    local healthLabel = display.newBitmapText(
        {
            text    = scene.player.health .. '',
            align   = 'center',
            x       = hX,
            y       = healthIcon.y,
            font    = style.font,
            fontSize  = style.size
        }
    )
    healthLabel.anchorX = 0
    healthLabel.alpha = .8
    self.healthLabel = healthLabel

    interfaceLayer:insert( healthLabel );


    -- ready label
    local text = _s("Get Ready!")
    self:showBigLabel(text, nil, true)

    self:initWarningBorder(scene)
end

function GUI:showWarning(direction)
    self:stopWarn(direction)
    self.alarmLabel.isVisible = true
    local border = self.warnings[direction]
    if not border.isVisible then
        border.isVisible = true
        border.alpha = 1
        local function sinTransition(start)
            local nextStart
            if start == 1 then
                nextStart = 0
            else
                nextStart = 1
            end
            border.transition = transition.to(
                border,
                {
                    time = 1000,
                    alpha = start,
                    transition=easing.inOutSine,
                    onComplete = function()
                        sinTransition( nextStart )
                    end
                }
            )
        end
        sinTransition(border.alpha)
    end

end

function GUI:stopWarn(exclusion)
    self.alarmLabel.isVisible = false
    local turnOff = function(key)
        if key ~= exclusion then
            self.warnings[key].isVisible = false
            if self.warnings[key].transition then
                transition.cancel(self.warnings[key].transition)
            end
        end
    end
    _.each(_.keys(self.warnings), turnOff)
end

function GUI:initWarningBorder(scene)
    local warnWidth = 20
    local frames = Planes.graphics.getFrame('turn_back.png')
    local framesVert = Planes.graphics.getFrame('turn_back_1.png')
    local getImage = function(vert)
        return ObjectFactory:create(
            'Image',
            vert and framesVert.sheet or frames.sheet,
            vert and framesVert.ids or frames.ids
        )
    end

    local leftWarn      = getImage(true)
    leftWarn.x = 40
    leftWarn.y = yMid
    leftWarn:scale(-1,1)
    leftWarn.height = _H
    leftWarn.width = 50
    local rightWarn     = getImage(true)
    rightWarn.x = _W - 30
    rightWarn.y = yMid
    rightWarn.height = _H
    rightWarn.width = 50
    local topWarn       = getImage()
    topWarn.x = xMid
    topWarn.y = 10
    local bottomWarn    = getImage()
    topWarn.x = xMid
    topWarn.y = 30

    local interfaceLayer = scene.layers.interfaceLayer
    interfaceLayer:insert(leftWarn)
    interfaceLayer:insert(rightWarn)
    interfaceLayer:insert(topWarn)
    interfaceLayer:insert(bottomWarn)

    leftWarn.isVisible      = false
    rightWarn.isVisible     = false
    topWarn.isVisible       = false
    bottomWarn.isVisible    = false


    local style = Planes.Config.mainTextStyle
    local txt = _s('Turn back to the battle, chicken!')
    local alarmLabel = display.newBitmapText(txt, xMid, self.healthIcon.y + 50, style.font, style.size )
    self.alarmLabel = alarmLabel
    self.alarmLabel.isVisible = false

    interfaceLayer:insert( alarmLabel );

    self.warnings = {
        left    = leftWarn,
        right   = rightWarn,
        top     = topWarn,
        bottom  = bottomWarn,
    }
end

function GUI:showBigLabel(text, style, hide)
    local interfaceLayer = self.scene.layers.interfaceLayer
    style = style
            or Planes.Config.bigNotificationStyle

    local bigLabel = display.newBitmapText(text, _W*.5, _H*.5 - 100, style.font, style.size)
    bigLabel:setTintColor(style.fill.r,style.fill.g,style.fill.b)

    bigLabel.xScale = 1
    bigLabel.yScale = 1

    -- local bigLabel = ObjectFactory:create(
    --     'Image',
    --     Planes.graphics.getFrame('get_ready1.png').sheet,
    --     Planes.graphics.getFrame('get_ready1.png').frames
    -- )
    -- bigLabel.x = xMid
    -- bigLabel.y = yMid

    -- bigLabel.xScale = .5
    -- bigLabel.yScale = .5

    interfaceLayer:insert( bigLabel );

    local disappearing = function()

        if hide then
            transition.to(
                bigLabel,
                {
                    xScale = 1,
                    yScale = 1,
                    time = 1000,
                    onComplete = function()
                        bigLabel:removeSelf()
                        bigLabel = nil
                    end,
                    transition=easing.inOutBack
                }
            )
        end
    end

    transition.to(
        bigLabel,
        {
            xScale = 2,
            yScale = 2 ,
            time = 1000,
            onComplete = disappearing,
            transition=easing.inOutBack
        }
    )

end

function GUI:showWinLabel()
    local text = _s("Level Cleared!")
    local style = Planes.Config.winLevelStyle

    self:showBigLabel(text, style)
end

function GUI:showLooseLabel()
    local style = Planes.Config.gameOverStyle
    local text = _s("Game Over!")
    self:showBigLabel(text, style)
end

function GUI:refresh()
    local scene = self.scene
    local player = scene.player
    if player then
        self.healthLabel:setText(player.health .. '')
    end
end

function GUI:onPause()
    local scene = self.scene
    local player = scene.player
    player.goDown   = false
    player.goUp     = false
    player:stopFire()
end

return GUI
