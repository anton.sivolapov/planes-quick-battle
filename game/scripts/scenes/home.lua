print 'Loading home scene..'

local perspective   = require 'scripts.vendor.perspective'
local scene         = composer.newScene()
local widget        = require 'widget'
local physics       = require 'physics'
local Store         = require 'scripts.Store'


scene.physics = physics

scene.layers = {
}
-- all timers
scene.timers = {}

scene.gameWorld = {
    width   = _W,
    height  = _H,
}
local titlePlane


function handleCollision(event)
    local border, other = utils.parseColliderObj(event.object1, event.object2, "border")
    if border and event.phase == 'began' then
--        print '=== set target center'
        other:setTarget({
            x = xMid,
            y = yMid
        })
    end
    if border and event.phase == 'ended' then
--        print '=== clear target center'
        other:setTarget(nil)
    end
end
-- exec on every frame
local function update()
    ObjectFactory:updateAll()
end

function scene:create(evt)
end

-- Custom function for resuming the game (from pause state)
function scene:resumeGame()
    --code to resume game
end

function scene:show( evt )
    local phase = evt.phase

    if ( phase == "will" ) then
        local state = Store.getState()

        -- Called when the scene is still off screen (but is about to come on screen).
        self.physics.start()
        self.physics.setDrawMode( Planes.Config.physicsDrawMode )

        local params    = evt.params or {}
        local gameState = Store.getState()

        ObjectFactory.scene = scene

        self.layers, self.camera = utils.createSceneLayers(self)

        -- Initialize the scene here.
        -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        local bg = ObjectFactory:create(
           'Image',
            self.layers.interfaceLayer,
            Planes.graphics.getFrame('ui_background.png').sheet,
            Planes.graphics.getFrame('ui_background.png').frames
        )

        bg.width    = _W
        bg.height   = _H
        bg.anchorX  = 0
        bg.anchorY  = 0

            -- Planes.graphics.getFrame(_s('quick_planes_battle.png')).sheet,
            -- Planes.graphics.getFrame(_s('quick_planes_battle.png')).frames
        local title1 = ObjectFactory:create(
           'Image',
            self.layers.interfaceLayer,
            Planes.graphics.getFrame(_s('quick_planes_battle.png')).sheet,
            Planes.graphics.getFrame(_s('quick_planes_battle.png')).frames
        )

        title1.anchorY  = 0
        title1.x = xMid
        title1.y = 70

        
        local removeAds = function(e)
            if e.phase == 'ended' then
                local product_id = 'full_version'
                print( 'buying', product_id )

                utils.buyItem({
                        text = 'Remove ads',
                        product_id = product_id,
                        onPurchased = function()
                            flurryAnalytics.logEvent( "Ads Removed" )
                            local updateState = function(state)
                                state.fullVersion = true
                                return state
                            end

                            Store.updateStore(updateState)
                            composer.gotoScene('scripts.scenes.home')
                        end,
                        onCanceled = function()
                            flurryAnalytics.logEvent( "Cancel Remove Ads" )
                        end
                })
            end
        end
        if not state.fullVersion then
            local style = Planes.Config.winLevelStyle
            local bigLabel = display.newBitmapText(
                {
                    text    = _s('Remove Ads'),
                    x       = 170,
                    y       = 170,
                    font    = style.font,
                    fontSize  = 80,
                    align   = 'center'
                }
            )
            bigLabel.rotation = -45
            bigLabel:setTintColor(style.fill.r,style.fill.g,style.fill.b)
            bigLabel.trans = utils.pulsating(bigLabel)
            bigLabel:addEventListener( "touch", removeAds )
            self.layers.interfaceLayer:insert( bigLabel )
        end

        -- local title2 = ObjectFactory:create(
        --    'Image',
        --     self.layers.interfaceLayer,
        --     Planes.graphics.getFrame(_s('Battle.png')).sheet,
        --     Planes.graphics.getFrame(_s('Battle.png')).frames
        -- )

        -- title2.anchorY  = 0
        -- title2.x = xMid
        -- title2.y = title1.bottom() - 70

        local lines = ObjectFactory:create(
           'Image',
            self.layers.interfaceLayer,
            Planes.graphics.getFrame('lines_prop.png').sheet,
            Planes.graphics.getFrame('lines_prop.png').frames
        )

        -- lines.anchorY  = 0
        lines.x = xMid
        lines.y = title1.bottom() + 30

        local propeller = ObjectFactory:create(
           'Image',
            Planes.graphics.getFrame('propeller.png').sheet,
            Planes.graphics.getFrame('propeller.png').frames
        )

        -- propeller.anchorY  = 0
        propeller.x = xMid
        propeller.y = title1.bottom() + 130

        local propellerCenter = ObjectFactory:create(
           'Image',
            Planes.graphics.getFrame('propeller_1.png').sheet,
            Planes.graphics.getFrame('propeller_1.png').frames
        )


        -- propellerCenter.anchorY  = 0
        propellerCenter.x = propeller.x
        propellerCenter.y = propeller.y

        local frame1 = Planes.graphics.getFrame(_s('button_start_off.png'))
        local frame2 = Planes.graphics.getFrame(_s('button_start_on.png'))

        local button = widget.newButton({
            label = '',
            x = xMid,
            y = propeller.bottom() + 30,
            sheet = frame1.sheet,
            defaultFrame = frame1.frames,
            overFrame = frame2.frames,
            onPress  = function()
                audio.play(Planes.Audio.click);
            end,
            onRelease = function ()
                local transitToNext = function()
                    composer.gotoScene('scripts.scenes.character_select')
                end
                
                transition.to( propeller,
                               {
                                 xScale=1.2, yScale=1.2,
                                 rotation=720,
                                 onComplete=transitToNext } )

            end
        })
        button.anchorY  = 0

        local frame1 = Planes.graphics.getFrame('tutorial_off.png')
        local frame2 = Planes.graphics.getFrame('tutorial_on.png')

        local tutorialButton = widget.newButton({
            label = '',
            x = _W - 100,
            y = _H - 170,
            sheet = frame1.sheet,
            defaultFrame = frame1.frames,
            overFrame = frame2.frames,
            onPress  = function()
                audio.play(Planes.Audio.click);
            end,
            onRelease = function ()
                local options = {
                    isModal = true,
                    effect = "fade",
                    time = 700,
                    -- params = {
                    --  nextScene = 'scripts.scenes.game',
                    --  sceneOptions = {
                    --      params = { level=levelNum }
                    --  }
                    -- }
                }
                composer.showOverlay( "scripts.scenes.tutorial", options )
            end
        })
        tutorialButton.anchorY  = 0

        local frame1 = Planes.graphics.getFrame('credits_off.png')
        local frame2 = Planes.graphics.getFrame('credits_on.png')

        local creditsButton = widget.newButton({
            label = '',
            x = _W - 200,
            y = _H - 170,
            sheet = frame1.sheet,
            defaultFrame = frame1.frames,
            overFrame = frame2.frames,
            onPress  = function()
                audio.play(Planes.Audio.click);
            end,
            onRelease = function ()
                composer.gotoScene('scripts.scenes.credits', {
                                       params = {
                                           fromHomePage = true
                                       }
                })
            end
        })
        creditsButton.anchorY  = 0

        self.layers.interfaceLayer:insert( creditsButton )
        self.layers.interfaceLayer:insert( tutorialButton )

        local function enableSound()
            print'enable sound'
            audio.setVolume(1)
            for i = 1, 32 do
                -- reserved channels
                if i == 1 or i == 2 then
                    -- print('set 1 for channel', i) 
                    -- audio.setVolume(1, {
                    --     channel = i
                    -- })
                else
                    -- print('set .2 for channel', i) 
                    audio.setVolume(.2, {
                        channel = i
                    })
                end
            end
        end
        local function disableSound()
            print'enable sound'
            audio.setVolume(1)
            for i = 1, 32 do
                -- reserved channels
                if i == 1 or i == 2 then
                    -- print('set 1 for channel', i) 
                    -- audio.setVolume(1, {
                    --     channel = i
                    -- })
                else
                    -- print('set .2 for channel', i) 
                    audio.setVolume(0, {
                        channel = i
                    })
                end
            end
        end

        -- set volumes
        audio.reserveChannels( 2 )
        if gameState.enableSound then
            enableSound()
        else
            disableSound()
        end
        utils.initMusic(gameState)
        


        local frame1 = Planes.graphics.getFrame('musicOff.png')
        local frame2 = Planes.graphics.getFrame('musicOn.png')
        local soundButton = widget.newSwitch({
            x = _W - 70,
            y = 70,
            style = "checkbox",
            id = "muteSwitch",
            initialSwitchState = gameState.enableMusic,
            sheet       = frame1.sheet,
            frameOn     = frame2.id,
            frameOff    = frame1.id,
            onRelease = function (e)
                gameState.enableMusic =  e.target.isOn
                if gameState.enableMusic then
                    utils.enableMusic()
                else
                    utils.disableMusic()
                end
                Store.setState(gameState)
            end
        })
        soundButton.anchorX = 1
        soundButton.anchorY = 0
        soundButton:scale(1.5,1.5)
        self.layers.interfaceLayer:insert( soundButton )

        local frame1 = Planes.graphics.getFrame('sound_on.png')
        local frame2 = Planes.graphics.getFrame('sound_off.png')
        local muteButton = widget.newSwitch({
            x = _W - 70,
            y = soundButton.y + 100,
            style = "checkbox",
            id = "muteSwitch",
            initialSwitchState = gameState.enableSound,
            sheet       = frame1.sheet,
            frameOn     = frame1.id,
            frameOff    = frame2.id,
            onRelease = function (e)
                gameState.enableSound =  e.target.isOn
                if gameState.enableSound then
                    enableSound()
                else
                    disableSound()
                end
                Store.setState(gameState)
            end
        })
        muteButton.anchorX = 1
        muteButton.anchorY = 0
        muteButton:scale(1.5,1.5)
        self.layers.interfaceLayer:insert( muteButton )

        self.layers.interfaceLayer:insert( button );
        self.layers.interfaceLayer:insert( propeller );
        self.layers.interfaceLayer:insert( propellerCenter );

        -- message from other states
        -- deprecated
        if false and params.message then

            local style = Planes.Config.gameOverStyle
            local message = display.newBitmapText(params.message, xMid, yMid + 70, style.font, style.size )
            message:setTintColor(style.fill.r,style.fill.g,style.fill.b)

            self.layers.interfaceLayer:insert( message )
        end


        -- version text
        local style = Planes.Config.techTextStyle
        local _versionText = 'Music CleytonRX. Created by Anton Sivolapov anton.sivolapov@gmail.com      version '
        _versionText = _versionText .. Planes.Config.version

        -- if gameState.fullVersion then
        --     _versionText = _versionText .. ' (full)'
        -- else
        --     _versionText = _versionText .. ' (free)'
        -- end
        -- _versionText = _versionText

        local versionText = display.newEmbossedText(_versionText, _W - 20, _H - 20, style.font, style.size)
        versionText:setFillColor( unpack(utils.convertColor(style.fill)) )
        -- local color =
        -- {
        --     highlight   = utils.converColorTable(style.stroke),
        --     shadow      = utils.converColorTable(style.stroke)
        -- }
        -- versionText:setEmbossColor( color )
        versionText.anchorX = 1
        versionText.anchorY = 1

        self.layers.interfaceLayer:insert( versionText )

        -- place plane

        local titlePlane = ObjectFactory:create('Plane', scene, {
            -- plane_id = 'red',
            plane_id = 'green',
            orientation = 'W',
            x = _W + 50,
            y = 100
        }, self.layers.interfaceLayer)
        titlePlane:startPropeller()
        -- titlePlane:stop()

        self.camera:toPoint(0, scene.gameWorld.height)

--        self.camera:setMasterOffset(200, 0)
        self.camera:setBounds(
            xMid, scene.gameWorld.width,
            yMid, scene.gameWorld.height
        )
        -- self.layers.interfaceLayer:insert( titlePlane );

        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
        ObjectFactory:create('Border', self.physics, self)
        Runtime:addEventListener( "enterFrame", update )
        Runtime:addEventListener( "collision", handleCollision )

        utils.trackCurrentScene()
    end
end

function scene:hide( event )
    local phase = event.phase
    if ( phase == "will" ) then
        Runtime:removeEventListener( "enterFrame", update )
        Runtime:removeEventListener( "collision", handleCollision )
        ObjectFactory:removeAll()
        scene.physics.stop()

        self.camera.cancel()

        for key, layer in pairs(self.layers) do
            layer:removeSelf()
            layer = nil
        end
    end
end

function scene:destroy( event )
end

scene:addEventListener('create' , scene)
scene:addEventListener('show'   , scene)
scene:addEventListener('hide'   , scene)
scene:addEventListener('destroy', scene)

return scene
