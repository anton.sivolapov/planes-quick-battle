print '\n\n'
print 'Loading game scene..'

--local perspective   = require("scripts.vendor.perspective")
local level         = require("scripts.scenes.game.level")
local Item          = require 'scripts.objects.Item'

local scene         = composer.newScene()
local battleEndId = Planes.adsConfig.battleEnd.id



-- exec on every frame
local function update()
    ObjectFactory:updateAll()
    level:update()
end

function bulletExplode( bullet )
    local availableChannel = audio.findFreeChannel()
    audio.play(Planes.Audio.bulletExplosion, {
        channel = availableChannel
    })

    ObjectFactory:create('Blast', scene, {
        x = bullet.x,
        y = bullet.y
    })
end

local function accelerometerListener( event )
    if event.isShake then
        print( "The device is being shaken" )
    end

    return true
end

function scene.onLocalCollision( self, event )
    local other = event.other
    if event.phase == "began" then
        if self._name == 'Player' and other._name == 'Cloud' then
--            printf('== %s covered in cloud', self._name)
            self.inClouds = true
            other.alpha = 0.5
        elseif self._name == 'Player' and other._name == 'Ground' then
           -- printf('== %s hit the ground', self._name)
        elseif self._name == 'Ground' and other._name == 'Bullet' then
--            printf('== %s hit the ground', self._name)
            bulletExplode(other)
            ObjectFactory:kill(other)
        elseif other._name == 'Bullet' then
            --            printf('== %s hit by bullet', self._name)
            -- explode bullet
            if self ~= other.planeEmmiter then
                bulletExplode(other)
                ObjectFactory:kill(other)
                if not ( scene.godMode and self._name == 'Player' ) then
                    self:damage()
                end
            end
        elseif other._name == 'Item' then
            --            printf('== %s get item', self._name)

            self:consumeItem(other)
            ObjectFactory:kill(other)
        end
    elseif event.phase == "ended" then
        if self._name == 'Player' and other._name == 'Cloud' then
--            printf('== %s leave cloud', self._name)
            other.alpha = 1
            self.inClouds = false
        end
    end
end
function scene:createEnemy(data)
    local y = data.y or
    math.random( 50, scene.gameWorld.height - 100 )

    local x = data.x or scene.gameWorld.width + 20
    -- set default orientation
    data.orientation = data.orientation or "W"


    -- always create new enemy
    data.bullets = self.enemyBullets
    data.x = x
    data.y = y

    local newEnemy = ObjectFactory:create('Enemy', scene, data)
    newEnemy:startEngine()
    newEnemy.healthBar:show()

    ObjectFactory:create('Pointer', scene, newEnemy)
    newEnemy:addEventListener( "kill", data.onEnemyKill )

    newEnemy.onFire = data.onEnemyFire
end


function scene:onEnemyKill( enemy )
--[[
    local blastAnimation = Planes.CONST.regularBlastAnimation
    --play sound
    if self.planeExplosion.isDecoded then
        self.planeExplosion.play()
    end
    --update stat
    self.levelStat.enemiesKilled = self.levelStat.enemiesKilled + 1

    local blast = self.game.add.sprite(
        enemy.x, enemy.y, 'graphics', blastAnimation[0]
    )
    blast.anchor.setTo(0.5)
    self.layers.playerLayer.add(blast)

    local blastOut = blast.animations.add('blastOut', blastAnimation, 10)
    blastOut.onComplete.add(function()
        blast.destroy()
    end, self)
    blastOut.play() 
--]]

end

function scene:getItem(itemId)
    local compare = function(object)
        return object.item_id == itemId
    end
    return _.detect( Planes.itemsData, compare )
end
function scene:getObject(objectId)
    local compare = function(object)
        return object.object_id == objectId
    end

    return _.detect( Planes.objectsData, compare )
end

function scene:createItem(data)
    if not data.item_id or data.item_id == 'random_bonus' then
        --pick up random item_id
        local bonuses = _.keys( Item.itemsHandlers )
        local bonusName = math.pickRandom(bonuses)
        --pick random position

        local itemConfig = {
            item_id = bonusName
        }
        return scene:createItem(itemConfig)
    else
        --look for a dead element
        local x = data.x or
            math.random( 150, scene.gameWorld.width - 150 )
        local y = data.y or scene.gameWorld.height - 70
        data.x = x
        data.y = y

        local newItem    = ObjectFactory:create('Item', scene, data)

        local pointer    = ObjectFactory:create('Pointer', scene, newItem)

        local function removePointer()
            pointer:removeSelf()
            pointer = nil
        end
        newItem:addEventListener( "remove", removePointer )

        table.insert(self.items, newItem)
        return newItem
    end
end
function scene:createObject(data)
    local objectData = scene:getObject( data.object_id ) or {}
    local defaultObjectConfig = scene:getObject( '_defaultObjectConfig' ) or {}

    if objectData then
        defaultObjectConfig  = _.extend( {}, defaultObjectConfig )
        objectData = _.extend( defaultObjectConfig, objectData )
        objectData = _.extend( objectData, data )
        local layer = objectData.layer
        local x     = objectData.x

        local y     = objectData.y

        -- local y = objectData.y or
        --     math.random( 100, scene.gameWorld.height - 150 )

        -- if object on the ground, set y coord
        if x and x < 0 then
            x = scene.gameWorld.width + x
        end
        if y and y < 0 then
            y = scene.gameWorld.height + y
        end
        if objectData.on_ground then
            y = scene.gameWorld.height - scene.ground.height
        end

        local startFrame = objectData.startFrame
        if layer and x ~= nil and y ~= nil then
            local object_constructor = objectData.object_constructor
            if object_constructor then
                objectData.x = x
                objectData.y = y
                ObjectFactory:create(object_constructor, scene, objectData)

            elseif startFrame then

                local frames = Planes.graphics.getFrame(startFrame)

                local sprite = display.newImage(frames.sheet,
                    frames.ids)

                if objectData.scale then
                    sprite.xScale = objectData.scale.x or 1
                    sprite.yScale = objectData.scale.y or 1
                end
                if objectData.on_ground then
                    -- if object on the ground, adjust anchor
                    sprite.anchorY = 1
                end
                sprite.x = x
                sprite.y = y
                scene.layers[layer]:insert( sprite )
            else
                print( 'ERROR. Object doesnt have mandatory fields..' )
                p(objectData)
            end
        else
            print( 'ERROR. Object doesnt have mandatory fields' )
            p(data)
        end
    end
end


function scene:pauseON()
    if not self.paused then
        print'turn ON pause'
        self.paused = true
        self.physics.pause()
        transition.pause()
        ObjectFactory:pauseAll()

        transition.to(
            self.menuIcon,
            {
                time = 100,
                y = self.menuIcon.y + 30,
                transition=easing.linear,
                onComplete = function()
                end
            }
        )

        local options = {
            isModal = true,
            effect = "fade",
            time = 400
        }
        composer.showOverlay(
            "scripts.scenes.pause", options
        )
        self.GUI:onPause()
    end
end
function scene:pauseOFF()
    if self.paused then
        print'turn OFF pause'
        composer.hideOverlay()

        self.paused = false
        self.physics.start()
        transition.resume()
        ObjectFactory:resumeAll()

        transition.to(
            self.menuIcon,
            {
                time = 100,
                y = self.menuIcon.y - 30,
                transition=easing.linear,
                onComplete = function()
                end
            }
        )
    end
end
function scene:togglePause( )
    if self.paused then
        self:pauseOFF()
    else
        self:pauseON()
    end
end

local function toggleDebug( )
    if scene.debug then
        print'turn OFF debug'

        scene.debug = false
        scene.physics.setDrawMode( "normal" )
    else
        print'turn ON debug'
        scene.debug = true
        scene.physics.setDrawMode( "hybrid" )
    end
end

local function toggleGodMode( )
    if scene.godMode then
        print'turn OFF god mode'
        scene.godMode = false
    else
        print'turn ON god mode'
        scene.godMode = true
    end
end

local function onSystemEvent( event )
    if ( event.type == "applicationSuspend" ) then
        scene:pauseON()
    end
end
    
local function globalKeyHandler( event )
    -- reset all controls to default
    if ( event.keyName == "p" ) then
        if event.phase == 'up' then
            scene:togglePause()
        end
    end
    if ( event.keyName == "d" ) then
        if event.phase == 'up' then
            toggleDebug()
        end
    end
    if ( event.keyName == "g" ) then
        if event.phase == 'up' then
            toggleGodMode()
        end
    end

    if ( event.keyName == "back" ) then
        if event.phase == 'up' then
            local platformName = system.getInfo( "platformName" )
            if ( platformName == "Android" ) or ( platformName == "WinPhone" ) then
                scene:togglePause()
                return true
            end
        end
    end
    -- IMPORTANT! Return false to indicate that this app is
    -- NOT overriding the received key
    -- This lets the operating system execute its default handling of
    -- the key
    return false
end

function scene:create(evt)
end

function scene:onPlayerKill( message, state )
    --play sound
    --update stat


    --wait some time before restart
    self:endGame(' GAME OVER! ');
end


function scene:endGame( message, state )
    print('============== endGame ===============', message, state)

    print('channel', self.battleChannel)
    audio.fade( { channel=Planes.Config.audioChannels.bgMusic, volume=0.0} )

    if not self.gameEnded then

        self.gameEnded = true
        --show cool label for few seconds
        if state == 'win' then

            local newPlaneToOpen = false
            local newLevelToOpen = self.currentLevel + 1

            audio.play(Planes.Audio.winLevel)
            -- currest game state
            local state = self.currentState

            --update stat
            self.GUI:showWinLabel()

            local updates = self.levelData.rewards.state_update or {}
            local gameFinished = false
            --update state. see "rewards" in levelData ( levels.json )
            if table.contains( state.openedLevels, newLevelToOpen) then
                --level already opened
                newLevelToOpen = false
            end

            if updates.openedPlanes then
                if not table.contains( state.openedPlanes, updates.openedPlanes[1] ) then
                    -- if plane wasnt opened before
                    newPlaneToOpen = updates.openedPlanes[1]
                end
            end

            --if it was latest level, so game finished. congrats!
            local latestLevel = #Planes.levelsData

            if self.currentLevel == latestLevel then
                state.gameFinished = true
                gameFinished = true
            end

            print'Rewards'
            p(updates)

            _.each(_.keys(updates), function( key )
                local val = updates[key]
                if type(val) == 'table' then
                    state[key] = state[key] or {}
                    _.each(_.keys(val), function( k )
                        local val = val[k]
                        _.push(state[key], val)
                    end )

                    state[key] = _m.unique(state[key])
                else
                    state[key] = val
                end
            end)

            Store.setState(state)
            ObjectFactory:create('Timer', 5000, function()
                -- return back to title screen in few seconds
                utils.showAd(battleEndId)

                if gameFinished then
                    composer.gotoScene('scripts.scenes.credits')
                else
                    local options = {
                        params = {
                            newLevelToOpen      = newLevelToOpen,
                            levelWin            = true
                        }
                    }
                    if newPlaneToOpen then
                        options.params.justOpened = newPlaneToOpen
                        composer.gotoScene('scripts.scenes.character_select', options)
                    else
                        composer.gotoScene('scripts.scenes.level_select', options)
                    end
                end
            end)
        else
            --play sound
            audio.play(Planes.Audio.lossLevel)

            self.GUI:showLooseLabel()

            --return back to title screen in few seconds
            ObjectFactory:create('Timer', 5000, function()
                utils.showAd(battleEndId)

                local options = {
                    levelWin            = false
                }
                composer.gotoScene('scripts.scenes.level_select', options)
            end)
        end
    end
end
-- Custom function for resuming the game (from pause state)
function scene:resumeGame()
    --code to resume game
end

function scene:updateGUI()
    -- print'update gui..'
    scene.GUI:refresh()
end

local function init(scene, params)
    utils.loadAd(
        "interstitial",
        { appId = battleEndId, testMode = Planes.adsConfig.testMode }
    )
    -- state
    scene.currentState = Store.getState()

    local params = params or { level = 1 }
    local physics = require("physics")
    physics.start()
    physics.setDrawMode( Planes.Config.physicsDrawMode )

    physics.setGravity( 0, 50 )

    scene.physics = physics


    --pause state
    scene.paused = false

    -- enemies
    scene.enemies = {}
    scene.enemyBullets = {}

    -- items
    scene.items = {}

    scene.player = nil
    scene.layers = {
    }

    scene.gameWorld = {
        width   = 2560,
        -- width   = 1280,
        -- height  = 800,
        height  = 1600,
    }
    
    -- start bg music
    audio.stop()
    audio.play(Planes.Audio.battle, {
        channel = Planes.Config.audioChannels.bgMusic,
        loops = -1,
    })

    scene.levetStartTime = system.getTimer()
    scene.physics.start()

    ObjectFactory.scene = scene
    scene.currentLevel = params.level
    scene.gameEnded = false

    scene.layers, scene.camera = utils.createSceneLayers(scene)

    Runtime:addEventListener( "system", onSystemEvent )


    -- Runtime:addEventListener( "accelerometer", accelerometerListener )
end

function scene:show( evt )
    local phase = evt.phase

    if ( phase == "will" ) then

        init(scene, evt.params)

        level:new(scene, self.currentLevel)

        Runtime:addEventListener( "key", globalKeyHandler )
        Runtime:addEventListener( "enterFrame", update )

        utils.trackCurrentScene()
    end
end

--[[
    Unload scene, kill all sprites, etc..
--]]

function scene:stopAll()
    audio.stop()
    transition.cancel()
    Runtime:removeEventListener( "key", globalKeyHandler )
    Runtime:removeEventListener( "enterFrame", update )
    Runtime:removeEventListener( "collision", handleCollision )
    Runtime:removeEventListener( "system", onSystemEvent )
    -- Runtime:removeEventListener( "accelerometer", accelerometerListener )

    ObjectFactory:removeAll()
    scene.physics.stop()

    self.camera.cancel()

    for key, layer in pairs(self.layers) do
        layer:removeSelf()
        layer = nil
    end
end

function scene:hide( event )
    local phase = event.phase
    if ( phase == "will" ) then
        self:stopAll()
    end
end

function scene:destroy( event )
end

scene:addEventListener('create' , scene)
scene:addEventListener('show'   , scene)
scene:addEventListener('hide'   , scene)
scene:addEventListener('destroy', scene)

return scene
