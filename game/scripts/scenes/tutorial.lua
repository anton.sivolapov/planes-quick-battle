local composer      = require( "composer" )
local scene         = composer.newScene()

local function buildControls()
    local grp = display.newGroup()


    local style = Planes.Config.winLevelStyle
    local bigLabel = display.newBitmapText(_s('Controls'), xMid, yMid, style.font, 120)
    bigLabel.rotation = -5
    bigLabel:setTintColor(style.fill.r,style.fill.g,style.fill.b)

    local tutorialScreens = ObjectFactory:create(
        'Image',
        'assets/graphics/tutorial-planes.png'
    )
    tutorialScreens.x = xMid
    tutorialScreens.y = yMid

    fitImage(tutorialScreens, _W, _H)


    local style = Planes.Config.mainTextStyle
    local controlsUpText = display.newBitmapText(
        {
            text    = _s('Touch here to dive DOWN'),
            align   = 'center',
            x       = .3 * _W,
            y       = .27 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )
    local controlsDownText = display.newBitmapText(
        {
            text    = _s('Touch here to rise UP'),
            align   = 'center',
            x       = .3 * _W,
            y       = .7 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )
    local controlsFireText = display.newBitmapText(
        {
            text    = _s('Touch here to FIRE'),
            align   = 'center',
            x       = .72 * _W,
            y       = .3 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )

    local continueText = display.newBitmapText(
        {
            text    = _s('tap to continue..'),
            x       = .75 * _W,
            y       = .8 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )
    
    grp:insert(tutorialScreens)
    grp:insert(controlsUpText)
    grp:insert(controlsDownText)
    grp:insert(controlsFireText)
    grp:insert(continueText)
    grp:insert(bigLabel)

    return grp;
end

local function buildRules()
    local grp = display.newGroup()

    local style = Planes.Config.winLevelStyle
    local bigLabel = display.newBitmapText(_s('Rules'), xMid, yMid, style.font, 120)
    bigLabel.rotation = -5
    bigLabel:setTintColor(style.fill.r,style.fill.g,style.fill.b)

    local tutorialScreens = ObjectFactory:create(
        'Image',
        'assets/graphics/tutorial-rules-planes.png'
    )
    tutorialScreens.x = xMid
    tutorialScreens.y = yMid

    fitImage(tutorialScreens, _W, _H)


    local style = Planes.Config.mainTextStyle
    local avoidGroundText = display.newBitmapText(
        {
            text    = _s('Avoid ground! It hits hard!'),
            align   = 'center',
            x       = .3 * _W,
            y       = .7 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )
    local hideInCloudsText = display.newBitmapText(
        {
            text    = _s('Hide in clouds!'),
            align   = 'center',
            x       = .3 * _W,
            y       = .35 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )
    local bonusesText = display.newBitmapText(
        {
            text    = _s('Collect bonuses to become stronger!'),
            align   = 'center',
            x       = .72 * _W,
            y       = .3 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )

    local continueText = display.newBitmapText(
        {
            text    = _s('tap to continue..'),
            x       = .75 * _W,
            y       = .8 * _H,
            font    = style.font,
            fontSize  = style.size
        }
    )
    
    grp:insert(tutorialScreens)
    grp:insert(avoidGroundText)
    grp:insert(hideInCloudsText)
    grp:insert(bonusesText)
    grp:insert(continueText)
    grp:insert(bigLabel)

    return grp;
end
-- Called when the scene's view does not exist:
function scene:show( event )
    if ( event.phase == "will" ) then
        local params = event.params
        local backgroundOverlay = ObjectFactory:create(
           'Image',
            Planes.graphics.getFrame('ui_background.png').sheet,
            Planes.graphics.getFrame('ui_background.png').frames
        )

        backgroundOverlay.width    = _W
        backgroundOverlay.height   = _H
        backgroundOverlay.anchorX  = 0
        backgroundOverlay.anchorY  = 0

        local control = buildControls()

        local hideTutorial = function(evt)
            if evt.phase == 'ended' then
                if params and params.nextScene then
                    local options = params.sceneOptions or {}
                    composer.hideOverlay( "fade", 200 )
                    composer.gotoScene(params.nextScene, options)
                else
                    composer.hideOverlay( "fade", 700 )
                end
            end
        end

        local showRules = function(evt)
            if evt.phase == 'ended' then
                control.isVisible = false
                local rules = buildRules()
                self.view:insert(rules)
                rules:addEventListener( "touch", hideTutorial )
            end
        end

        control:addEventListener( "touch", showRules )

        self.view:insert(backgroundOverlay)
        self.view:insert(control)

        utils.trackCurrentScene()
    end
end

function scene:hide( event )
    if ( event.phase == "will" ) then
        -- self.view:removeSelf()
        -- Call the "resumeGame()" function in the parent scene
--        parent:resumeGame()
    end
end

scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
return scene
