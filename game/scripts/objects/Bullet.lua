local Bullet = {}

-- graphics
local spritesheet   = Planes.graphics
local sheet         = Planes.graphicsSheet

function Bullet:new(bulletConfig)

    local newBullet = ObjectFactory:create(
        'Image',
        Planes.graphics.getFrame('bullet.png').sheet,
        Planes.graphics.getFrame('bullet.png').frames
    )
    newBullet.name = 'bullet'
    newBullet.outOfBoundsKill = true

    bulletConfig.speed = bulletConfig.speed or 750

    newBullet.x = bulletConfig.x
    newBullet.y = bulletConfig.y

    local direction = 1
    if utils.chanceRoll(50) then
        direction = -1
    end

    newBullet.TURN_RATE = 5
    newBullet.WOBBLE_LIMIT = direction * 15 -- degrees
    newBullet.WOBBLE_SPEED = 250 -- milliseconds
    newBullet.wobble = 0

    local offsetRectParams = { halfWidth=10, halfHeight=5 }
    physics.addBody(newBullet, "dynamic", {
                        box=offsetRectParams,
                        filter={
                            categoryBits    = Planes.Config.physics.interactiveCategory,
                            maskBits        = Planes.Config.physics.allMask,
                        }
                        
    })
    newBullet.isSensor = true
    newBullet.gravityScale = 0

    newBullet.rotation = bulletConfig.angle

    newBullet.wobble = -newBullet.WOBBLE_LIMIT


    local function wobbling(start)
        transition.to(
            newBullet,
            {
                wobble = start,
                time = newBullet.WOBBLE_SPEED,
                transition=easing.inOutSine,
                onComplete = function()
                    wobbling(-newBullet.wobble)
                end
            }
        )
    end
    wobbling(newBullet.WOBBLE_LIMIT)

    function newBullet:turnUp()
        self.rotation = self.rotation - self.TURN_RATE
    end
    function newBullet:turnDown()
        self.rotation = self.rotation + self.TURN_RATE
    end
    function newBullet:updateVelocity()
        -- convert rotation to radianes
        local rad = math.rad(self.rotation)
        local x = math.floor( math.cos(rad) * bulletConfig.speed );
        local y = math.floor( math.sin(rad) * bulletConfig.speed );
        self:setLinearVelocity( x, y )
    end

    function newBullet:rotateToAngle(targetAngle)
        if targetAngle and self.rotation ~= targetAngle then
            local delta = targetAngle - self.rotation
            if delta > 180 then delta = delta - 360 end
            if delta < -180 then delta = delta + 360 end

            if delta > 0 then
                self:turnDown()
            elseif delta < 0 then
                self:turnUp()
            end

            if math.abs(delta) < self.TURN_RATE then
                self.rotation = targetAngle
            end
        end
    end

    function newBullet:update()
        --if plane use aim, follow plane rotation
        if self.planeEmmiter and self.planeEmmiter.useAim then
            bulletConfig.angle = self.planeEmmiter.rotation
        end
        local targetAngle = bulletConfig.angle

        -- Add our "wobble" factor to the targetAngle to make the missile wobble
        -- Remember that self.wobble is tweening (above)
        if bulletConfig.wobble then
            targetAngle = targetAngle + self.wobble
        end
        self:rotateToAngle(targetAngle)
        self:updateVelocity()
    end

    return newBullet
end

return Bullet

