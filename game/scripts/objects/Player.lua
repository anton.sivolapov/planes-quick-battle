local Plane         = require 'scripts.objects.Plane'
Player = {}
function Player:new(...)
    p('create player..')

    local newPlane = Plane:new(...)
    newPlane.inClouds = false

    -- Called when a key event has been received
    local function onKeyEvent( event )
        -- reset all controls to default
        if ( event.keyName == "up" ) then
            if event.phase == 'down' then
                newPlane.goUp = true
            else
                newPlane.goUp = false
            end
        elseif ( event.keyName == "down" ) then
            if event.phase == 'down' then
                newPlane.goDown = true
            else
                newPlane.goDown = false
            end
        end
        if event.keyName == "space" then
            if event.phase == 'down' then
                newPlane:startFire()
            end
            if event.phase == 'up' then
                newPlane:stopFire()
            end
        end

        -- IMPORTANT! Return false to indicate that this app is
        -- NOT overriding the received key
        -- This lets the operating system execute its default handling of
        -- the key
        return false
    end

    -- Add the key event listener
    Runtime:addEventListener( "key", onKeyEvent )

    -- kill
    local genericKill = newPlane.kill
    function newPlane:kill()
        print'Player killed'
        self.scene.camera.cancel()
        genericKill(self)
    end

    -- back to camera, when leave game world
    function newPlane:backToCamera(cameraInfo)
        local angle

        if cameraInfo.outLeft then
            angle = 0
        end
        if cameraInfo.outRight then
            angle = 180
        end
        if cameraInfo.outTop then
            angle = 90
        end
        if cameraInfo.outBottom then
            angle = -90
        end
        if cameraInfo.outRight and cameraInfo.outTop then
            angle = 45
        end
        if cameraInfo.outRight and cameraInfo.outBottom then
            angle = -45
        end
        if cameraInfo.outLeft and cameraInfo.outTop then
            angle = 135
        end
        if cameraInfo.outLeft and cameraInfo.outBottom then
            angle = -135
        end

        self.rotation = angle

    end

    -- update
    local genericUpdate = newPlane.update
    function newPlane:update()
        local inCamera, inCameraInfo = utils.inCamera(self, 50)

        if inCamera then
            if self.goUp then
                self:turnUp()
            elseif self.goDown then
                self:turnDown()
            end
        else
            newPlane:backToCamera(inCameraInfo)
        end


        genericUpdate(self)
    end
    return newPlane
end

return Player

