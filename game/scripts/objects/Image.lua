local Image = {}

function Image:new(...)
    local newImage = display.newImage(...)
    newImage.bottom = function()
        return newImage.contentBounds.yMax
    end
    newImage.top    = function()
        return newImage.contentBounds.yMin
    end
    newImage.lef    = function()
        return newImage.contentBounds.xMin
    end
    newImage.right    = function()
        return newImage.contentBounds.xMax
    end
    return newImage
end

return Image

