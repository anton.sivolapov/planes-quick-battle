local Blast = {}

function Blast:new(scene, config)
    local blastAnimation = Planes.Config.simpleBlastAnimation
    local scale = .3
    local time = 500

    if config.bigBlast then
        blastAnimation = Planes.Config.regularBlastAnimation
        scale = 2
        time = 1000
    end


    local frames = Planes.graphics.getFrame(blastAnimation)
    local blast = display.newSprite(frames.sheet, {
        name        = 'blast',
        loopCount   = 1,
        time        = time,
        frames      = frames.ids
    })
    blast.x = config.x
    blast.y = config.y
    blast:scale(scale, scale)

    local function spriteListener( event )
        if event.phase == 'ended' then
            ObjectFactory:kill(blast)
        end
    end

    -- Add sprite listener
    blast:addEventListener( "sprite", spriteListener )

    blast:play()
    scene.layers.frontLayer:insert(blast)
    return blast
end

return Blast
