local Item = {}

function Item:new(scene, itemConfig)
    local itemData = scene:getItem(itemConfig.item_id)
    p('creating item with config:')
    p(itemConfig)
    
    local newItem = ObjectFactory:create(
        'Image',
        Planes.graphics.getFrame(itemData.startFrame).sheet,
        Planes.graphics.getFrame(itemData.startFrame).frames
    )

    newItem.outOfBoundsKill = true

    local viewGroup = scene.layers.playerLayer
    viewGroup:insert(newItem)

    newItem.itemData = itemData
    newItem.x = itemConfig.x
    newItem.y = itemConfig.y
    newItem.width = 20
    newItem.height = 20


    newItem.DEFAULT_DURATION = 30 --seconds
    newItem.SPEED = 25
    newItem.scene = scene

    physics.addBody(newItem, "dynamic", {
                        filter={
                            categoryBits    = Planes.Config.physics.interactiveCategory,
                            maskBits        = Planes.Config.physics.allMask,
                        }
    })
    newItem.isSensor = true
    newItem.gravityScale = 0
    newItem:scale(3,3)

    newItem:setLinearVelocity( 0, -newItem.SPEED )

    function newItem:updatePlane(plane)
        print'updating plane..'
        local handlers = Item.itemsHandlers[self.itemData.item_id]
        if not self.consumed and handlers then
            self.consumed = true
            self.apply = handlers.apply
            if handlers.resetStat then
                print'resetstat set'
                self.resetStat = handlers.resetStat
            end

            self.plane = plane
            --apply bonus
            self:apply(plane, self.itemData)

            local duration = self.itemData.duration
            if duration and self.resetStat then
                local _self = self
                ObjectFactory:create('Timer', duration * 1000, function()
                    _self:resetStat(_self.plane, _self.itemData)
                end)
            end
        end
    end

--[[    function newItem:update()
        print'update'
    end
]]

    newItem.superRemove = newItem.removeSelf
    function newItem:removeSelf(...)
        local event = {
            name = "kill",
        }
        self:dispatchEvent( event )
        newItem:superRemove(...)
    end

    return newItem
end


Item.itemsHandlers = {
    drunken_bullet= {
        apply= function( self, plane )
            plane.bulletWobble = true;
        end,
        resetStat= function( self, plane )
            plane.bulletWobble = false;
        end
    },
    aim= {
        apply= function( self, plane )
            plane:showAim()
        end,
        resetStat= function( self, plane )
            plane:hideAim()
        end
    },
    fire_up= {
        apply= function( self, plane )
            plane:incMaxBullets()
        end
    },
    speed= {
        apply= function( self, plane, itemData )
            plane:incSpeed(itemData.value)
        end,
        resetStat= function( self, plane, itemData )
            plane:incSpeed(-itemData.value)
        end
    },
    turn_rate= {
        apply= function( self, plane, itemData )
            plane:incTurnRate(itemData.value)
        end,
        resetStat= function( self, plane, itemData )
            plane:incTurnRate(-itemData.value)
        end
    },
    add_clouds= {
        apply= function( self, plane, itemData )
            local scene = self.scene
            --pick side of the cloud
            local x = math.random(
                -10,
                scene.gameWorld.width + 10
            )
            local y = math.random( 50, scene.gameWorld.height - 100 )
            --pick velocity
            local velocity = { y= 0 }
            if x < 0 then
                velocity.x = Planes.Config.cloudSpeed
            else
                velocity.x = -Planes.Config.cloudSpeed
            end

            timer.performWithDelay(1, function()
                scene:createObject({
                    object_constructor     = 'Cloud',
                    object_id     = cloud,
                    x             = x,
                    y             = y,
                    velocity      = velocity
                })
            end)
        end
    }
}

return Item

