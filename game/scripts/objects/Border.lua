-----------------------------------------------------------------------------------------
--
-- Border.lua
--
-----------------------------------------------------------------------------------------
local Border = {}

function Border:new(worldPhysics, scene)
    local newBorder = {}
    local borderWidth = 250
    local physics = worldPhysics

    local border = scene.layers.interfaceLayer

    local top = display.newRect( xMid, 0, _W - borderWidth - 130, borderWidth )
    top.type = 'border'
    local bottom = display.newRect( xMid, _H,  _W - borderWidth - 130, borderWidth )
    bottom.type = 'border'
    local left = display.newRect( 0, 0, borderWidth, 10000 )
    left.type = 'border'
    local right = display.newRect( _W, 0, borderWidth, 10000 )
    right.type = 'border'

    border:insert(top)
    border:insert(bottom)
    border:insert(left)
    border:insert(right)
    --

    physics.addBody( top, "static", { isSensor = true } )
    physics.addBody( bottom, "static", { isSensor = true } )
    physics.addBody( left, "static", { isSensor = true } )
    physics.addBody( right, "static", { isSensor = true } )

    top.alpha = 0
    bottom.alpha = 0
    left.alpha = 0
    right.alpha = 0

    newBorder.top       = top
    newBorder.bottom    = bottom
    newBorder.left      = left
    newBorder.right     = right

    function newBorder:removeSelf()
        newBorder.top:removeSelf()
        newBorder.bottom:removeSelf()
        newBorder.left:removeSelf()
        newBorder.right:removeSelf()
        self = nil
    end

    return newBorder
end

return Border

