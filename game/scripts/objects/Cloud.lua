local Cloud = {}

function Cloud:new(scene, config)
    local cloudsStyles = {
        'cloud2.png',
        'cloud3.png',
        'cloud4.png',
        'cloud5.png',
        'cloud6.png',
        'cloud7.png',
        'cloud8.png'
    }
    local cloudStyle = math.pick(cloudsStyles)

    local frames = Planes.graphics.getFrame(cloudStyle)

    local cloud = display.newImage(frames.sheet, frames.id)
    cloud.x = config.x
    cloud.y = config.y

    cloud:scale(1.5,1.5)

    local viewGroup = scene.layers.frontLayer
    viewGroup:insert(cloud)

    cloud.scene = scene

    physics.addBody(cloud, "dynamic", {
                        filter={
                            categoryBits = 3,
                            maskBits = 2
                        }
    })
    cloud.isSensor = true
    cloud.gravityScale = 0


    if config.velocity then
        cloud:setLinearVelocity( config.velocity.x or -25, config.velocity.y or 0 )
    else
        cloud:setLinearVelocity( -25, 0 )
    end

    function cloud:update( )
        local width = self.scene.gameWorld.width
        local vx, vy = self:getLinearVelocity()

        local bounds = self.contentBounds

        if vx > 0 and bounds.xMin > width then
            self.x = -100
            self.y = config.y
        end
        if vx < 0 and bounds.xMax < 0 then
            self.x = width + 100
            self.y = config.y
        end
    end

    -- cloud:scale(.8,.8)
    return cloud
end

return Cloud
