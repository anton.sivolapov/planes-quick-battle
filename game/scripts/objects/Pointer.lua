--TODO: listen 'destroy' event of parent sprite and kill self
local Pointer = {}

local updatePosition = function(pointer, cameraInfo)
    local x, y, angle
    local _W, _H = display.actualContentWidth, display.actualContentHeight

    y = cameraInfo.cameraY

    if cameraInfo.outLeft then
        x = 0
        angle = 180
    end
    if cameraInfo.outRight then
        x = _W
        angle = 0
    end
    if cameraInfo.outTop then
        y = 0;
        angle = -90
    end
    if cameraInfo.outBottom then
        y = _H
        angle = 90
    end
    if x == nil then
        x = cameraInfo.cameraX
    end
    if cameraInfo.outRight and cameraInfo.outTop then
        angle = -45
    end
    if cameraInfo.outRight and cameraInfo.outBottom then
        angle = 45
    end
    if cameraInfo.outLeft and cameraInfo.outTop then
        angle = -135
    end
    if cameraInfo.outLeft and cameraInfo.outBottom then
        angle = 135
    end

    pointer.x = x
    pointer.y = y
    pointer.rotation = angle
end

function Pointer:new(scene, sprite)
    -- create pointer
    local spriteId = "arrow_enemy.png"
    if sprite._name == "Item" then
        spriteId = "arrow_bonus.png"
    end
    local arrowFrame = Planes.graphics.getFrame(spriteId)

    local pointer = ObjectFactory:create(
        'Image',
        arrowFrame.sheet,
        arrowFrame.id
    )
    scene.layers.interfaceLayer:insert(pointer)
    pointer.isVisible = false
    pointer.anchorX = 1
    pointer.anchorY = .5
    pointer.updatePosition = updatePosition

    local onKill = function()
        sprite = nil
        ObjectFactory:kill(pointer)
    end

    sprite:addEventListener( "kill", onKill )

    function pointer:update()
        if sprite then
            local inCamera, inCameraInfo = utils.inCamera(sprite)
            if not inCamera then
                pointer.isVisible = true
                pointer:updatePosition(inCameraInfo)
            else
                pointer.isVisible = false
            end
        end
    end

    pointer.superRemove = pointer.removeSelf
    function pointer:removeSelf(...)
        pointer:superRemove()
    end

    return pointer
end

return Pointer

