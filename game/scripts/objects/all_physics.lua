-- This file is for use with Corona(R) SDK
--
-- This file is automatically generated with PhysicsEdtior (http://physicseditor.de). Do not edit
--
-- Usage example:
--			local scaleFactor = 1.0
--			local physicsData = (require "shapedefs").physicsData(scaleFactor)
--			local shape = display.newImage("objectname.png")
--			physics.addBody( shape, physicsData:get("objectname") )
--

-- copy needed functions to local scope
local unpack = unpack
local pairs = pairs
local ipairs = ipairs

local M = {}

function M.physicsData(scale)
	local physics = { data =
	{ 
		
		["planeGreen2"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   -32.428569793701172, -19.285713195800781  ,  45, 56  ,  -36, 33  ,  -58, 5  ,  -61, -40  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   59, -12  ,  59, 23  ,  45, 56  ,  -32.428569793701172, -19.285713195800781  ,  -2.4285697937011719, -50.714286804199219  ,  33, -50  }
                    }
                    
                    
                    
		}
		
		, 
		["planeBlue2"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   29, 52  ,  -20, 51  ,  -64, 10  ,  -60, -49  ,  15, -50  ,  64, -5  }
                    }
                    
                    
                    
		}
		
		, 
		["planeRed2"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   -39, 33  ,  -56, 4  ,  -40, -30  ,  2, -48  ,  60, 9  ,  33, 57  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   60, 9  ,  22, -29  ,  60, -20  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   -40, -30  ,  -56, 4  ,  -59, -56  }
                    }
                    
                    
                    
		}
		
		, 
		["planeYellow2"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   -33, 29  ,  -63.285714268684387, -4.4285697937011719  ,  -29, -31  ,  -3, -47  ,  62, -18  ,  62, 11  ,  42, 57  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   62, -18  ,  -3, -47  ,  47, -47  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   -29, -31  ,  -63.285714268684387, -4.4285697937011719  ,  -56, -57  ,  -40, -57  }
                    }
                    
                    
                    
		}
		
		, 
		["planeGray1"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   -53, -43  ,  -36.5, -24  ,  -15, 30  ,  -18, 31  ,  -63.5, 8  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   63, 6  ,  40, 34  ,  35, 36  ,  -15, 30  ,  -36.5, -24  ,  19, -42  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   35, 36  ,  40, 34  ,  43, 52  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 2, maskBits = 5, groupIndex = 0 },
                    shape = {   -18, 31  ,  -15, 30  ,  -16, 51  }
                    }
                    
                    
                    
		}
		
	} }

        -- apply scale factor
        local s = scale or 1.0
        for bi,body in pairs(physics.data) do
                for fi,fixture in ipairs(body) do
                    if(fixture.shape) then
                        for ci,coordinate in ipairs(fixture.shape) do
                            fixture.shape[ci] = s * coordinate
                        end
                    else
                        fixture.radius = s * fixture.radius
                    end
                end
        end
	
	function physics:get(name)
		return unpack(self.data[name])
	end

	function physics:getFixtureId(name, index)
                return self.data[name][index].pe_fixture_id
	end
	
	return physics;
end

return M

