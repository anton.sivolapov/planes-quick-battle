--[[
TODO:
]]--


-- graphics
local spritesheet   = Planes.graphics
local sheet         = Planes.graphicsSheet

local Plane = {
    getPlane = function(planeId)
        local compare = function(plane)
            return plane.plane_id == planeId
        end

        return _.detect( Planes.planesData, compare )
    end
}

function createPlaneSprite(planeConfig)
   -- p'create plane with config'
   -- p(planeConfig)
    local planeData = Plane.getPlane(planeConfig.plane_id)
    local frames = Planes.graphics.getFrame(planeData.animations.flight)
    local standFrames = Planes.graphics.getFrame(
        planeData.animations.stand or {planeData.startFrame}
    )
    local sequenceData = {
        {
            name    = 'stand',
            sheet   = standFrames.sheet,
            time    = 300,
            frames  = standFrames.ids
        },
        {
            name    = 'flight',
            sheet   = frames.sheet,
            time    = 300,
            frames  = frames.ids
        }
    }
    if planeData.animations.takeoff then
        local takeoffFrames = Planes.graphics.getFrame(planeData.animations.takeoff)
        _m.push(sequenceData, {
            name = 'takeoff',
            sheet   = takeoffFrames.sheet,
            time    = 700,
            frames  = takeoffFrames.ids,
            loopCount = 1
        })
    end

    local planeSprite = display.newSprite(
        frames.sheet, 
        sequenceData 
    )
    planeSprite:setSequence( "stand" )
    planeSprite:play()

    local x = planeConfig.x
    local y = planeConfig.y
    
    planeSprite.x = x
    planeSprite.y = y
    planeSprite.planeData = planeData

    local scale = planeData.scale or 1
    planeSprite:scale( scale, scale )

    local DEFAULT_SPEED = 70;
    planeSprite.SPEED = planeSprite.planeData.speed or DEFAULT_SPEED
    planeSprite.bulletWobble = false

    -- all bullets
    planeSprite.planeBullets = {}
    --coldown in second between firing
    planeSprite.FIRE_COOLDOWN = planeSprite.planeData.fire_cooldown or 2000
    --delay between bullets in miliseconds
    planeSprite.FIRE_RATE = 70
    --maximum bullets in one firing
    planeSprite.MAX_BULLETS = planeSprite.planeData.max_bullets or 3
    --bullet speed
    planeSprite.BULLET_SPEED = planeSprite.planeData.bullet_speed
    --when last bullet was created
    planeSprite.lastShotTime = 0
    -- is within firing session
    planeSprite._firing = false

    planeSprite.health = planeConfig.health or planeSprite.planeData.health or 1

    planeSprite.turning = false
    planeSprite.flying = false
    planeSprite.takingOff = false
    planeSprite.TURN_RATE = planeSprite.planeData.turn_rate or planeConfig.turn_rate or 1

    planeSprite.direction = planeConfig.orientation or
            planeData.orientation or "E"

    


    -- handle plane physics
    local physicsBodyName = planeData.bodyName
    
    planeSprite._adjustingAngle = 0

    if( planeSprite.direction == 'W' ) then
        planeSprite.yScale = -1 * planeSprite.yScale
        planeSprite._adjustingAngle = 180

        local body = pack( physicsData:get(physicsBodyName) )
        physics.addBody(planeSprite, "dynamic", unpack( flipBodyY( body ) ))
    else
        physics.addBody(planeSprite, "dynamic", physicsData:get(physicsBodyName))
    end

    
    ObjectFactory:create('Timer', 2 * 1000, function()
                               planeSprite.isFixedRotation = true
    end)
    if planeConfig.preview then
        planeSprite.isSensor = true
        planeSprite.isFixedRotation = true
        planeSprite.gravityScale = 0
    end

    planeSprite.rotation = planeSprite.rotation + planeSprite._adjustingAngle
    return planeSprite
end

function createAim( planeSprite )
    -- place aim
    planeSprite.useAim = false
    planeSprite.aimDistance = 300

    local frames = Planes.graphics.getFrame({"aim.png"})
    local aim = display.newSprite(
        frames.sheet, {
            name    = 'aim',
            frames  = frames.ids
        } )

    planeSprite.aim = aim
    planeSprite.aim.isVisible = false
    return aim
end

function createHealthBar( planeSprite )
    local width = 64
    local height = 4

    local healthBar = display.newGroup()
    healthBar.anchorChildren = true
    healthBar.isVisible = false

    healthBar.initialHealth = planeSprite.health
    healthBar.yAdjustment = -90
    
    local redBar = display.newRect( 0, 0, width, height )
    healthBar:insert(redBar)
    redBar.anchorX  = 0
    redBar:setFillColor(
        222/255.0,
        56/255.0,
        74/255.0
    )

    local greenBar = display.newRect( 0, 0, width, height )
    healthBar:insert(greenBar)
    greenBar:setFillColor( 
        57/255.0,
        129/255.0,
        65/255.0
    )
    greenBar.anchorX  = 0

    planeSprite.healthBar = healthBar

    function healthBar:decrease(numberOfDamage)
        local percent = numberOfDamage / self.initialHealth
        local newWidth = greenBar.width - greenBar.width * percent
        if newWidth < 0 then
            newWidth = 0
        end
        greenBar.width = newWidth
    end

    function healthBar:show()
        self.isVisible = true
    end

    return healthBar
end

function Plane:new(scene, planeConfig, group)
    local viewGroup = group or scene.layers.playerLayer
    local newPlane = createPlaneSprite(planeConfig)
    newPlane.scene = scene

    newPlane.collision = scene.onLocalCollision
    newPlane:addEventListener( "collision", newPlane )

    local aim = createAim(newPlane)
    local healthBar = createHealthBar(newPlane)

    viewGroup:insert(newPlane)

    -- local emitterFile = 'assets/emmiters/emitter31071.rg'
    -- local particle = 'particle31071.png'
    -- newPlane.emitter = pex.loadRG( nil, newPlane.x, newPlane.y,
    --     emitterFile,
    --     {
    --         texturePath = "assets/emmiters/",
    --         altTexture = particle,
    --         absolutePosition = true
    --     }
    -- )
    -- viewGroup:insert(newPlane.emitter)

    viewGroup:insert(aim)
    viewGroup:insert(healthBar)

    function newPlane:updateVelocity()
        -- convert rotation to radianes
        local rad = math.rad(self.rotation)
        local x = math.floor( math.cos(rad) * self.SPEED );
        local y = math.floor( math.sin(rad) * self.SPEED );

        -- print(x, y)
        -- self:applyForce( x, y, self.x, self.y )
        
        self:setLinearVelocity( x, y )

        
        -- draw trajectory
        
        --     local s = { x=self.x, y=self.y }
        -- for i = 1,10 do
        --     local newRad = rad + .2
        -- local x = math.floor( math.cos(newRad) * self.SPEED * 10  );
        -- local y = math.floor( math.sin(newRad) * self.SPEED * 10 );

        -- local startingVelocity = { x=x, y=y }
        --     local trajectoryPosition = getTrajectoryPoint( s, startingVelocity, i )
        --     local circ = display.newCircle(
        --         -- scene.layers.playerLayer,
        --         trajectoryPosition.x,
        --         trajectoryPosition.y,
        --         5
        --     )
        -- end
    end

    function newPlane:update()
        if self.useAim then
            self:updateAimPosition()
        end


        if self.takingOff then
            self.rotation = self.rotation - 1
        end

        if self._firing then
            self:fire()
        end

        if self.stopped then
            return true
        end

        self:followTarget()
        self:rotateToTarget()
        self:updateVelocity()

        -- self.emitter.x = self.x
        -- self.emitter.y = self.y
        -- self.emitter.angle = self.rotation + 180
        
        -- calculating angle

        if self.healthBar.isVisible then
            self.healthBar.x = self.x
            self.healthBar.y = self.y + self.healthBar.yAdjustment
        end
        
    end

    function getTrajectoryPoint( startingPosition, startingVelocity, n )
        --velocity and gravity are given per second but we want time step values here
        local t = 1/display.fps  --seconds per time step at 60fps
        local stepVelocity = { x=t*startingVelocity.x, y=t*startingVelocity.y }
        local stepGravity = { x=t*0, y=t*0 }
        return {
            x = startingPosition.x + n * stepVelocity.x + 0.25 * (n*n+n) * stepGravity.x,
            y = startingPosition.y + n * stepVelocity.y + 0.25 * (n*n+n) * stepGravity.y
        }
    end

    function newPlane:onPause()
        self:pause()
    end

    function newPlane:damage(n)
        n = n or 1
        self.health = self.health - n

--        print'Plane damaged'
        local event = {
            name = "damage"
        }
        self:dispatchEvent( event )

        if self.health <= 0 then
            self:killByBullet()
        end


        if self.healthBar then
            self.healthBar:decrease(n)
        end
    end

    function newPlane:killByGround(x, y)
        ObjectFactory:create('GroundBlast', scene, {
            bigBlast = true,
            x = x,
            y = y
        })
        self.health = 0
        local event = {
            name = "damage"
        }
        self:dispatchEvent( event )
        self:kill()
    end
    function newPlane:killByBullet()
        ObjectFactory:create('Blast', scene, {
            bigBlast = true,
            x = self.x,
            y = self.y
        })
        self:kill()
    end

    function newPlane:kill()
        if not self.died then
            self.died = true

            audio.play(Planes.Audio.planeExplosion)

            if self.onKill then
                self:onKill(self)
            end

            if self.aim then
                self.aim:removeSelf()
                self.aim = nil
            end

            if self.healthBar then
                self.healthBar:removeSelf()
                self.healthBar = nil
            end

            ObjectFactory:kill(self)
            local event = {
                name = "kill",
                target = self
            }
            self:dispatchEvent( event )
        end
    end

    function newPlane:setTarget(target, callback)
        if self.target and target and self.target.x ~= target.x then
--            printf('set target x: %s, y: %s', target.x, target.y)
        end

        self.target         = target
        self.targetCallback = callback
    end
    function newPlane:removeTarget()
        self.target = nil
    end
    function newPlane:setRotationTarget(rotationTarget, callback)
        self.rotationTarget         = rotationTarget
        self.rotationTargetCallback = callback
    end
    function newPlane:removeRotationTarget()
        self.rotationTarget         = nil
        self.rotationTargetCallback = nil
    end

    function newPlane:rotateToAngle(targetAngle)
        if self.rotation ~= targetAngle then
            local delta = targetAngle - self.rotation
            if delta > 180 then delta = delta - 360 end
            if delta < -180 then delta = delta + 360 end

            if delta > 0 then
                self:turnDown()
--                self:turnUp()
            elseif delta < 0 then
                self:turnUp()
--                self:turnDown()
            end


            if math.abs(delta) < self.TURN_RATE then
                self.rotation = targetAngle
                return true
            else
                return false
            end
        else
            return true
        end
    end

    function newPlane:rotateToTarget()
        if self.rotationTarget then
            local x1, y1 = self.x, self.y
            local x2, y2 = self.rotationTarget.x, self.rotationTarget.y

            if( x1 and y1 and x2 and y2 ) then
                local angle = utils.angleBetweenTwoPoints( x1, y1, x2, y2 )
                local finished = self:rotateToAngle(angle)

                if finished then
                    if self.rotationTargetCallback then
                        self.rotationTargetCallback()
                    end
                    self:removeRotationTarget()
                end
            end

        end
    end

    function newPlane:followTarget()
        if self.target then
--            p(self.target)
            local x1, y1 = self.x, self.y
            local x2, y2 = self.target.x, self.target.y

            if( x1 and y1 and x2 and y2 ) then
                local angle = utils.angleBetweenTwoPoints( x1, y1, x2, y2 )
                self:rotateToAngle(angle)

                angle = angle - self.rotation
                -- print( 'angle', angle )
            end
            if math.abs(x1-x2) < 10 and math.abs(y1-y2) < 10 then

                    self.targetCallback()
                    self.targetCallback = nil
                self:removeTarget()
            end
--            angle = angle - self._adjustingAngle
        end
    end

    function newPlane:turnUp()
        local dir = -1
        if( self.yScale < 0 ) then
            dir = 1
        end
        self.rotation = self.rotation - dir * self.TURN_RATE
    end
    function newPlane:turnDown()
        local dir = -1
        if( self.yScale < 0 ) then
            dir = 1
        end
        self.rotation = self.rotation + dir * self.TURN_RATE
    end
    function newPlane:reachBorder()
        self.borderReached = true
    end
    function newPlane:leaveBorder()
        self.borderReached = false
    end

    function newPlane:stop( )
        self.stopped = true
        self:setLinearVelocity( 0, 0 )
    end

    function newPlane:landed()
        self:stop()
        self.flying = false
    end
    function newPlane:incSpeed(value)
        self.SPEED = self.SPEED + value
    end
    function newPlane:incTurnRate(value)
        self.TURN_RATE = self.TURN_RATE + value
    end
    function newPlane:incMaxBullets()
        self.MAX_BULLETS = self.MAX_BULLETS + 1
        self:incAvailableBullets()
    end
    function newPlane:showAim()
        self.useAim = true
        self.aim.isVisible = true
    end
    function newPlane:hideAim()
        self.useAim = false
        if self.aim then
            self.aim.isVisible = false
        end
    end
    function newPlane:updateAimPosition()
        local rotation = math.rad(self.rotation)
        self.aim.x = self.x + math.cos(rotation) * self.aimDistance
        self.aim.y = self.y + math.sin(rotation) * self.aimDistance
        self.aim.rotation = self.rotation
    end
    function newPlane:startPropeller()
        if self.planeData.hasPropeller then
            if self.planeData.animations.stand then
                -- do nothing
                self:setSequence( "stand" )
                self:play()
            else
                self:setSequence( "flight" )
                self:play()
            end
        end
    end
    function newPlane:startEngine()
        -- play sound
        audio.play(Planes.Audio.enginePlane, {
            loops = -1
        })
        if self.planeData.animations.takeoff then
            -- do nothing
            self:startPropeller()
        else
            self:startPropeller()
        end
    end
    function newPlane:takeoff()
        self.takingOff  = true
        self.flying     = true
        self.stopped    = false
        if self.planeData.animations.takeoff then
            self:setSequence( "takeoff" )
            self:play()
            local function spriteListener( event )
                if event.phase == 'ended' then
                    print( "Sprite event: " .. event.phase )
                    self:removeEventListener( "sprite", spriteListener )
                    self.takingOff  = false
                    self:setSequence( "flight" )
                    self:play()
                end
            end

            -- Add sprite listener
            self:addEventListener( "sprite", spriteListener )
        else
            ObjectFactory:create('Timer', 500, function()
                    self.takingOff  = false
            end)
        end
    end

    ----
    -- firing logic
    ----
    local availableBullets = newPlane.MAX_BULLETS

    function newPlane:getAvailableBullets()
        return availableBullets
    end

    function newPlane:incAvailableBullets()
        availableBullets = availableBullets + 1
        local event = {
            name = "availableBulletsChanged"
        }
        self:dispatchEvent( event )
    end

    function newPlane:_fire()
        local plane = self
        plane:createBullet()
        availableBullets = availableBullets - 1
        local event = {
            name = "availableBulletsChanged"
        }
        self:dispatchEvent( event )
    end
    function newPlane:restoreBullet()
        if availableBullets < self.MAX_BULLETS then
            availableBullets = availableBullets + 1

        -- print('restore bullet. new available', availableBullets, 'max bullets', self.MAX_BULLETS)
            local event = {
                name = "availableBulletsChanged"
            }
            if self and self.dispatchEvent then
                self:dispatchEvent( event )
            end
        end
    end

    function newPlane:fire()
        local plane = self
        local now = system.getTimer()
        local delay = now - plane.lastShotTime
        if availableBullets > 0 then
            if delay > plane.FIRE_RATE then
                plane:_fire()
                plane.lastShotTime = now

                -- slightly randomize bullet recreation
                local randomCooldown = math.random(
                    plane.FIRE_COOLDOWN * .5,
                    plane.FIRE_COOLDOWN + plane.FIRE_COOLDOWN * .5
                ) 
                ObjectFactory:create('Timer', randomCooldown, function()
                                         plane:restoreBullet()
                end)
            end
        end
    end
    ----
    -- END firing logic
    ----

    function newPlane:createBullet()
        
        local freeChannel = audio.findFreeChannel()
        -- print('fire on channel', freeChannel, audio.getVolume( { channel=freeChannel } ))
        audio.play(Planes.Audio.shot, {
                       channel = freeChannel
        })
        
        local bulletConfig = {
            x = self.x,
            y = self.y,
            angle = self.rotation,
            speed = self.BULLET_SPEED,
            wobble =  self.bulletWobble
        }
        local bullet = ObjectFactory:create('Bullet', bulletConfig)
        bullet.planeEmmiter = self

        self.scene.layers.backgroundDetail:insert(bullet)
        bullet:toBack()
    end
    function newPlane:startFire()
        self._firing = true
    end
    function newPlane:stopFire()
        self._firing = false
    end

    function newPlane:consumeItem(item)
        print'consuming item..'
        audio.play(Planes.Audio.consumeBonus)

        item:updatePlane(self)
    end

    return newPlane
end


return Plane
