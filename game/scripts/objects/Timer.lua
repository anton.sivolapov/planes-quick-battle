local Timer = {}

function Timer:new(delay, cb)
    local newTimer
    local wrappedCb = function()
        cb()
        newTimer:removeSelf()
    end
    newTimer = timer.performWithDelay(delay, wrappedCb)
    newTimer.status = 'running'

    function newTimer:pause()
        if newTimer.status == 'running' then
            newTimer.status = 'paused'
            timer.pause(self)
        end
    end

    function newTimer:play()
        if newTimer.status == 'paused' then
            newTimer.status = 'running'
            timer.resume(self)
        end
    end

    function newTimer:removeSelf()
        newTimer.status = 'canceled'
        timer.cancel(self)
        self = nil
    end

    return newTimer
end

return Timer

