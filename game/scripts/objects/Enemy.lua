local Plane         = require 'scripts.objects.Plane'
Enemy = {}
function Enemy:new(scene, planeConfig)
    local newPlane = Plane:new(scene, planeConfig)
    newPlane.agresiveness = planeConfig.chanceToFollowPlayer or 30
    newPlane.chanceToLoosePlayer = planeConfig.chanceToLoosePlayer or 10
    newPlane.followingPlayerAllowed = true

    -- function newPlane:avoidBorder()
    --     local shouldAvoid = self:shouldAvoidBorder()

    --     local scene = self.planeData.scene
    --     local angle = GetAngleOfLineBetweenTwoPoints(
    --         self.x,
    --         self.y,
    --         scene.gameWorld.widht * .5,
    --         scene.gameWorld.height * .5
    --     )
    --     -- print(  angle )
    --     --[[
    --     print(
    --         display.actualCenterX,
    --         display.actualCenterY,
    --         self.plane.x,
    --         self.plane.y
    --     )
    --     --]]
    --     self.plane.rotation = angle
    -- end
    -- function newPlane:shouldAvoidBorder()
    --     -- display.viewableContentWidth
    -- end

    function newPlane:angleToPlayer()
        local playerAngle = utils.angleBetweenTwoPoints(
            self.x, self.y,
            self.scene.player.x, self.scene.player.y
        )

        return playerAngle
    end

    function newPlane:pointToPlayer()
        if self.scene.player then
            local angleToPlayer = self:angleToPlayer()
            local rotation = self.rotation

            if math.abs( angleToPlayer - rotation ) < 15 then
                return true
            else
                return false
            end
        else
            return false
        end
    end

    function newPlane:shootPlayer()
        if self:pointToPlayer() then
            if utils.chanceRoll(10) then
                self:fire()
            end
        end
    end

    local onPlayerKill = function()
        newPlane:removeTarget()
        newPlane.playerKilled = true
    end
    scene.player:addEventListener( "kill", onPlayerKill )

    function newPlane:avoidGround(callback)
        self:setRotationTarget({
                x = self.x,
                y = 0,
        }, callback)
    end
    
    function newPlane:followPlayer()
        local chance = utils.chanceRoll(newPlane.agresiveness)
        if chance then
            if self.followingPlayerAllowed then
                self:setRotationTarget(self.scene.player)
            end

            -- should be very small chance
            local looseChance = utils.chanceRoll(newPlane.chanceToLoosePlayer)
            if looseChance and self.followingPlayerAllowed then
                print 'loose player for few seconds'

                self.followingPlayerAllowed = false
                -- loose player for few seconds 
                newPlane.followPlayerTimer = timer.performWithDelay(5000, function()
                    newPlane.followingPlayerAllowed = true
                end)
            end
        end
    end


    -- update
    local genericUpdate = newPlane.update

    function newPlane:update()
        local world             = self.scene.gameWorld
        local distanceToGround  = world.height - self.y
        local outOfBorder       = utils.outOfBorder( world, self, -200 )

        local canInteractWithPlayer = true
        if self.scene.player then
            canInteractWithPlayer = not self.scene.player.inClouds and not newPlane.playerKilled
        else
            canInteractWithPlayer = false
        end

        --TODO remove
        -- canInteractWithPlayer = false

        local angle = self.rotation
        if angle > 180  then angle = angle - 360 end
        if angle < -180 then angle = angle + 360 end
        
        local finishManeur = function()
            -- print'finish maneur'
            self.maneuvering = false 
        end

        if self.maneuvering then
            -- do nothing
        else
            -- select right maneuver


            -- don't forget, that angle is upsidedown
            if 0 < angle and angle < 180 and distanceToGround < 500 then
                self.maneuvering = true
                -- to close to ground, maneuvering
                -- print( 'avoid ground', distanceToGround )

                self:avoidGround(finishManeur)
            elseif outOfBorder then
                -- print'out of border, follow to the center'

                self.maneuvering = true
                -- out of border, do flight to the center of the world
                self:setRotationTarget({
                        x = world.width * .5,
                        y = world.height * .5,
                }, finishManeur)
            elseif canInteractWithPlayer then
                self:shootPlayer()
                self:followPlayer()
            end
        end

         --[[
        local canInteractWithPlayer = true
        if self.scene.player then
            canInteractWithPlayer = not self.scene.player.inClouds
        end

        --TODO remove
        canInteractWithPlayer = false

        local border = scene.gameWorld
        local outOfBorder = utils.outOfBorder( border, self, -200 )
        if outOfBorder then
            -- print 'OUT of border'
            self.outofborder = true

            -- track game center
            
            local delta = self.rotation
            if delta > 180 then delta = delta - 360 end
            if delta < -180 then delta = delta + 360 end

            local world = self.scene.gameWorld
            local distanceToGround = world.height - self.y

            if distanceToGround < 500 then
                print( 'to close to ground')
            end

            if 0<delta and delta < 180 or distanceToGround < 500 then
                if not self.avoidingBorder then
                    -- print('facing ground')
                    self.avoidingBorder = true
                    self:setTarget({
                            x = self.x,
                            y = self.y - 100,
                    })
                end
            else
                if not self.avoidingBorder then
                    self.avoidingBorder = true
                    self:setTarget({
                            x = world.width * .5,
                            y = world.height * .5,
                    })
                end
            end
        else
            self.avoidingBorder = false
            if self.outofborder then
--                print 'IN border'
            end
            self.outofborder = false

            if canInteractWithPlayer then
                -- track player or track nothing
                local chance = utils.chanceRoll(70)
                if chance then
                    print 'track player'

                    self:setTarget(self.scene.player)
                else
                    self:removeTarget()
                end
            else
                self:removeTarget()
            end

        end

        if canInteractWithPlayer and not newPlane.playerKilled then
            self:shootPlayer()
        end
]]--
        genericUpdate(self)

    end
    return newPlane
end

return Enemy
