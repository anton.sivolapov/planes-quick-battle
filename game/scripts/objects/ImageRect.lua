local ImageRect = {}

function ImageRect:new(...)
    local newImage = display.newImageRect(...)
    return newImage
end

return ImageRect

