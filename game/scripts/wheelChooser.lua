local getIds = function(center, items)
    local totalItems = #items
    local centerId = center
    if centerId > totalItems then
        centerId = 1
    end
    if centerId < 1 then
        centerId = totalItems
    end
    if ( centerId > 1 ) and ( centerId < totalItems ) then
        return centerId - 1, centerId, centerId + 1
    elseif ( centerId == totalItems ) then
        return centerId - 1, centerId, 1
    else
        return totalItems, centerId, centerId + 1
    end
end

local initWheel = function(config)
    -- set first item on center
    local items = config.items
    local currentCenter = config.startIndex
    local totalItems = #items
    local wheelGroup = display.newGroup()
    config.parentGroup:insert( wheelGroup )

    wheelGroup.anchorChildren = true
    -- wheelGroup.anchorX = 0
    -- wheelGroup.anchorY = 0

    wheelGroup.x = config.x
    wheelGroup.y = config.y
    -- print('widht %s height %s', wheelGroup.width, wheelGroup.height)

    local enableUI = true
    local animationsTime = 500
    local onSelectCallback  = config.onSelect or function() print('--- empty onselect') end
    local onSwipeCallback   = config.onSwipe or function() print('--- empty onswipe') end

    local centerScale   = 1
    local leftScale     = .7
    local rightScale    = .7
    local outScale      = .05
    local padding       = 300
    
    local leftId, centerId, rightId = getIds( currentCenter, items )
    
    -- printf('new left %s, new center %s, new right %s',
    --        leftId, centerId, rightId)
    -- get indexes
    _m.each(items, function(index, val) 
                wheelGroup:insert(val)

                if index == centerId then
                    val.x = wheelGroup.x
                    val:scale(centerScale, centerScale)
                elseif index == rightId then
                    val.x = wheelGroup.x + padding
                    val:scale(rightScale, rightScale)
                elseif index == leftId then
                    val.x = wheelGroup.x - padding
                    val:scale(leftScale, leftScale)
                else
                    val.x = wheelGroup.x
                    val.isVisible = false
                end
    end)
    print('call on select')
    onSelectCallback(items[currentCenter])


    local function swipeLeft()
        onSwipeCallback(items[currentCenter])
        --prepare items to appear
        enableUI = false
        local newLeftId, newCenterId, newRightId = getIds( currentCenter + 1, items )
        -- printf('new left %s, new center %s, new right %s',
        --        newLeftId, newCenterId, newRightId)
        -- printf('left %s, center %s, right %s',
        --        leftId, centerId, rightId)

        items[newRightId].x = items[rightId].x
        items[newRightId]:scale(0, 0)
        items[newRightId].isVisible = true

        wheelGroup:insert(items[leftId])
        wheelGroup:insert(items[newLeftId])
        wheelGroup:insert(items[rightId])
        wheelGroup:insert(items[currentCenter])

        transition.to(items[currentCenter], {
                          xScale = leftScale,
                          yScale = leftScale,
                          x = items[leftId].x,
                          time = animationsTime,
                          onComplete = function() 
                              enableUI = true 
                              onSelectCallback(items[currentCenter])
                          end
        } )

        transition.to(items[rightId], {
                          xScale = centerScale,
                          yScale = centerScale,
                          x = items[currentCenter].x,
                          time = animationsTime
        })
        transition.to(items[leftId], {
                          xScale = outScale,
                          yScale = outScale,
                          x = items[leftId].x + 30,
                          time = animationsTime,
                          onComplete = function(item) 
                              item.isVisible = false
                          end
        })


        transition.to(items[newRightId], {
                          xScale = rightScale,
                          yScale = rightScale,
                          time = animationsTime
        })
        
        leftId, centerId, rightId = newLeftId, newCenterId, newRightId
        currentCenter = newCenterId
    end

    local function swipeRight()
        onSwipeCallback(items[currentCenter])
        --prepare items to appear
        enableUI = false
        local newLeftId, newCenterId, newRightId = getIds( currentCenter - 1, items )
        -- printf('new left %s, new center %s, new right %s',
        --        newLeftId, newCenterId, newRightId)
        -- printf('left %s, center %s, right %s',
        --        leftId, centerId, rightId)

        items[newLeftId].x = items[leftId].x
        items[newLeftId]:scale(0, 0)
        items[newLeftId].isVisible = true

        wheelGroup:insert(items[rightId])
        wheelGroup:insert(items[newLeftId])
        wheelGroup:insert(items[leftId])
        wheelGroup:insert(items[currentCenter])

        transition.to(items[currentCenter], {
                          xScale = rightScale,
                          yScale = rightScale,
                          x = items[rightId].x,
                          time = animationsTime,
                          onComplete = function() 
                              enableUI = true 
                              onSelectCallback(items[currentCenter])
                          end
        } )

        transition.to(items[rightId], {
                          xScale = outScale,
                          yScale = outScale,
                          x = items[rightId].x - 30,
                          time = animationsTime,
                          onComplete = function(item) 
                              item.isVisible = false
                          end
        })
        transition.to(items[leftId], {
                          xScale = centerScale,
                          yScale = centerScale,
                          x = items[currentCenter].x,
                          time = animationsTime
        })


        transition.to(items[newLeftId], {
                          xScale = leftScale,
                          yScale = leftScale,
                          time = animationsTime
        })
        
        leftId, centerId, rightId = newLeftId, newCenterId, newRightId

        currentCenter = newCenterId
    end

    local function onObjectTouch( event )

        if event.phase == 'began' then
            audio.play(Planes.Audio.click);
        end
        if ( event.phase == "ended" and enableUI ) then
            local delta = event.xStart - event.x
            local minimumShift = 50
            if math.abs(delta) > minimumShift then
                if ( delta < 0 ) then
                    print('swipe right -->')
                    swipeRight()
                else
                    print('swipe left <--')
                    swipeLeft()
                end
                return false
            else
                local xMin, xMax, yMin, yMax =
                    wheelGroup.contentBounds.xMin,
                    wheelGroup.contentBounds.xMax,
                    wheelGroup.contentBounds.yMin,
                    wheelGroup.contentBounds.yMax

                if yMin < event.y and event.y < yMax then
                    local partWidth  = wheelGroup.width*.3
                    if event.x < xMax and event.x > xMax - partWidth then
                        swipeLeft()
                    elseif event.x > xMin and event.x < xMin + partWidth then
                        swipeRight()
                    end
                end
            end
        end
        return true
    end
    config.parentGroup:addEventListener( "touch", onObjectTouch )
    return wheelGroup
end

return {
    init = initWheel
}
