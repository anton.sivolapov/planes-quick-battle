print'Init Store..'
-- init DB
local sqlite3   = require 'sqlite3'
-- Open "data.db". If the file doesn't exist, it will be created
local path      = system.pathForFile( "game_store3.db", system.DocumentsDirectory )
local db        = sqlite3.open( path )

-- Handle the "applicationExit" event to close the database
local function onSystemEvent( event )
    if ( event.type == "applicationExit" ) then
        db:close()
    end
end

-- Set up the table if it doesn't exist
local tablesetup = [[CREATE TABLE IF NOT EXISTS state_game (id INTEGER PRIMARY KEY, key, content);]]
db:exec( tablesetup )

-- Setup the event listener to catch "applicationExit"
Runtime:addEventListener( "system", onSystemEvent )


local __defaultState = {
    fullVersion  = false,
    enableMusic  = true,
    enableSound  = true,
    currentPlane = 'green',
    numberOfRuns = 0,
    askedForRating = false,
    openedPlanes = {
        'green',
    },
    openedLevels = {
        1
    }
}

local __manualState
if utils.desktopApp() then
    __manualState = {
        fullVersion  = false,
        enableMusic  = true,
        enableSound  = true,
        currentPlane = 'green',
        numberOfRuns = 0,
        askedForRating = false,
        openedPlanes = {
            'green', 'red', 'yellow', 'blue'
        },
        openedLevels = {
            1, 2, 3, 4, 5, 6
        }
    }
end
-- __manualState = {
--     currentPlane = 'green',
--     openedPlanes = {
--         'green', 'red', 'gray'
--     },
--     openedLevels = {
--         1, 2, 3
--     }
-- }
local Store
Store = {
    _getState       = function()
        local getCurrentState = "SELECT * from state_game where key='current_state'"
        local currentState
        for row in db:nrows(getCurrentState) do
            currentState = row
            break
        end
        if currentState then
            currentState = json.decode(currentState.content)
        end

        return currentState
    end,
    updateStore     = function(changer)
        Store.setState( changer( Store.getState() ) )
    end,
    getState        = function()
        local success, state = pcall( Store._getState )
        if success then
            -- no errors while running `foo'
            return state
        else
            -- `foo' raised an error: take appropriate actions
            return false
        end
    end,
    setState        = function(newState)
        local strState = json.encode(newState)
        local update = ([[
        UPDATE state_game
        SET content='%s'
        WHERE key='current_state';
        ]]):format(strState)


       print'New state:'
       print(json.prettify(newState))
        return sqlite3.OK == db:exec( update )
    end,
    initState       = function()
        local tablefill = ("INSERT INTO state_game VALUES (NULL, '%s','%s');"):format(
            'current_state',
            json.encode(__defaultState)
        )
        db:exec( tablefill )
    end,
    resetState      = function()
--[[        local droptable = ("DROP TABLE state_game")
        db:exec( droptable )]]
        Store.setState(__defaultState)
    end
}

local state = Store.getState()
if not state then
    print'Empty table. Init game state..'
    Store.initState()
end
if __manualState then
    print'WARNING: game state was manually rewrited. Please remove it for public version'
    Store.setState(__manualState)
end

--Store:resetState()

--print'Current state:'
--print(json.prettify(Store.getState()))
return Store
