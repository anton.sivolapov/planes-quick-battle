application =
{
    license =
    {
        google =
        {
            key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkRAdfwSrtnVX7OBEbw5lt0zNOM1W7/4TT9hD/JkDGlng/bk3eSo025uMIfPqr2AckrAoDPwpQeVReSMOb+rlt0mb6zTF1s/YZag3cGSCK99znoNH57jNVlrSZdG6K7Xut5eQYrZH03gZug5B3jkcoM9AchUiPKkPAyrGO3ept4yTg1X6ng4sUig8m3aenZe5ItgUhCXT7D0Zj8QUryixn+oW7BJ6TNBMLmVPhn3IBJTTZcpjtOQRRmnThQAW/MdPoeocd606HORsv0ZQgAlcraVlDre1zyEoLxC40l7kHP50SMrsIFySrgvrXlFOx0Qc5yfbsWdXl+Kx1XIzqa7szwIDAQAB",
        },
    },
    content =
    {
        height = 1280,
        width = 800,
        scale = "letterbox",
        fps = 30,
        xAlign = "left",
        yAlign = "top",
        audioPlayFrequency = 22050
    },
}
